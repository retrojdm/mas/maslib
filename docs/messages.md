/ [Home](../../README.md) / Serial commands

# Modular Audio System - Serial commands

The Modular Audio System uses serial commands to control the various modules within each unit.

The command format was designed to be human-readable, making it ideal for controlling and debugging via a terminal.

## Message format

### Commands

Command codes are case-insensitive, and are always three characters long. The first character identifies the module. Module character "keys" are unique across all units.

Eg:

- `SLS` is a (S)ystem command that lists module keys.
- `OVL` is an Audio (O)utput command that gets the volume level.

### Command parameters

Optional parameters follow a space, with multiple parameters separated by additional spaces.

Eg:

- `SAK A`
- `AMO fft mono 20 16`

If an argument contains spaces, you can wrap it with double quotes (`"`).

A limited set of special characters is supported using C-style escape sequences. Eg: `\n`, `\t`, `\\`, `\"`. All other special characters are escaped with their ASCII hex code. Eg: `\xF8`.

Eg:

- `TNA "Gonzo's car"`
- `TNA "The \"animal\""`

### Responses and events

Event codes are surrounded by square brackets. Parameters follow the same rules as command parameters.

Eg:

- `[TAR] "Led Zeppelin"`

### Errors and logging

Any module can emit a special "System Log" (`SLG`) event for errors, warnings, and info. See the [System Module messages](./messages-main-unit.md) for more info.

## Faking responses and events

Messages are passed between units without encoding thier origin. Any command can originate from any unit.

With the exception of some system messages, responses and events usually originate from their own module. However there are no restrictions, which means a user on a terminal could fake any message. This can be very useful when debugging.

The system has been designed to be hackable.

## Serial module

The serial module runs on both the Main Unit and Front Panel.

### Serial commands

| Command        | Description                                                                                                              |
| :------------- | :----------------------------------------------------------------------------------------------------------------------- |
| `XEC <state?>` | Get or set the command echo state. Only effects the serial port you're currently on.                                     |
| `XFC <state?>` | Get or set a value indicating whether ETX/ACK flow control is enabled. Only effects the serial port you're currently on. |

### Serial responses

| Message         | Description                                                 |
| :-------------- | :---------------------------------------------------------- |
| `[XEC] <state>` | A value indicating whether serial command echo is enabled.  |
| `[XFC] <state>` | A value indicating whether ETX/ACK flow control is enabled. |

## See also

- [Serial messages - Main Unit (MU50)](./messages-main-unit.md)
- [Serial messages - Front Panel (FP10, FP20)](./messages-front-panel.md)