/ [Home](../../../README.md) / [Serial commands](../README.md) / List of Main Unit commands

# Modular Audio System - List of Main Unit (MU50) Serial Messages

## Software modules

Modules are a way of organising code into logical groups. They don't have to represent hardware. For example, the radio module will exist whether or not a radio I/O card is installed. It's also possible to have more than one radio card installed.

The following software modules run on the Main Unit:

| Key | Module name  | Responsibilities                                                                                           |
| :-- | :----------- | ---------------------------------------------------------------------------------------------------------- |
| `A` | Analyser     | Manages analysers. Eg: Peaks (L/R levels), FFT (spectrum analyser) and Waveform.                           |
| `I` | Audio Input  | Manages switching between audio input sources.                                                             |
| `O` | Audio Output | Manages audio output. i.e.: volume, mute, balance, and fade.                                               |
| `T` | Bluetooth    | Manages pairing, AVRCP remote control, and AVRCP title/album/artist metadata from bluetooth audio sources. |
| `C` | Clock        | Manages the realtime clock.                                                                                |
| `F` | Effects      | Manages effects. Eg: Beep or XDR toneburst, etc.                                                           |
| `Q` | Equaliser    | Manages EQ levels (Up to 8 frequencies for bass/treble control).                                           |
| `M` | Media        | Manages navigation and playback of media files on USB and SD.                                              |
| `P` | Power        | Manages the battery relay and accessories optocoupler.                                                     |
| `R` | Radio        | Manages radio bands, frequencies, and presets etc.                                                         |
| `N` | Settings     | Handles settings stored in EEPROM.                                                                         |
| `S` | System       | Provides help, and manages the state of the overall system.                                                |

### Analyser commands

| Command                                | Description                                                        |
| :------------------------------------- | :----------------------------------------------------------------- |
| `AMO <mode?> <stereo?> <fps?> <size?>` | Get or set Audio analyser mode, stereo mode, frame rate, and size. |

### Analyser responses/events

| Message                                | Description                                                                              |
| :------------------------------------- | :--------------------------------------------------------------------------------------- |
| `[AMO] <mode> <stereo> <fps?> <size?>` | Audio analyser mode (`none`, `peaks`, `fft`, `waveform`), frame rate, and optional size. |
| `[APM] <value>`                        | Audio analyser mono peak data (0..255).                                                  |
| `[APS] <left> <right>`                 | Audio analyser stereo peak data (0..255 per channel).                                    |
| `[AFM] <mixed>`                        | Audio analyser mono Fourier Transform data (base 64), <size> length.                     |
| `[AFS] <left> <right>`                 | Audio analyser stereo Fourier Transform data (base 64). Each channel is <size> length.   |
| `[AWM] <mixed>`                        | Audio analyser mono Waveform data (base 64), <size> length.                              |
| `[AWS] <left> <right>`                 | Audio analyser stereo Waveform data (base 64). Each channel is <size> length.            |

Base64 data is handled in a very simplistic way. The Audio Analyser only transmits Fourier Transform and Waveform data
as 6-bit values (0..63). It encodes a single 6-bit value per character using the standard base64 character set.
Interested modules can then use a simple lookup-table to decode the event data.

## Audio Inputs

### Audio Input commands

**Note:** Audio input source indexes don't represent I/O card slots. See the `ioc` setting.

| Command        | Description                                                                   |
| :------------- | :---------------------------------------------------------------------------- |
| `IPC`          | Get PC USB connection state.                                                  |
| `ILS`          | List audio source types.                                                      |
| `IDX <index?>` | Get or set audio source index. Prefix value with '+' or '-' to make relative. |
| `INF <index?>` | Get current or specific audio source info.                                    |

### Audio Input responses/events

**Note:** the Audio Input module listens for the `[NSE] ioc ...` setting event, which will trigger it to publish an `[ILS] <types[]>` event of it's own.

| Message                                      | Description                                                                                                                                                                                                                                        |
| :------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `[IPC] <state>`                              | PC USB connection state.                                                                                                                                                                                                                           |
| `[ILS] <types[]>`                            | Pipe-separated list of audio sources.                                                                                                                                                                                                              |
| `[IDX] <index>`                              | Current audio source index.                                                                                                                                                                                                                        |
| `[INF] <index> <type> <slot> <id> <suffix?>` | `<type>` can be: `PC`, `MED`, `LIN`, `RAD`, or `BLU`. `<slot>` is `-1` when N/A. `<id>` is "-" when N/A. Otherwise it's something like `BLU`, `RAD`, `RAT`, `RAS`, `LIN`, `LIO`, etc. `<suffix>` is used when multiple of the same type exist. |

## Audio Output module

### Audio Output commands

| Command          | Description                                                               |
| :--------------- | :------------------------------------------------------------------------ |
| `OVL <volume?>`  | Get or set volume (0.255). Prefix value with '+' or '-' to make relative. |
| `OMU <state?>`   | Get or set mute status.                                                   |
| `OBA <balance?>` | Get or set left-right balance (-127..127).                                |
| `OFA <fade?>`    | Get or set front-rear fade (-127..127).                                   |

### Audio Output responses/events

| Message           | Description         |
| :---------------- | :------------------ |
| `[OVL] <volume>`  | Volume.             |
| `[OMU] <state>`   | Mute status.        |
| `[OBA] <balance>` | Left-right balance. |
| `[OFA] <fade>`    | Front-rear fade.    |

## Bluetooth module

### Bluetooth commands

The system supports multiple bluetooth I/O cards installed at once.

| Command                | Description                                                                        |
| :--------------------- | :--------------------------------------------------------------------------------- |
| `TSL`                  | Get the current bluetooth card's slot index.                                       |
| `TEC <slot> <state?>`  | Get or set serial echo to show AT commands and indications.                        |
| `TTX <slot> <command>` | Send an AT command directly to this bluetooth device.                              |
| `TNA <slot> <name?>`   | Get or set name of this bluetooth device.                                          |
| `TPE <slot> <state?>`  | Get or set PIN enabled (required).                                                 |
| `TPN <slot> <pin?>`    | Get or set pairing PIN.                                                            |
| `TCN <slot>`           | Get connection status.                                                             |
| `TMO <slot>`           | Get play mode. Eg: `playing`, `paused`, `stopped`, `fast-forwarding`, `rewinding`. |
| `TPL <slot>`           | Play                                                                               |
| `TPA <slot>`           | Pause                                                                              |
| `TST <slot>`           | Stop                                                                               |
| `TPR <slot>`           | Previous track                                                                     |
| `TNE <slot>`           | Next track                                                                         |
| `TIN <slot>`           | Get track title, artist, and album.                                                |

### Bluetooth responses/events

| Message                        | Description                                                                    |
| :----------------------------- | :----------------------------------------------------------------------------- |
| `[TSL] <slot>`                 | The current bluetooth card's slot index.                                       |
| `[TEC] <slot> <state>`         | Status of echo to/from this bluetooth device.                                  |
| `[TTX] <slot> <command>`       | An AT command sent to the Bluetooth device.                                    |
| `[TRX] <slot> <indication>`    | An AT indication (response) received from the Bluetooth device.                |
| `[TNA] <slot> <name>`          | Name of this bluetooth device.                                                 |
| `[TPE] <slot> <state>`         | PIN enabled (required).                                                        |
| `[TPN] <slot> <pin>`           | Pairing PIN.                                                                   |
| `[TCN] <slot> <status>`        | Connection status.                                                             |
| `[TMO] <slot> <mode>`          | Play mode. Eg: `playing`, `paused`, `stopped`, `fast-forwarding`, `rewinding`. |
| `[TTM] <slot> <time> <length>` | AVRCP Track time and length in seconds.                                        |
| `[TTI] <slot> <title>`         | AVRCP track title.                                                             |
| `[TAR] <slot> <atist>`         | AVRCP artist.                                                                  |
| `[TAL] <slot> <album>`         | AVRCP album.                                                                   |

**Note:** Internally, the bluetooth device uses AT commands. The echo commands allow users to communicate directly with it for debugging.

See the the [FSC-BT1026E](https://www.feasycom.com/qcc5125-bluetooth-5-audio-module) (based on the Qualcomm QCC5125) and [FSC_BT100X_Sink_Programming_User_Guide_V3.7.pdf](https://d1p0junyotb70z.cloudfront.net/wp-content/uploads/2022/03/1648545679-FSC_BT100X_Sink_Programming_User_Guide_V3.7.pdf) for a list of AT commands.

## Clock module

### Clock commands

| Command           | Description                           |
| :---------------- | :------------------------------------ |
| `CDT <datetime?>` | Get or set date/time.                 |
| `CTK <mode?>`     | `<mode>` can be: `off`, `min`, `sec`. |

### Clock responses/events

| Message            | Description              |
| :----------------- | :----------------------- |
| `[CDT] <datetime>` | Date/time.               |
| `[CTK] <mode>`     | Clock tick event status. |

## Effects module

### Effects commands

| Command        | Description                                       |
| :------------- | :------------------------------------------------ |
| `FPL <effect>` | Play sound effect. Eg: beep or XDR toneburst etc. |

### Effects responses/events

| Message | Description      |
| :------ | :--------------- |
| `[FST]` | Effect complete. |

## Equaliser module

### Equaliser commands

| Command                | Description                                               |
| :--------------------- | :-------------------------------------------------------- |
| `QON <state?>`         | Get or set EQ On/Off state.                               |
| `QFR <frequencies[]?>` | Get or set comma-separated list of EQ center frequencies. |
| `QLV <levels[]?>`      | Get or set comma-separated list of EQ levels.             |

### Equaliser responses/events

| Message                 | Description                                   |
| :---------------------- | :-------------------------------------------- |
| `[QON] <state>`         | EQ On/Off state.                              |
| `[QFR] <frequencies[]>` | Pipe-separated list of EQ center frequencies. |
| `[QLV] <levels[]>`      | Pipe-separated list of EQ levels.             |

## Media module

These commands/messages are common among USB and SD.

### Media commands

| Command                  | Description                                                                            |
| :----------------------- | :------------------------------------------------------------------------------------- |
| `MDV <drive?>`           | Get or set drive (USB or SD).                                                          |
| `MPT <path?>`            | Get or set path.                                                                       |
| `MCO`                    | Get file count in folder.                                                              |
| `MLS <offset?> <limit?>` | Get a single page of files in the folder.                                              |
| `MMO <action?>`          | Get/set play mode. Eg: `playing`, `paused`, `stopped`, `fast-forwarding`, `rewinding`. |
| `MPL`                    | Play                                                                                   |
| `MPA`                    | Pause                                                                                  |
| `MST`                    | Stop                                                                                   |
| `MPR`                    | Previous track                                                                         |
| `MNE`                    | Next track                                                                             |
| `MRW <state>`            | Rewind (on/off)                                                                        |
| `MFF <state>`            | Fast-forward (on/off)                                                                  |
| `MSH <mode?>`            | Get or set shuffle mode.                                                               |
| `MRP <mode?>`            | Get or set repeat mode.                                                                |

### Media responses/events

| Message                          | Description                                                   |
| :------------------------------- | :------------------------------------------------------------ |
| `[MDD] <drive>`                  | Drive detected (USB or SD).                                   |
| `[MDR] <drive>`                  | Drive removed (USB or SD).                                    |
| `[MDV] <drive>`                  | Drive (USB or SD).                                            |
| `[MPT] <path>`                   | Path.                                                         |
| `[MCO] <total>`                  | File count in folder.                                         |
| `[MPH] <total> <offset> <limit>` | Page header.                                                  |
| `[MPI] <file>`                   | Page item. Multiple page item events can be emitted per page. |
| `[MMO] <mode>`                   | Play mode. Eg: `playing`, `paused`, `stopped`.                |
| `[MSH] <mode>`                   | Shuffle mode.                                                 |
| `[MRP] <mode>`                   | Repeat mode.                                                  |
| `[MTM] <time> <length>`          | Track time and length in seconds.                             |
| `[MNO] <number>`                 | ID3 Track number.                                             |
| `[MTT] <title>`                  | ID3 track title.                                              |
| `[MAR] <artist>`                 | ID3 artist.                                                   |
| `[MAL] <album>`                  | ID3 album.                                                    |
| `[MYR] <year>`                   | ID3 year.                                                     |

## Power module

### Power commands

| Command        | Description                        |
| :------------- | :--------------------------------- |
| `PBT <state?>` | Get or set battery relay output.   |
| `PAC`          | Get accessories optocoupler input. |

### Power responses/events

| Message         | Description                    |
| :-------------- | :----------------------------- |
| `[PBT] <state>` | Battery relay output.          |
| `[PAC] <state>` | Accessories optocoupler input. |

## Radio module

### Radio commands

The system supports multiple radio I/O cards installed at once.

After the `[DID]` I/O card event, the radio card for that slot is instansiated and subsequence events can be handled.

| Command                   | Description                                                 |
| :------------------------ | :---------------------------------------------------------- |
| `RSL`                     | Get the current radio card's slot index.                    |
| `RBL <slot>`              | Get list of supported bands.                                |
| `RBA <slot> <band?>`      | Get or set band.                                            |
| `RFR <slot> <frequency?>` | Get or set frequency (prefix with + or - to make relative). |
| `RPR <slot>`              | Seek down.                                                  |
| `RNE <slot>`              | Seek up.                                                    |
| `RDS <slot>`              | Get RDS message.                                            |
| `RST <slot>`              | Get stereo mode.                                            |
| `RSI <slot>`              | Get signal strength (0..4).                                 |
| `RSU <slot> <state?>`     | Get or set status update state (on/off).                    |

### Radio responses/events

| Message                    | Description                             |
| :------------------------- | :-------------------------------------- |
| `[RSL]`                    | The current radio card's slot index.    |
| `[RBL] <slot> <bands[]>`   | Pipe-separated list of supported bands. |
| `[RBA] <slot> <band>`      | Band. Eg: AM or FM.                     |
| `[RFR] <slot> <frequency>` | Frequency.                              |
| `[RDS] <slot> <message>`   | RDS message.                            |
| `[RST] <slot> <state>`     | Stereo mode.                            |
| `[RSI] <slot> <level>`     | Signal strength.                        |
| `[RSU] <slot> <state>`     | Status update state.                    |

### Settings commands

Eg: an important setting is the `ioc` setting. Eg: `-,RAD,BLU,LIO,LOT`.

Settings that are io-card specific will have an index appended to their key. Eg: `bands|1` for the radio bands on slot 1.

| Message               | Description                    |
| :-------------------- | :----------------------------- |
| `NRD`                 | Read all from EEPROM.          |
| `NWR`                 | Write all to EEPROM.           |
| `NSE <key?> <value?>` | Get all or a specific setting. |
| `NSX <key>`           | Remove a setting.              |

### Settings events

| Message               | Description                                      |
| :-------------------- | :----------------------------------------------- |
| `[NRD]`               | Acknowledge all read from EEPROM.                |
| `[NWR]`               | Acknowledge all Written to EEPROM.               |
| `[NSE] <key> <value>` | A setting value.                                 |
| `[NSC]`               | Indicates that the settings listing is complete. |
| `[NSX] <key>`         | Acknowledge the setting was removed.             |

### Example settings

```
NSE ioc -,RAS,BLU,LIO,LOT
NSE idx 0
NSE ovl 255
NSE omu off
NSE oba 0
NSE ofa 0
NSE qon off
NSE qfr 50,6300
NSE qlv 12,3
NSE uvm waveform
NSE uvf off
NSE rad:1 0x11
NSE rbl:1 a,f
NSE rba:1 f
NSE rfr:1 a531,f9770
NSE rpr:1 f9090,f9250,f9410,f9770,f10290,f10770
```

## System module

The system module runs on the Main Unit, but many of the system commands are actually processed by other modules on other units.

### System commands

| Command                | Description                                                                                                                     |
| :--------------------- | :------------------------------------------------------------------------------------------------------------------------------ |
| `SVE`                  | Get firmware version. Should trigger an event from both the System and UI modules.                                              |
| `SYS <action?> <key?>` | `start`, or `stop` all, or a specified module.                                                                                  |
| `SLS`                  | List all known modules.                                                                                                         |
| `SAK <key>`            | Requests that a given module acknowledges that the command was received. Useful to check a connection before starting a module. |

### System responses

| Message                         | Description                                              |
| :------------------------------ | :------------------------------------------------------- |
| `[SLG] <level> <key> <message>` | System log. \[I\]nfo, \[W\]arning, \[E\]rror.            |
| `[SVE] <unit> <version>`        | Firmware version.                                        |
| `[SYS] <status> <key>`          | Status of a module.                                      |
| `[SLS] <keys[]>`                | Pipe-separated list of modules (keys).                   |
| `[SAK] <key>`                   | Acknowledges that the given module recieved the command. |

## See also:

- [Serial commands](./messages.md)
- [Serial messages - Front Panel (FP10, FP20)](./messages-front-panel.md)
