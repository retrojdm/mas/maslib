/ [Home](../../../README.md) / [Serial commands](../README.md) / List of Front Panel commands

# Modular Audio System - List of Front Panel (FP10, FP20) Serial Messages

## Software modules

The following modules usually run on the Front Panel (although it's entirely up to each Front Panel's firmware):

| Key | Module name | Responsibilities                                                                  |
| :-- | :---------- | --------------------------------------------------------------------------------- |
| `B` | Buttons     | Emits button events.                                                              |
| `E` | Encoders    | Emits rotary encoder events.                                                      |
| `L` | LED         | Handles LED output.                                                               |
| `U` | UI          | Handles user interface screens, controls, and the display.                        |

### Why isn't the display a separate module?

The display is usually part of the UI module to avoid supporting large amounts of data on the message queue. For example: a Front Panel that uses a 256 x 64px OLED would need about 3K of base64 data _per message_ to render a full frame.

The Modular Audio System's interchangeable front panels can accommodate significantly diverse hardware, each with multiple display variants. These variants all necessitate distinct user interfaces (UIs). It's hard to avoid tight coupling between UIs and displays.

In contrast, the Button and Encoder modules are separate from the UI, primarily so that we can see their events on the serial monitor.

## Encoders module

### Encoder events

| Message                 | Description                               |
| :---------------------- | :---------------------------------------- |
| `[ENC] <index> <steps>` | Encoder changed. +/- indicated direction. |

## Buttons module

### Button events

| Message         | Description     |
| :-------------- | :-------------- |
| `[BDN] <index>` | Button down.    |
| `[BUP] <index>` | Button up.      |
| `[BPR] <index>` | Button pressed. |
| `[BHL] <index>` | Button held.    |

## LED module

### LED commands

LEDs can be PWM controlled for brightness. 0 = off; 255 = full brightness.

| Message           | Description                                               |
| :---------------- | :-------------------------------------------------------- |
| `LED <levels[]?>` | Get or set a comma-separated list of LED levels (0..255). |

### LED events

| Message            | Description                                    |
| :----------------- | :--------------------------------------------- |
| `[LED] <levels[]>` | A comma-separated list of LED levels (0..255). |

## UI module

The UI module controls everything, and orchestrates all of the other modules.

See the firmware in each front panel project:

- [FP10](https://gitlab.com/retrojdm/mas/FP10)
- [FP20](https://gitlab.com/retrojdm/mas/FP20)

### UI commands

| Message        | Description                                |
| :------------- | :----------------------------------------- |
| `UVM <state?>` | Get or set visualisation mode.             |
| `UVF <state?>` | Get or set visualisation full-screen.      |

### UI events

| Message         | Description                     |
| :-------------- | :------------------------------ |
| `[UVM] <state>` | Visualisation mode.             |
| `[UVF] <state>` | Visualisation full-screen.      |

## See also:

- [Serial commands](./messages.md)
- [Serial messages - Main Unit (MU50)](./messages-main-unit.md)
