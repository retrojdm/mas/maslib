#include <gtest/gtest.h>

#include "../../src/Helpers/ArrayHelper.h"

namespace mas
{
  TEST(ArrayHelperTests, findIndex_empty)
  {
    const size_t count = 0;
    const char* names[0] = { };
    const char* q = "Ernie";
    int16_t index = ArrayHelper::findIndex(names, count, [q](const char* x) { return strcmp(x, q) == 0; });
    EXPECT_EQ(index, -1);
  }

  TEST(ArrayHelperTests, findIndex_first)
  {
    const size_t count = 3;
    const char* names[count] = { "Ernie", "Bert", "Gonzo" };
    const char* q = "Ernie";
    int16_t index = ArrayHelper::findIndex(names, count, [q](const char* x) { return strcmp(x, q) == 0; });
    EXPECT_EQ(index, 0);
  }

  TEST(ArrayHelperTests, findIndex_last)
  {
    const size_t count = 3;
    const char* names[count] = { "Ernie", "Bert", "Gonzo" };
    const char* q = "Gonzo";
    int16_t index = ArrayHelper::findIndex(names, count, [q](const char* x) { return strcmp(x, q) == 0; });
    EXPECT_EQ(index, 2);
  }

  TEST(ArrayHelperTests, findIndex_notFound)
  {
    const size_t count = 3;
    const char* names[count] = { "Ernie", "Bert", "Gonzo" };
    const char* q = "Trevor";
    int16_t index = ArrayHelper::findIndex(names, count, [q](const char* x) { return strcmp(x, q) == 0; });
    EXPECT_EQ(index, -1);
  }

  TEST(ArrayHelperTests, findIndex_numbers)
  {
    const size_t count = 3;
    const uint16_t names[count] = { 123, 456, 789 };
    const uint16_t q = 456;
    int16_t index = ArrayHelper::findIndex(names, count, [q](const uint16_t x) { return x == q; });
    EXPECT_EQ(index, 1);
  }

  TEST(ArrayHelperTests, findIndex_firstMatch)
  {
    const size_t count = 5;
    const uint16_t names[count] = { 123, 456, 456, 456, 789 };
    const uint16_t q = 456;
    int16_t index = ArrayHelper::findIndex(names, count, [q](const uint16_t x) { return x == q; });
    EXPECT_EQ(index, 1);
  }
}