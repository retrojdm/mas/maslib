#include <gtest/gtest.h>

#include "../../src/Helpers/ValidationHelper.h"

namespace mas
{
  TEST(ValidationHelperTests, tryParseArrayOfIntegers_uint16)
  {
    char error[256];
    const uint8_t size = 8;
    uint16_t buffer[size];

    Array<uint16_t> array(buffer, size);

    const char * value = "63,3500";

    bool success = ValidationHelper::tryParseArrayOfIntegers(
      value, PSTR("value"), (uint16_t)0, (uint16_t)65535, IntegerFormats::U, &array, error);

    EXPECT_TRUE(success);
    EXPECT_EQ(array.length(), 2);
    EXPECT_EQ(array[0], 63);
    EXPECT_EQ(array[1], 3500);
  }

  TEST(ValidationHelperTests, tryParseArrayOfIntegers_int8)
  {
    char error[256];
    const uint8_t size = 8;
    int8_t buffer[size];

    Array<int8_t> array(buffer, size);

    const char * value = "12,-1";

    bool success = ValidationHelper::tryParseArrayOfIntegers(
      value, PSTR("value"), (int8_t)-128, (int8_t)127, IntegerFormats::U, &array, error);

    EXPECT_TRUE(success);
    EXPECT_EQ(array.length(), 2);
    EXPECT_EQ(array[0], 12);
    EXPECT_EQ(array[1], -1);
  }
}