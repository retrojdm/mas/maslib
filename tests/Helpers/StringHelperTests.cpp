#include <gtest/gtest.h>

#include "../../src/Constants/DataSize.h"
#include "../../src/Helpers/StringHelper.h"

namespace mas
{
  TEST(StringHelperTests, join)
  {
    const size_t count = 5;
    const char* names[count] = { "Ernie", "Bert", "Gonzo", "", "" };
    char result[DataSize::MESSAGE_PARAMETERS_SIZE];
    StringHelper::join('|', names, 3, result);
    EXPECT_STREQ(result, "Ernie|Bert|Gonzo");
  }

  TEST(StringHelperTests, split_empty)
  {
    const size_t MAX_COUNT = 5;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char buffer[MAX_COUNT][DataSize::MESSAGE_PARAMETERS_SIZE];
 
    char * ptrBuffer[MAX_COUNT];
    ArrayHelper::convertToPointerArray(buffer, ptrBuffer);

    Array<char *> array(ptrBuffer, MAX_COUNT);    
    bool success = StringHelper::split<DataSize::MESSAGE_PARAMETERS_SIZE, DataSize::MESSAGE_PARAMETERS_SIZE>("", '|', array, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(array.length(), 0);
  }

  TEST(StringHelperTests, split_list)
  {
    const size_t MAX_COUNT = 5;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char buffer[MAX_COUNT][DataSize::MESSAGE_PARAMETERS_SIZE];
    
    char * ptrBuffer[MAX_COUNT];
    ArrayHelper::convertToPointerArray(buffer, ptrBuffer);

    Array<char *> array(ptrBuffer, MAX_COUNT);
    bool success = StringHelper::split<DataSize::MESSAGE_PARAMETERS_SIZE, DataSize::MESSAGE_PARAMETERS_SIZE>("Ernie|Bert|Gonzo", '|', array, error);
    EXPECT_TRUE(success);
    EXPECT_EQ(array.size(), MAX_COUNT);
    EXPECT_EQ(array.length(), 3);
    EXPECT_STREQ(array[0], "Ernie");
    EXPECT_STREQ(array[1], "Bert");
    EXPECT_STREQ(array[2], "Gonzo");
  }
}