#include <gtest/gtest.h>
#include <new>
#include "../../src/Constants/DataSize.h"
#include "../../src/Constants/Localisation.h"
#include "../../src/Constants/SettingKeys.h"
#include "../../src/Helpers/SerialisationHelper.h"
#include "../../src/Messages/Analyser/Commands/AnalyserModeCommand.h"
#include "../../src/Models/Relative.h"
#include "../TestModels/TestModel.h"

namespace mas
{
  // isHexChar /////////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, isHexChar)
  {
    EXPECT_TRUE(SerialisationHelper::isHexChar('0'));
    EXPECT_TRUE(SerialisationHelper::isHexChar('9'));
    EXPECT_TRUE(SerialisationHelper::isHexChar('a'));
    EXPECT_TRUE(SerialisationHelper::isHexChar('f'));
    EXPECT_TRUE(SerialisationHelper::isHexChar('A'));
    EXPECT_TRUE(SerialisationHelper::isHexChar('F'));
    
    EXPECT_FALSE(SerialisationHelper::isHexChar('-'));
    EXPECT_FALSE(SerialisationHelper::isHexChar(' '));
    EXPECT_FALSE(SerialisationHelper::isHexChar('\0'));
    EXPECT_FALSE(SerialisationHelper::isHexChar('G'));
    EXPECT_FALSE(SerialisationHelper::isHexChar(';'));
    EXPECT_FALSE(SerialisationHelper::isHexChar('\n'));
  }

  // serialise /////////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, serialise_noParams)
  {
    char result[DataSize::MESSAGE_SIZE];
    AnalyserModeCommand * pMessage = new(std::nothrow) AnalyserModeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "AMO");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_char)
  {
    char result[DataSize::MESSAGE_SIZE];
    SystemAcknowledgeEvent * pMessage = new(std::nothrow) SystemAcknowledgeEvent('S');
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[SAK] S");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_datetime)
  {
    // See https://www.unixtimestamp.com/
    char result[DataSize::MESSAGE_SIZE];
    time_t t = 1711935928;
    ClockDatetimeEvent * pMessage = new(std::nothrow) ClockDatetimeEvent(t);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[CDT] 2024-04-01T14:45:28");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_enum)
  {
    char result[DataSize::MESSAGE_SIZE];
    AnalyserModeCommand * pMessage = new(std::nothrow) AnalyserModeCommand(AnalyserMode::Off);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "AMO off");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_integer)
  {
    char result[DataSize::MESSAGE_SIZE];
    AudioInputInfoCommand * pMessage = new(std::nothrow) AudioInputInfoCommand(4);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "INF 4");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_relativeIntegerOptionalValue)
  {
    char result[DataSize::MESSAGE_SIZE];
    AudioOutputVolumeCommand * pMessage = new(std::nothrow) AudioOutputVolumeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "OVL");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_relativeIntegerNormalValue)
  {
    char result[DataSize::MESSAGE_SIZE];
    Relative<int16_t> volume(192, false);
    AudioOutputVolumeCommand * pMessage = new(std::nothrow) AudioOutputVolumeCommand(volume);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "OVL 192");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_relativeIntegerIncrementValue)
  {
    char result[DataSize::MESSAGE_SIZE];
    Relative<int16_t> volume(16, true);
    AudioOutputVolumeCommand * pMessage = new(std::nothrow) AudioOutputVolumeCommand(volume);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "OVL +16");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_relativeIntegerDecrementValue)
  {
    char result[DataSize::MESSAGE_SIZE];
    Relative<int16_t> volume(-16, true);
    AudioOutputVolumeCommand * pMessage = new(std::nothrow) AudioOutputVolumeCommand(volume);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "OVL -16");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_stringNormal)
  {
    char result[DataSize::MESSAGE_SIZE];
    SystemVersionEvent * pMessage = new(std::nothrow) SystemVersionEvent("MU50", "0.1.0");
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[SVE] MU50 0.1.0");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_stringQuotedAndEscaped)
  {
    char result[DataSize::MESSAGE_SIZE];
    BluetoothNameEvent * pMessage = new(std::nothrow) BluetoothNameEvent(2, "I\\O test");
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[TNA] 2 \"I\\\\O test\"");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_multipleParams)
  {
    char result[DataSize::MESSAGE_SIZE];
    AnalyserModeEvent * pMessage = new(std::nothrow) AnalyserModeEvent(AnalyserMode::FFT, true, 20, 16);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[AMO] fft stereo 20 16");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_arrayOfChars)
  {
    char result[DataSize::MESSAGE_SIZE];
    char buffer[SystemListEvent::KEY_COUNT_MAX];
    Array<char> keys(buffer, SystemListEvent::KEY_COUNT_MAX);
    keys.add('S');
    keys.add('I');
    keys.add('O');
    keys.add('D');
    SystemListEvent * pMessage = new(std::nothrow) SystemListEvent(keys);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[SLS] S,I,O,D");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_arrayOfEnum)
  {
    char result[DataSize::MESSAGE_SIZE];
    AudioInputType buffer[DataSize::AUDIO_INPUT_MAX];
    Array<AudioInputType> types(buffer, DataSize::AUDIO_INPUT_MAX);
    types.add(AudioInputType::PC);
    types.add(AudioInputType::Media);
    types.add(AudioInputType::LineIn);
    types.add(AudioInputType::Radio);
    types.add(AudioInputType::Bluetooth);
    AudioInputListEvent * pMessage = new(std::nothrow) AudioInputListEvent(types);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[ILS] PC,MED,LIN,RAD,BLU");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_arrayOfInteger)
  {
    char result[DataSize::MESSAGE_SIZE];
    uint16_t buffer[EqualiserFrequenciesCommand::FREQUENCY_COUNT_MAX];
    Array<uint16_t> levels(buffer, EqualiserFrequenciesCommand::FREQUENCY_COUNT_MAX);
    levels.add(60);
    levels.add(125);
    levels.add(250);
    levels.add(550);
    levels.add(1000);
    levels.add(3500);
    levels.add(10000);
    EqualiserFrequenciesCommand * pMessage = new(std::nothrow) EqualiserFrequenciesCommand(levels);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "QFR 60,125,250,550,1000,3500,10000");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_base64)
  {
    uint8_t buffer[DataSize::ANALYSER_MONO];
    Array<uint8_t> data(buffer, DataSize::ANALYSER_MONO);
    data.add((uint8_t)0);
    data.add(15);
    data.add(31);
    data.add(47);
    data.add(63);
    char result[DataSize::MESSAGE_SIZE];
    AnalyserWaveformMonoEvent * pMessage = new(std::nothrow) AnalyserWaveformMonoEvent(data);
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[AWM] APfv/");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, serialise_systemLogError)
  {
    char result[DataSize::MESSAGE_SIZE];
    SystemLogEvent * pMessage = new(std::nothrow) SystemLogEvent(LogLevel::Error, 'X', "The radio is pooched.");
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::serialise(pMessage, result);
    EXPECT_STREQ(result, "[SLG] E X \"The radio is pooched.\"");
    delete pMessage;
  }

  // tryParse //////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  TEST(SerialisationHelperTests, tryParse_empty)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("", pMessage, error);
    EXPECT_FALSE(success);
    EXPECT_EQ(pMessage, nullptr);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_notNull)
  {
    Message * pMessage = new(std::nothrow) AnalyserModeCommand();
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    EXPECT_NE(pMessage, nullptr);
    bool success = SerialisationHelper::tryParse("AMO", pMessage, error);
    EXPECT_FALSE(success);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_invalid)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("What the heck!?", pMessage, error);
    EXPECT_FALSE(success);
    EXPECT_EQ(pMessage, nullptr);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_codeNotFound)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("XYZ", pMessage, error);
    EXPECT_FALSE(success);
    EXPECT_EQ(pMessage, nullptr);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_code)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("AMO", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AnalyserModeCommand);
    AnalyserModeCommand * pCommand = reinterpret_cast<AnalyserModeCommand *>(pMessage);
    EXPECT_FALSE(pCommand->mode.hasValue());
    EXPECT_FALSE(pCommand->stereo.hasValue());
    EXPECT_FALSE(pCommand->fps.hasValue());
    EXPECT_FALSE(pCommand->size.hasValue());
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_tooFewParams)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("DJE", pMessage, error);
    EXPECT_FALSE(success) << error;
  }

  TEST(SerialisationHelperTests, tryParse_tooManyParams)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("DJE 0 Oops", pMessage, error);
    EXPECT_FALSE(success) << error;
  }

  TEST(SerialisationHelperTests, tryParse_charRequired)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("SAK S", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::SystemAcknowledgeCommand);
    SystemAcknowledgeCommand * pCommand = reinterpret_cast<SystemAcknowledgeCommand *>(pMessage);
    EXPECT_EQ(pCommand->key, 'S');
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_datetimeRequired)
  {
    // See https://www.unixtimestamp.com/
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[CDT] 2024-04-01T14:45:28", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::ClockDatetimeEvent);
    ClockDatetimeEvent * pEvent = reinterpret_cast<ClockDatetimeEvent *>(pMessage);
    EXPECT_EQ(pEvent->datetime, 1711935928);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_datetimeOptional)
  {
    // See https://www.unixtimestamp.com/
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("CDT 2024-04-01T14:45:28", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::ClockDatetimeCommand);
    ClockDatetimeCommand * pCommand = reinterpret_cast<ClockDatetimeCommand *>(pMessage);
    EXPECT_TRUE(pCommand->datetime.hasValue());
    EXPECT_EQ(pCommand->datetime.value(), 1711935928);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_enumRequired)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[XEC] on", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::SerialEchoEvent);
    SerialEchoEvent * pEvent = reinterpret_cast<SerialEchoEvent *>(pMessage);
    EXPECT_EQ(pEvent->state, true);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_enumOptional)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("XEC on", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::SerialEchoCommand);
    SerialEchoCommand * pCommand = reinterpret_cast<SerialEchoCommand *>(pMessage);
    EXPECT_TRUE(pCommand->state.hasValue());
    EXPECT_EQ(pCommand->state.value(), true);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_integerRequired)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[APM] 191", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AnalyserPeaksMonoEvent);
    AnalyserPeaksMonoEvent * pEvent = reinterpret_cast<AnalyserPeaksMonoEvent *>(pMessage);
    EXPECT_EQ(pEvent->value, 191);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_integerOptional)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("INF 3", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AudioInputInfoCommand);
    AudioInputInfoCommand * pCommand = reinterpret_cast<AudioInputInfoCommand *>(pMessage);
    EXPECT_TRUE(pCommand->index.hasValue());
    EXPECT_EQ(pCommand->index.value(), 3);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_relativeIntegerAbsolute)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("OVL 67", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AudioOutputVolumeCommand);
    AudioOutputVolumeCommand * pCommand = reinterpret_cast<AudioOutputVolumeCommand *>(pMessage);
    EXPECT_TRUE(pCommand->volume.hasValue());
    EXPECT_FALSE(pCommand->volume.value().relative());
    EXPECT_EQ(pCommand->volume.value().value(), 67);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_relativeIntegerRelative)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("OVL +16", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AudioOutputVolumeCommand);
    AudioOutputVolumeCommand * pCommand = reinterpret_cast<AudioOutputVolumeCommand *>(pMessage);
    EXPECT_TRUE(pCommand->volume.hasValue());
    EXPECT_TRUE(pCommand->volume.value().relative());
    EXPECT_EQ(pCommand->volume.value().value(), 16);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_optionalStringNull)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("TNA 1", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::BluetoothNameCommand);
    BluetoothNameCommand * pCommand = reinterpret_cast<BluetoothNameCommand *>(pMessage);
    EXPECT_EQ(pCommand->slot, 1);
    EXPECT_FALSE(pCommand->name.hasValue());
    EXPECT_STREQ(pCommand->name.value(), nullptr);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_optionalString)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("TNA 1 Zukuphone", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::BluetoothNameCommand);
    BluetoothNameCommand * pCommand = reinterpret_cast<BluetoothNameCommand *>(pMessage);
    EXPECT_EQ(pCommand->slot, 1);
    EXPECT_TRUE(pCommand->name.hasValue());
    EXPECT_STREQ(pCommand->name.value(), "Zukuphone");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_requiredString)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[TNA] 1 Zukuphone", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::BluetoothNameEvent);
    BluetoothNameEvent * pEvent = reinterpret_cast<BluetoothNameEvent *>(pMessage);
    EXPECT_EQ(pEvent->slot, 1);
    EXPECT_STREQ(pEvent->name, "Zukuphone");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_requiredStringQuoted)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[TNA] 1 \"Zuku's phone\"", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::BluetoothNameEvent);
    BluetoothNameEvent * pEvent = reinterpret_cast<BluetoothNameEvent *>(pMessage);
    EXPECT_EQ(pEvent->slot, 1);
    EXPECT_STREQ(pEvent->name, "Zuku's phone");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_arrayOfChar)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[SLS] S,I,O,D", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::SystemListEvent);
    SystemListEvent * pEvent = reinterpret_cast<SystemListEvent *>(pMessage);
    EXPECT_EQ(pEvent->keys.length(), 4);
    EXPECT_EQ(pEvent->keys[0], 'S');
    EXPECT_EQ(pEvent->keys[1], 'I');
    EXPECT_EQ(pEvent->keys[2], 'O');
    EXPECT_EQ(pEvent->keys[3], 'D');
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_arrayOfEnum)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[ILS] PC,MED,LIN,RAD,BLU", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AudioInputListEvent);
    AudioInputListEvent * pEvent = reinterpret_cast<AudioInputListEvent *>(pMessage);
    EXPECT_EQ(pEvent->types.length(), 5);
    EXPECT_EQ(pEvent->types[0], AudioInputType::PC);
    EXPECT_EQ(pEvent->types[1], AudioInputType::Media);
    EXPECT_EQ(pEvent->types[2], AudioInputType::LineIn);
    EXPECT_EQ(pEvent->types[3], AudioInputType::Radio);
    EXPECT_EQ(pEvent->types[4], AudioInputType::Bluetooth);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_arrayOfInteger)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("LED 0,51,102,153,204,255", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::LedLevelsCommand);
    LedLevelsCommand * pCommand = reinterpret_cast<LedLevelsCommand *>(pMessage);
    EXPECT_EQ(pCommand->levels.length(), 6);
    EXPECT_EQ(pCommand->levels[0], 0);
    EXPECT_EQ(pCommand->levels[1], 51);
    EXPECT_EQ(pCommand->levels[2], 102);
    EXPECT_EQ(pCommand->levels[3], 153);
    EXPECT_EQ(pCommand->levels[4], 204);
    EXPECT_EQ(pCommand->levels[5], 255);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_base64)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[AWM] APfv/", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AnalyserWaveformMonoEvent);
    AnalyserWaveformMonoEvent * pEvent = reinterpret_cast<AnalyserWaveformMonoEvent *>(pMessage);
    EXPECT_EQ(pEvent->data.length(), 5);
    EXPECT_EQ(pEvent->data[0], 0);
    EXPECT_EQ(pEvent->data[1], 15);
    EXPECT_EQ(pEvent->data[2], 31);
    EXPECT_EQ(pEvent->data[3], 47);
    EXPECT_EQ(pEvent->data[4], 63);
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_systemLogError)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("[SLG] E X \"The radio is pooched.\"", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::SystemLogEvent);
    SystemLogEvent * pEvent = reinterpret_cast<SystemLogEvent *>(pMessage);
    EXPECT_EQ(pEvent->level, LogLevel::Error);
    EXPECT_EQ(pEvent->key, 'X');
    EXPECT_STREQ(pEvent->message, "The radio is pooched.");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, tryParse_optionalValuesNull)
  {
    Message * pMessage = nullptr;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParse("AMO peaks stereo", pMessage, error);
    EXPECT_TRUE(success) << error;
    ASSERT_NE(pMessage, nullptr);
    EXPECT_EQ(pMessage->type(), MessageType::AnalyserModeCommand);
    AnalyserModeCommand * pCommand = reinterpret_cast<AnalyserModeCommand *>(pMessage);
    EXPECT_TRUE(pCommand->mode.hasValue());
    EXPECT_EQ(pCommand->mode.value(), AnalyserMode::Peaks);
    EXPECT_TRUE(pCommand->stereo.hasValue());
    EXPECT_TRUE(pCommand->stereo.value());
    EXPECT_FALSE(pCommand->fps.hasValue());
    EXPECT_FALSE(pCommand->size.hasValue());
    delete pMessage;
  }

  // tryParseHex ///////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, tryParseHex_empty)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("", output, error);
    EXPECT_FALSE(success);
    EXPECT_STREQ(error, Localisation::ERROR_MISSING_HEX_PREFIX);
  }

  TEST(SerialisationHelperTests, tryParseHex_missingPrefix)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0", output, error);
    EXPECT_FALSE(success);
    EXPECT_STREQ(error, Localisation::ERROR_MISSING_HEX_PREFIX);
  }

  TEST(SerialisationHelperTests, tryParseHex_zero)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0x00", output, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(output, 0x00);
  }

  TEST(SerialisationHelperTests, tryParseHex_4bit)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0xA", output, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(output, 0xA);
  }

  TEST(SerialisationHelperTests, tryParseHex_8bit)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0xFB", output, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(output, 0xFB);
  }

  TEST(SerialisationHelperTests, tryParseHex_16bit)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0xFB02", output, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(output, 0xFB02);
  }

  TEST(SerialisationHelperTests, tryParseHex_32bit)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0x80001012", output, error);
    EXPECT_TRUE(success) << error;
    EXPECT_EQ(output, 0x80001012);
  }

  TEST(SerialisationHelperTests, tryParseHex_tooManyChars)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0x800010123", output, error);
    EXPECT_FALSE(success);
  }

  TEST(SerialisationHelperTests, tryParseHex_invalidChars)
  {
    uint32_t output;
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    bool success = SerialisationHelper::tryParseHex("0xABCDEFGH", output, error);
    EXPECT_FALSE(success);
  }

  // tryParseSlotSettingKey ////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, tryParseSlotSettingKey_empty)
  {
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char key[DataSize::SETTING_KEY_SIZE];
    int8_t slot;
    bool success = SerialisationHelper::tryParseSlotSettingKey("", key, slot, error);
    EXPECT_STREQ(key, "");
    EXPECT_EQ(slot, -1);
    EXPECT_TRUE(success);
  }

  TEST(SerialisationHelperTests, tryParseSlotSettingKey_key)
  {
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char key[DataSize::SETTING_KEY_SIZE];
    int8_t slot;
    bool success = SerialisationHelper::tryParseSlotSettingKey("ioc", key, slot, error);
    EXPECT_STREQ(key, "ioc");
    EXPECT_EQ(slot, -1);
    EXPECT_TRUE(success);
  }

  TEST(SerialisationHelperTests, tryParseSlotSettingKey_keyAndEmpty)
  {
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char key[DataSize::SETTING_KEY_SIZE];
    int8_t slot;
    bool success = SerialisationHelper::tryParseSlotSettingKey("rad:", key, slot, error);
    EXPECT_STREQ(key, "rad");
    EXPECT_EQ(slot, -1);
    EXPECT_TRUE(success);
  }

  TEST(SerialisationHelperTests, tryParseSlotSettingKey_keyAndSlot)
  {
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char key[DataSize::SETTING_KEY_SIZE];
    int8_t slot;
    bool success = SerialisationHelper::tryParseSlotSettingKey("rad:1", key, slot, error);
    EXPECT_STREQ(key, "rad");
    EXPECT_EQ(slot, 1);
    EXPECT_TRUE(success);
  }

  // escape ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, escape_empty)
  {
    char result[10];
    char* pOut = result;
    int16_t outCharsLeft = 9;
    SerialisationHelper::escape("", pOut, outCharsLeft);
    EXPECT_STREQ(result, "\"\"");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  TEST(SerialisationHelperTests, escape_string)
  {
    char result[10];
    char* pOut = result;
    int16_t outCharsLeft = sizeof(result) - 1;
    SerialisationHelper::escape("Hellorld", pOut, outCharsLeft);
    EXPECT_STREQ(result, "Hellorld");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  TEST(SerialisationHelperTests, escape_space)
  {
    char result[20];
    char* pOut = result;
    int16_t outCharsLeft = sizeof(result) - 1;
    SerialisationHelper::escape("Hello world", pOut, outCharsLeft);
    EXPECT_STREQ(result, "\"Hello world\"");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  TEST(SerialisationHelperTests, escape_quotes)
  {
    char result[24];
    char* pOut = result;
    int16_t outCharsLeft = sizeof(result) - 1;
    SerialisationHelper::escape("Frickin \"lasers\".", pOut, outCharsLeft);
    EXPECT_STREQ(result, "\"Frickin \\\"lasers\\\".\"");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  TEST(SerialisationHelperTests, escape_hex)
  {
    char result[10];
    char* pOut = result;
    int16_t outCharsLeft = sizeof(result) - 1;
    SerialisationHelper::escape("\xF8", pOut, outCharsLeft);
    EXPECT_STREQ(result, "\"\\xF8\"");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  TEST(SerialisationHelperTests, escape_longInput)
  {
    char result[10];
    char* pOut = result;
    int16_t outCharsLeft = sizeof(result) - 1;
    SerialisationHelper::escape("0123456789ABCDEF", pOut, outCharsLeft);
    EXPECT_STREQ(result, "012345678");
    EXPECT_EQ(outCharsLeft, sizeof(result) - strlen(result) - 1);
  }

  // usage /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, usage_code)
  {
    char result[64];
    SystemVersionCommand * pMessage = new(std::nothrow) SystemVersionCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "SVE");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_char)
  {
    char result[64];
    SystemAcknowledgeCommand * pMessage = new(std::nothrow) SystemAcknowledgeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "SAK <key>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_datetime)
  {
    char result[64];
    ClockDatetimeEvent * pMessage = new(std::nothrow) ClockDatetimeEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[CDT] <datetime>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_enum)
  {
    char result[64];
    SerialEchoEvent * pMessage = new(std::nothrow) SerialEchoEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[XEC] <state>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_integer)
  {
    char result[64];
    AudioInputIndexEvent * pMessage = new(std::nothrow) AudioInputIndexEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[IDX] <index>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalChar)
  {
    char result[64];
    SystemStatusCommand * pMessage = new(std::nothrow) SystemStatusCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "SYS <action?> <key?>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalDatetime)
  {
    char result[64];
    ClockDatetimeCommand * pMessage = new(std::nothrow) ClockDatetimeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "CDT <datetime?>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalEnum)
  {
    char result[64];
    SerialEchoCommand * pMessage = new(std::nothrow) SerialEchoCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "XEC <state?>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalInteger)
  {
    char result[64];
    AudioInputInfoCommand * pMessage = new(std::nothrow) AudioInputInfoCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "INF <index?>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalRelativeInteger)
  {
    char result[64];
    AudioOutputVolumeCommand * pMessage = new(std::nothrow) AudioOutputVolumeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "OVL <volume?>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_string)
  {
    char result[64];
    SystemVersionEvent * pMessage = new(std::nothrow) SystemVersionEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[SVE] <unit> <version>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_arrayOfChar)
  {
    char result[64];
    SystemListEvent * pMessage = new(std::nothrow) SystemListEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[SLS] <keys[]>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_arrayOfEnum)
  {
    char result[64];
    AudioInputListEvent * pMessage = new(std::nothrow) AudioInputListEvent();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "[ILS] <types[]>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_arrayOfInteger)
  {
    char result[64];
    LedLevelsCommand * pMessage = new(std::nothrow) LedLevelsCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "LED <levels[]>");
    delete pMessage;
  }

  TEST(SerialisationHelperTests, usage_optionalEnumsAndIntegers)
  {
    char result[64];
    AnalyserModeCommand * pMessage = new(std::nothrow) AnalyserModeCommand();
    ASSERT_NE(pMessage, nullptr);
    SerialisationHelper::usage(pMessage, result);
    EXPECT_STREQ(result, "AMO <mode?> <stereo?> <fps?> <size?>");
    delete pMessage;
  }

  // Byte alignment ////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, byteAlignment)
  {
    EXPECT_EQ(sizeof(int8_t), 1);
    EXPECT_EQ(sizeof(Message), 16);
    EXPECT_EQ(sizeof(time_t), 8);
    EXPECT_EQ(sizeof(ClockDatetimeEvent), 24);
  }

  // Message size //////////////////////////////////////////////////////////////////////////////////////////////////////

  TEST(SerialisationHelperTests, messageSize)
  {
    size_t sizes[MESSAGE_TYPE_COUNT] =
    {
      // Core
      sizeof(SerialEchoCommand),              //  XEC   <state?>
      sizeof(SerialFlowControlCommand),       //  XFC   <state?>
      sizeof(SerialEchoEvent),                // [XEC]  <state>
      sizeof(SerialFlowControlEvent),         // [XFC]  <state>

      // Main Unit

      sizeof(AnalyserModeCommand),            //  AMO   <mode?> <stereo?> <fps?> <size?>
      sizeof(AnalyserModeEvent),              // [AMO]  <mode> <stereo?> <fps?> <size?>
      sizeof(AnalyserPeaksMonoEvent),         // [APM]  <value>
      sizeof(AnalyserPeaksStereoEvent),       // [APS]  <left> <right>
      sizeof(AnalyserFftMonoEvent),           // [AFM]  <data>
      sizeof(AnalyserFftStereoEvent),         // [AFS]  <data>
      sizeof(AnalyserWaveformMonoEvent),      // [AWM]  <data>
      sizeof(AnalyserWaveformStereoEvent),    // [AWS]  <data>

      sizeof(AudioInputPcUsbCommand),         //  IPC
      sizeof(AudioInputListCommand),          //  ILS   <index?>
      sizeof(AudioInputIndexCommand),         //  IDX
      sizeof(AudioInputInfoCommand),          //  INF   <index?>
      sizeof(AudioInputPcUsbEvent),           // [IPC]  <state>
      sizeof(AudioInputListEvent),            // [ILS]  <types[]>
      sizeof(AudioInputIndexEvent),           // [IDX]  <index>
      sizeof(AudioInputInfoEvent),            // [INF]  <index> <slot> <type> <suffix?>

      sizeof(AudioOutputVolumeCommand),       //  OVL   <volume?>
      sizeof(AudioOutputMuteCommand),         //  OMU   <mute?>
      sizeof(AudioOutputBalanceCommand),      //  OBA   <balance?>
      sizeof(AudioOutputFadeCommand),         //  OFA   <fade?>
      sizeof(AudioOutputVolumeEvent),         // [OVL]  <volume>
      sizeof(AudioOutputMuteEvent),           // [OMU]  <mute>
      sizeof(AudioOutputBalanceEvent),        // [OBA]  <balance>
      sizeof(AudioOutputFadeEvent),           // [OFA]  <fade>

      sizeof(BluetoothSerialEchoCommand),     //  TEC   <slot> <state?>
      sizeof(BluetoothSerialTransmitCommand), //  TTX   <slot> <command>
      sizeof(BluetoothNameCommand),           //  TNA   <slot> <name?>
      sizeof(BluetoothPinEnableCommand),      //  TPE   <slot> <state?>
      sizeof(BluetoothPinCommand),            //  TPN   <slot> <pin?>
      sizeof(BluetoothPlayModeCommand),       //  TMO   <slot>
      sizeof(BluetoothPlayCommand),           //  TPL   <slot>
      sizeof(BluetoothPauseCommand),          //  TPA   <slot>
      sizeof(BluetoothStopCommand),           //  TST   <slot>
      sizeof(BluetoothPrevTrackCommand),      //  TPR   <slot>
      sizeof(BluetoothNextTrackCommand),      //  TNE   <slot>
      sizeof(BluetoothTrackInfoCommand),      //  TIN   <slot>
      sizeof(BluetoothSerialEchoEvent),       // [TEC]  <slot> <state>
      sizeof(BluetoothSerialTransmitEvent),   // [TTX]  <slot> <command>
      sizeof(BluetoothSerialReceiveEvent),    // [TRX]  <slot> <indication>
      sizeof(BluetoothNameEvent),             // [TNA]  <slot> <name>
      sizeof(BluetoothPinEnableEvent),        // [TPE]  <slot> <state>
      sizeof(BluetoothPinEvent),              // [TPN]  <slot> <pin>
      sizeof(BluetoothConnectionEvent),       // [TCN]  <slot> <status>
      sizeof(BluetoothPlayModeEvent),         // [TMO]  <slot> <mode>
      sizeof(BluetoothTrackTimeEvent),        // [TTM]  <slot> <time> <length>
      sizeof(BluetoothTitleEvent),            // [TTI]  <slot> <title>
      sizeof(BluetoothArtistEvent),           // [TAR]  <slot> <atist>
      sizeof(BluetoothAlbumEvent),            // [TAL]  <slot> <album>

      sizeof(ClockDatetimeCommand),           //  CDT   <datetime?>
      sizeof(ClockTickModeCommand),           //  CTK   <mode?>
      sizeof(ClockDatetimeEvent),             // [CDT]  <datetime>
      sizeof(ClockTickModeEvent),             // [CTK]  <mode>

      sizeof(EffectsPlayCommand),             //  FPL   <effect>
      sizeof(EffectsCompleteEvent),           // [FST]

      sizeof(EqualiserOnCommand),             //  QON   <state?>
      sizeof(EqualiserFrequenciesCommand),    //  QFR   <frequencies[]?>
      sizeof(EqualiserLevelsCommand),         //  QLV   <levels[]?>
      sizeof(EqualiserOnEvent),               // [QON]  <state>
      sizeof(EqualiserFrequenciesEvent),      // [QFR]  <frequencies[]>
      sizeof(EqualiserLevelsEvent),           // [QLV]  <levels[]>

      /*
      sizeof(MediaDriveCommand),              //  MDV   <drive?>
      sizeof(MediaPathCommand),               //  MPT   <path?>
      sizeof(MediaFileCountCommand),          //  MCO
      sizeof(MediaListCommand),               //  MLS   <offset?> <limit?>
      sizeof(MediaPlayModeCommand),           //  MMO   <action?>
      sizeof(MediaPlayCommand),               //  MPL
      sizeof(MediaPauseCommand),              //  MPA
      sizeof(MediaStopCommand),               //  MST
      sizeof(MediaPrevTrackCommand),          //  MPR
      sizeof(MediaNextTrackCommand),          //  MNE
      sizeof(MediaRewindCommand),             //  MRW   <state>
      sizeof(MediaFastforwardCommand),        //  MFF   <state>
      sizeof(MediaShuffleCommand),            //  MSH   <mode?>
      sizeof(MediaRepeatCommand),             //  MRP   <mode?>
      sizeof(MediaDriveDetectedEvent),        // [MDD]  <drive>
      sizeof(MediaDriveRemovedEvent),         // [MDR]  <drive>
      sizeof(MediaDriveEvent),                // [MDV]  <drive>
      sizeof(MediaPathEvent),                 // [MPT]  <path>
      sizeof(MediaFileCountEvent),            // [MCO]  <total>
      sizeof(MediaPageHeaderEvent),           // [MPH]  <total> <offset> <limit>
      sizeof(MediaPageItemEvent),             // [MPI]  <file>
      sizeof(MediaPlayModeCommandEvent),      // [MMO]  <mode>
      sizeof(MediaShuffleEvent),              // [MSH]  <mode>
      sizeof(MediaRepeatEvent),               // [MRP]  <mode>
      sizeof(MediaTrackTimeEvent),            // [MTM]  <time> <length>
      sizeof(MediaTrackNumberEvent),          // [MNO]  <number>
      sizeof(MediaTrackTitleEvent),           // [MTT]  <title>
      sizeof(MediaArtistEvent),               // [MAR]  <artist>
      sizeof(MediaAlbumEvent),                // [MAL]  <album>
      sizeof(MediaYearEvent),                 // [MYR]  <year>
      */
    
      sizeof(PowerBatteryCommand),            //  PBT   <state?>
      sizeof(PowerAccessoriesCommand),        //  PAC
      sizeof(PowerBatteryEvent),              // [PBT]  <state>
      sizeof(PowerAccessoriesEvent),          // [PAC]  <state>

      sizeof(RadioBandListCommand),           //  RBL   <slot>
      sizeof(RadioBandCommand),               //  RBA   <slot> <band?>
      sizeof(RadioFrequencyCommand),          //  RFR   <slot> <frequency?>
      sizeof(RadioSeekDownCommand),           //  RPR   <slot>
      sizeof(RadioSeekUpCommand),             //  RNE   <slot>
      sizeof(RadioRdsCommand),                //  RDS   <slot>
      sizeof(RadioStereoCommand),             //  RST   <slot>
      sizeof(RadioSignalStrengthCommand),     //  RSI   <slot>
      sizeof(RadioStatusUpdateCommand),       //  RSU   <slot> <state?>
      sizeof(RadioBandListEvent),             // [RBL]  <slot> <bands[]>
      sizeof(RadioBandEvent),                 // [RBA]  <slot> <band>
      sizeof(RadioFrequencyEvent),            // [RFR]  <slot> <frequency>
      sizeof(RadioRdsEvent),                  // [RDS]  <slot> <message>
      sizeof(RadioStereoEvent),               // [RST]  <slot> <state>
      sizeof(RadioSignalStrengthEvent),       // [RSI]  <slot> <level>
      sizeof(RadioStatusUpdateEvent),         // [RSU]  <slot> <state>

      sizeof(SystemVersionCommand),           //  SVE
      sizeof(SystemStatusCommand),            //  SYS   <action?> <key?>
      sizeof(SystemListCommand),              //  SLS
      sizeof(SystemAcknowledgeCommand),       //  SAK   <key>

      sizeof(SystemLogEvent),                 // [SLG]  <level> <key> <message>
      sizeof(SystemVersionEvent),             // [SVE]  <unit> <version>
      sizeof(SystemStatusEvent),              // [SYS]  <status> <key>
      sizeof(SystemListEvent),                // [SLS]  <keys[]>
      sizeof(SystemAcknowledgeEvent),         // [SAK]  <key>

      // Front Panel

      sizeof(EncoderChangedEvent),            // [ENC]  <index> <steps>

      sizeof(ButtonDownEvent),                // [BDN]  <index>
      sizeof(ButtonUpEvent),                  // [BUP]  <index>
      sizeof(ButtonPressedEvent),             // [BPR]  <index>
      sizeof(ButtonHeldEvent),                // [BHE]  <index>

      sizeof(LedLevelsCommand),               //  LED   <levels[]?>
      sizeof(LedLevelsEvent),                 // [LED]  <levels[]>

      sizeof(UiVisModeCommand),               //  UVM   <mode?>
      sizeof(UiVisFullScreenCommand),         //  UVF   <state?>
      sizeof(UiVisModeEvent),                 // [UVM]  <mode>
      sizeof(UiVisFullScreenEvent),           // [UVF]  <state>

      sizeof(SettingsReadCommand),            //  NRD
      sizeof(SettingsWriteCommand),           //  NWR
      sizeof(SettingsItemCommand),            //  NSE   <key?> <value?>
      sizeof(SettingsRemoveCommand),          //  NSX   <key>
      sizeof(SettingsItemEvent),              // [NSE]  <key> <value>
      sizeof(SettingsListCompleteEvent),      // [NSC]  
      sizeof(SettingsRemoveEvent),            // [NSX]  <key>
    };

    Message *pMessage = nullptr;
    size_t maxSize = 0;

    for (uint16_t i = 0; i < MESSAGE_TYPE_COUNT; i++)
    {
      size_t size = sizes[i];
      maxSize = size > maxSize ? size : maxSize;

      EXPECT_LE(size, DataSize::MESSAGE_SIZE);
    }

    char debug[64];
    sprintf(debug, "\n\nDataSize::MESSAGE_SIZE = %zu\nmaxSize = %zu\n\n", DataSize::MESSAGE_SIZE, maxSize);
    std::cout << debug;
  }
}