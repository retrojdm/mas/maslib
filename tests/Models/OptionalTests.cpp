#include <gtest/gtest.h>

#include "../../src/Models/Optional.h"

namespace mas
{
  TEST(OptionalTests, Optional_defaultConstructor)
  {
    Optional<uint8_t> optional;
    EXPECT_FALSE(optional.hasValue());
  }

  TEST(OptionalTests, Optional_valueConstructor)
  {
    Optional<uint8_t> optional(123);
    EXPECT_TRUE(optional.hasValue());
    EXPECT_EQ(optional.value(), 123);
  }

  TEST(OptionalTests, Optional_assignmentOperator)
  {
    Optional<uint8_t> optional;
    optional = 123;
    EXPECT_TRUE(optional.hasValue());
    EXPECT_EQ(optional.value(), 123);
  }

  TEST(OptionalTests, Optional_copyOperatorNull)
  {
    Optional<uint8_t> optional;
    Optional<uint8_t> other = optional;
    EXPECT_FALSE(other.hasValue());
    EXPECT_EQ(other.value(), 0);
  }

  TEST(OptionalTests, Optional_copyOperatorHasValue)
  {
    Optional<uint8_t> optional(123);
    Optional<uint8_t> other = optional;
    EXPECT_TRUE(other.hasValue());
    EXPECT_EQ(other.value(), 123);
  }

  TEST(OptionalTests, Optional_copyOperatorOverwriteToNull)
  {
    Optional<uint8_t> optional;
    Optional<uint8_t> other(123);
    other = optional;
    EXPECT_FALSE(other.hasValue());
    EXPECT_EQ(other.value(), 0);
  }

  TEST(OptionalTests, Optional_set)
  {
    Optional<uint8_t> optional;
    optional.set(123);
    EXPECT_TRUE(optional.hasValue());
    EXPECT_EQ(optional.value(), 123);
  }

  TEST(OptionalTests, Optional_reset)
  {
    Optional<uint8_t> optional(123);
    optional.reset();
    EXPECT_FALSE(optional.hasValue());
  }

  TEST(OptionalTests, Optional_resetString)
  {
    char buffer[20];
    Optional<char*> optional(buffer, sizeof(buffer), "Hello");

    EXPECT_TRUE(optional.hasValue());
    EXPECT_STREQ(optional.value(), "Hello");

    optional.reset();

    EXPECT_FALSE(optional.hasValue());
    EXPECT_STREQ(optional.value(), nullptr);

    optional.set("There");

    EXPECT_TRUE(optional.hasValue());
    EXPECT_STREQ(optional.value(), "There");
    EXPECT_EQ(optional.value(), buffer);
  }
}