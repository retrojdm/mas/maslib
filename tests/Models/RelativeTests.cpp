#include <gtest/gtest.h>

#include "../../src/Models/Relative.h"

namespace mas
{
  TEST(RelativeTests, Relative_defaultConstructor)
  {
    Relative<uint8_t> relative;
    EXPECT_EQ(relative.value(), 0);
    EXPECT_FALSE(relative.relative());
  }

  TEST(RelativeTests, Relative_valueConstructor)
  {
    Relative<uint8_t> relative(123);
    EXPECT_EQ(relative.value(), 123);
    EXPECT_FALSE(relative.relative());
  }

  TEST(RelativeTests, Relative_valueRelativeConstructor)
  {
    Relative<uint8_t> relative(123, true);
    EXPECT_EQ(relative.value(), 123);
    EXPECT_TRUE(relative.relative());
  }

  TEST(RelativeTests, Relative_set)
  {
    Relative<uint8_t> relative;
    relative.set(123, true);
    EXPECT_EQ(relative.value(), 123);
    EXPECT_TRUE(relative.relative());
  }

  TEST(RelativeTests, Relative_assignmentOperator)
  {
    Relative<uint8_t> relative;
    relative = 123;
    EXPECT_EQ(relative.value(), 123);
    EXPECT_FALSE(relative.relative());
  }

  TEST(RelativeTests, Relative_valueRelativeTo)
  {
    EXPECT_EQ(Relative(16, true).valueRelativeTo(0), 16);
    EXPECT_EQ(Relative(16, false).valueRelativeTo(0), 16); // Not relative. Pass-thru original value.
    EXPECT_EQ(Relative(-16, true).valueRelativeTo(0), -16);
    EXPECT_EQ(Relative(-16, false).valueRelativeTo(0), -16); // Not relative. Pass-thru original value.
    EXPECT_EQ(Relative(16, true).valueRelativeTo(64), 80);
    EXPECT_EQ(Relative(16, false).valueRelativeTo(64), 16); // Not relative. Pass-thru original value.
    EXPECT_EQ(Relative(-16, true).valueRelativeTo(64), 48);
    EXPECT_EQ(Relative(-16, false).valueRelativeTo(64), -16); // Not relative. Pass-thru original value.

    EXPECT_EQ(Relative<int8_t>(-16, true).valueRelativeTo((uint8_t)8), -8);
  }
}