#include <gtest/gtest.h>

#include "../../src/Models/Array.h"

namespace mas
{
  TEST(ArrayTests, Array_constructorByte)
  {
    uint8_t buffer[3];
    Array<uint8_t> array(buffer, 3);
    EXPECT_EQ(array.size(), 3);
    EXPECT_EQ(array.length(), 0);

    EXPECT_EQ(buffer[0], array.data()[0]);
    EXPECT_EQ(buffer[1], array.data()[1]);
    EXPECT_EQ(buffer[2], array.data()[2]);

    EXPECT_EQ(buffer[0], array[0]);
    EXPECT_EQ(buffer[1], array[1]);
    EXPECT_EQ(buffer[2], array[2]);    
  }

  TEST(ArrayTests, Array_constructorString)
  {
    char buffer[3][20];
    
    char * ptrBuffer[3];
    ArrayHelper::convertToPointerArray(buffer, ptrBuffer);

    Array<char *> array(ptrBuffer, 3);
    EXPECT_EQ(array.size(), 3);
    EXPECT_EQ(array.length(), 0);

    EXPECT_EQ(buffer[0], array.data()[0]);
    EXPECT_EQ(buffer[1], array.data()[1]);
    EXPECT_EQ(buffer[2], array.data()[2]);

    EXPECT_EQ(buffer[0], array[0]);
    EXPECT_EQ(buffer[1], array[1]);
    EXPECT_EQ(buffer[2], array[2]);    
  }

  TEST(ArrayTests, Array_addValues)
  {
    uint8_t buffer[3];
    Array<uint8_t> array(buffer, 3);
    EXPECT_TRUE(array.add((uint8_t)0));
    EXPECT_TRUE(array.add(127));
    EXPECT_TRUE(array.add(255));
    EXPECT_EQ(array.size(), 3);
    EXPECT_EQ(array.length(), 3);
    EXPECT_EQ(array[0], 0);
    EXPECT_EQ(array[1], 127);
    EXPECT_EQ(array[2], 255);
  }

  TEST(ArrayTests, Array_maxValues)
  {
    uint8_t buffer[3];
    Array<uint8_t> array(buffer, 3);
    EXPECT_TRUE(array.add((uint8_t)0));
    EXPECT_TRUE(array.add(127));
    EXPECT_TRUE(array.add(255));
    EXPECT_FALSE(array.add(123));
    EXPECT_EQ(array.length(), 3);
  }

  TEST(ArrayTests, Array_findIndex)
  {
    uint8_t buffer[3];
    Array<uint8_t> array(buffer, 3);
    EXPECT_TRUE(array.add((uint8_t)0));
    EXPECT_TRUE(array.add(127));
    EXPECT_TRUE(array.add(255));
    EXPECT_EQ(array.findIndex((uint8_t)0), 0); // Need to cast to avoid function overload getting confused.
    EXPECT_EQ(array.findIndex(127), 1);
    EXPECT_EQ(array.findIndex(255), 2);
    EXPECT_EQ(array.findIndex(123), -1);
  }

  TEST(ArrayTests, Array_findIndexString)
  {
    const size_t ARRAY_SIZE = 3;
    const size_t STRING_LENGTH = 20;

    char buffer[ARRAY_SIZE][STRING_LENGTH];
    char * ptrBuffer[ARRAY_SIZE];
    ArrayHelper::convertToPointerArray(buffer, ptrBuffer);

    Array<char*> array(ptrBuffer, ARRAY_SIZE);
    EXPECT_TRUE(array.add((const char * )"Ernie"));
    EXPECT_TRUE(array.add((const char * )"Bert"));
    EXPECT_TRUE(array.add((const char * )"Gonzo"));
    EXPECT_EQ(array.findIndex((const char * )"Ernie"), 0);
    EXPECT_EQ(array.findIndex((const char * )"Bert"), 1);
    EXPECT_EQ(array.findIndex((const char * )"Gonzo"), 2);
    EXPECT_EQ(array.findIndex((const char * )"Elmo"), -1);
  }

  TEST(ArrayTests, Array_copyOperator)
  {
    uint8_t buffer[3];
    Array<uint8_t> array(buffer, 3);
    EXPECT_TRUE(array.add((uint8_t)0));
    EXPECT_TRUE(array.add(127));
    EXPECT_TRUE(array.add(255));
    uint8_t newBuffer[3];
    Array<uint8_t> newArray(newBuffer, 3);
    newArray = array;
    EXPECT_EQ(newArray.length(), 3);
    EXPECT_EQ(newArray[0], 0);
    EXPECT_EQ(newArray[1], 127);
    EXPECT_EQ(newArray[2], 255);
  }
}