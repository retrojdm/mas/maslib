#include <gtest/gtest.h>

#include "../../src/Models/SettingDictionary.h"

namespace mas
{
  TEST(SettingDictionaryTests, SettingDictionary_defaultConstructor)
  {
    SettingDictionary settings;
    EXPECT_EQ(settings.count(), 0);
  }

  TEST(SettingDictionaryTests, SettingDictionary_indexOperator)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");
    EXPECT_EQ(settings.count(), 2);
    EXPECT_STREQ(settings[0].key, "name");
    EXPECT_STREQ(settings[0].value, "Sunny");
    EXPECT_STREQ(settings[1].key, "stink");
    EXPECT_STREQ(settings[1].value, "true");
  }

  TEST(SettingDictionaryTests, SettingDictionary_hasKey)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");
    EXPECT_TRUE(settings.hasKey("name"));
    EXPECT_TRUE(settings.hasKey("stink"));
    EXPECT_FALSE(settings.hasKey("lic"));
    EXPECT_FALSE(settings.hasKey(""));
    EXPECT_FALSE(settings.hasKey(nullptr));
  }

  TEST(SettingDictionaryTests, SettingDictionary_getSet)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");

    char name[64];
    name[0] = '\0';
    bool gotName = settings.get("name", name);
    EXPECT_TRUE(gotName);
    EXPECT_STREQ(name, "Sunny");

    char stinks[64];
    stinks[0] = '\0';
    bool gotStinks = settings.get("stink", stinks);
    EXPECT_TRUE(gotStinks);
    EXPECT_STREQ(stinks, "true");

    char license[64];
    license[0] = '\0';
    bool gotLicense = settings.get("lic", license);
    EXPECT_FALSE(gotLicense);
    EXPECT_STREQ(license, "");
  }

  TEST(SettingDictionaryTests, SettingDictionary_update)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");
    settings.set("name", "Skittles");
    settings.set("stink", "false");

    char name[64];
    name[0] = '\0';
    bool gotName = settings.get("name", name);
    EXPECT_TRUE(gotName);
    EXPECT_STREQ(name, "Skittles");

    char stinks[64];
    stinks[0] = '\0';
    bool gotStinks = settings.get("stink", stinks);
    EXPECT_TRUE(gotStinks);
    EXPECT_STREQ(stinks, "false");
  }

  TEST(SettingDictionaryTests, SettingDictionary_clear)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");
    settings.clear();
    EXPECT_EQ(settings.count(), 0);
    EXPECT_FALSE(settings.hasKey("name"));
    EXPECT_FALSE(settings.hasKey("stink"));
  }

  TEST(SettingDictionaryTests, SettingDictionary_remove)
  {
    SettingDictionary settings;
    settings.set("name", "Sunny");
    settings.set("stink", "true");
    settings.remove("stink");
    EXPECT_EQ(settings.count(), 1);
    EXPECT_TRUE(settings.hasKey("name"));
    EXPECT_FALSE(settings.hasKey("stink"));
  }
}