#include "../../src/Platform.h"

// The `<TimeLib.h>` library that `DatetimeHelper` relies on for the `TimeElements` class is Arduino (Teensy>) specific.
// We'll just mock the output for our unit tests in Clang.

#if !defined(ARDUINO)
  namespace mas::DatetimeHelper
  {
    void toIso(time_t t, char* const output)
    {
      strcpy(output, "2024-04-01T14:45:28");
    }

    bool tryParseIso(const char* const input, time_t &outT, char* const outError)
    {
      outT = 1711935928;
      return true;
    }
  }
#endif