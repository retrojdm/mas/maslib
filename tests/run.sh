#!/bin/bash

clear;
if ! cmake --build build; then
    exit 1
fi

cd build
./Tests # ./Tests.exe on windows.
cd ..