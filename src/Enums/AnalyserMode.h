#pragma once

#include "../Platform.h"

namespace mas
{
  enum class AnalyserMode:uint8_t
  {
    Off,
    Peaks,
    FFT,
    Waveform,

    __COUNT,
  };

  const char ANALYSER_MODE_OFF_NAME[]       PROGMEM = "off";
  const char ANALYSER_MODE_PEAKS_NAME[]     PROGMEM = "peaks";
  const char ANALYSER_MODE_FFT_NAME[]       PROGMEM = "fft";
  const char ANALYSER_MODE_WAVEFORM_NAME[]  PROGMEM = "waveform";

  const size_t ANALYSER_MODE_COUNT = static_cast<size_t>(AnalyserMode::__COUNT);

  const char* const ANALYSER_MODE_NAMES[ANALYSER_MODE_COUNT]
  {
    ANALYSER_MODE_OFF_NAME,
    ANALYSER_MODE_PEAKS_NAME,
    ANALYSER_MODE_FFT_NAME,
    ANALYSER_MODE_WAVEFORM_NAME,
  };
}
