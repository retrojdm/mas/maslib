#pragma once

#include "../Platform.h"

namespace mas
{
  enum class ClockTickMode:uint8_t
  {
    Off,
    Minute,
    Second,

    __COUNT,
  };

  const char CLOCK_TICK_MODE_OFF_NAME[]     PROGMEM = "off";
  const char CLOCK_TICK_MODE_MINUTE_NAME[]  PROGMEM = "min";
  const char CLOCK_TICK_MODE_SECOND_NAME[]  PROGMEM = "sec";

  const size_t CLOCK_TICK_MODE_COUNT = static_cast<size_t>(ClockTickMode::__COUNT);

  const char* const CLOCK_TICK_MODE_NAMES[CLOCK_TICK_MODE_COUNT]
  {
    CLOCK_TICK_MODE_OFF_NAME,
    CLOCK_TICK_MODE_MINUTE_NAME,
    CLOCK_TICK_MODE_SECOND_NAME,
  };
}
