#pragma once

#include "../Platform.h"

namespace mas
{
  enum class StereoMode:uint8_t
  {
    Mono,
    Stereo,

    __COUNT,
  };

  const char STEREO_MODE_MONO_NAME[]    PROGMEM = "mono";
  const char STEREO_MODE_STEREO_NAME[]  PROGMEM = "stereo";

  const size_t STEREO_MODE_COUNT = static_cast<size_t>(StereoMode::__COUNT);

  const char* const STEREO_MODE_NAMES[STEREO_MODE_COUNT]
  {
    STEREO_MODE_MONO_NAME,
    STEREO_MODE_STEREO_NAME,
  };
}
