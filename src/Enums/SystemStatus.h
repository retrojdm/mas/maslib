#pragma once

#include "../Platform.h"

namespace mas
{
  enum class SystemStatus:uint8_t
  {
    Stopped,
    Starting,
    Active,
    Stopping,

    __COUNT,
  };

  const char SYSTEM_STATUS_STOPPED_NAME[]   PROGMEM = "stopped";
  const char SYSTEM_STATUS_STARTING_NAME[]  PROGMEM = "starting";
  const char SYSTEM_STATUS_ACTIVE_NAME[]    PROGMEM = "active";
  const char SYSTEM_STATUS_STOPPING_NAME[]  PROGMEM = "stopping";

  const size_t SYSTEM_STATUS_COUNT = static_cast<size_t>(SystemStatus::__COUNT);

  const char* const SYSTEM_STATUS_NAMES[SYSTEM_STATUS_COUNT]
  {
    SYSTEM_STATUS_STOPPED_NAME,
    SYSTEM_STATUS_STARTING_NAME,
    SYSTEM_STATUS_ACTIVE_NAME,
    SYSTEM_STATUS_STOPPING_NAME,
  };
}
