#pragma once

#include "../Platform.h"

namespace mas
{
  enum class SystemAction:uint8_t
  {
    Start,
    Stop,

    __COUNT,
  };

  const char SYSTEM_ACTION_START_NAME[] PROGMEM = "start";
  const char SYSTEM_ACTION_STOP_NAME[]  PROGMEM = "stop";

  const size_t SYSTEM_ACTION_COUNT = static_cast<size_t>(SystemAction::__COUNT);

  const char* const SYSTEM_ACTION_NAMES[SYSTEM_ACTION_COUNT]
  {
    SYSTEM_ACTION_START_NAME,
    SYSTEM_ACTION_STOP_NAME,
  };
}
