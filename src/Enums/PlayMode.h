#pragma once

#include "../Platform.h"

namespace mas
{
  enum class PlayMode:uint8_t
  {
    Stopped,
    Playing,
    Paused,
    FastForwarding,
    Rewinding,

    __COUNT,
  };

  const char PLAY_MODE_STOPPED_NAME[]         PROGMEM = "stopped";
  const char PLAY_MODE_PLAYING_NAME[]         PROGMEM = "playing";
  const char PLAY_MODE_PAUSED_NAME[]          PROGMEM = "paused";
  const char PLAY_MODE_FAST_FORWARDING_NAME[] PROGMEM = "ffd";
  const char PLAY_MODE_REWINDING_NAME[]       PROGMEM = "rwd";

  const size_t PLAY_MODE_COUNT = static_cast<size_t>(PlayMode::__COUNT);

  const char* const PLAY_MODE_NAMES[PLAY_MODE_COUNT]
  {
    PLAY_MODE_STOPPED_NAME,
    PLAY_MODE_PLAYING_NAME,
    PLAY_MODE_PAUSED_NAME,
    PLAY_MODE_FAST_FORWARDING_NAME,
    PLAY_MODE_REWINDING_NAME,
  };
}
