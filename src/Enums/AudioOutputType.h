#pragma once

#include "../Platform.h"

namespace mas
{
  enum class AudioOutputType:uint8_t
  {
    LineOut,

    __COUNT,
  };
}
