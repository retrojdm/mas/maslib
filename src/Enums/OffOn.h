#pragma once

#include "../Platform.h"

namespace mas
{
  enum class OffOn:uint8_t
  {
    Off,
    On,

    __COUNT,
  };

  const char OFF_ON_OFF_NAME[]  PROGMEM = "off";
  const char OFF_ON_ON_NAME[]   PROGMEM = "on";

  const size_t OFF_ON_COUNT = static_cast<size_t>(OffOn::__COUNT);

  const char* const OFF_ON_NAMES[OFF_ON_COUNT]
  {
    OFF_ON_OFF_NAME,
    OFF_ON_ON_NAME,
  };
}
