#pragma once

#include "../Platform.h"

namespace mas
{
  enum class AudioInputType:uint8_t
  {
    PC,
    Media,
    LineIn,
    Radio,
    Bluetooth,

    __COUNT,
  };

  const char AUDIO_INUPT_TYPE_PC_NAME[]         PROGMEM = "PC";
  const char AUDIO_INUPT_TYPE_MEDIA_NAME[]      PROGMEM = "MED";
  const char AUDIO_INUPT_TYPE_LINE_IN_NAME[]    PROGMEM = "LIN";
  const char AUDIO_INUPT_TYPE_RADIO_NAME[]      PROGMEM = "RAD";
  const char AUDIO_INUPT_TYPE_BLUETOOTH_NAME[]  PROGMEM = "BLU";

  const size_t AUDIO_INPUT_TYPE_COUNT = static_cast<size_t>(AudioInputType::__COUNT);

  const char* const AUDIO_INPUT_TYPE_NAMES[AUDIO_INPUT_TYPE_COUNT]
  {
    AUDIO_INUPT_TYPE_PC_NAME,
    AUDIO_INUPT_TYPE_MEDIA_NAME,
    AUDIO_INUPT_TYPE_LINE_IN_NAME,
    AUDIO_INUPT_TYPE_RADIO_NAME,
    AUDIO_INUPT_TYPE_BLUETOOTH_NAME,
  };
}
