#pragma once

#include "../Platform.h"

namespace mas
{
  enum class RadioBand:uint8_t
  {
    AM,
    FM,

    __COUNT,
  };

  // WARNING: These must be a single character.
  // We store frequencies and presets like "a531" and "f10770".
  // See `_tryParseBandFrequencyPair` in `RadioBaseCard.cpp` if you want to refactor this.
  const char RADIO_BAND_AM_NAME[] PROGMEM = "a";
  const char RADIO_BAND_FM_NAME[] PROGMEM = "f";

  const size_t RADIO_BAND_COUNT = static_cast<size_t>(RadioBand::__COUNT);

  const char* const RADIO_BAND_NAMES[RADIO_BAND_COUNT]
  {
    RADIO_BAND_AM_NAME,
    RADIO_BAND_FM_NAME,
  };
}
