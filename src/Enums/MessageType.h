#pragma once

#include "../Platform.h"

// IMPORTANT:
// If you add enums and codes here, make sure you also add to MessageFactory.
namespace mas
{
  // This is a special enum, with an underlying type of uint16_t, instead of the normal uint8_t.
  enum class MessageType:uint16_t
  {
    // Core

    SerialEchoCommand = 0,          //  XEC   <state?>
    SerialFlowControlCommand,       //  XFC   <state?>
    SerialEchoEvent,                // [XEC]  <state>
    SerialFlowControlEvent,         // [XFC]  <state>

    // Main Unit

    AnalyserModeCommand,            //  AMO   <mode?> <stereo?> <fps?> <size?>
    AnalyserModeEvent,              // [AMO]  <mode> <stereo?> <fps?> <size?>
    AnalyserPeaksMonoEvent,         // [APM]  <value>
    AnalyserPeaksStereoEvent,       // [APS]  <left> <right>
    AnalyserFftMonoEvent,           // [AFM]  <data>
    AnalyserFftStereoEvent,         // [AFS]  <left> <right>
    AnalyserWaveformMonoEvent,      // [AWM]  <data>
    AnalyserWaveformStereoEvent,    // [AWS]  <left> <right>

    AudioInputPcUsbCommand,         //  IPC
    AudioInputListCommand,          //  ILS   <index?>
    AudioInputIndexCommand,         //  IDX
    AudioInputInfoCommand,          //  INF   <index?>
    AudioInputPcUsbEvent,           // [IPC]  <state>
    AudioInputListEvent,            // [ILS]  <types[]>
    AudioInputIndexEvent,           // [IDX]  <index>
    AudioInputInfoEvent,            // [INF]  <index> <slot> <type> <suffix?>

    AudioOutputVolumeCommand,       //  OVL   <volume?>
    AudioOutputMuteCommand,         //  OMU   <mute?>
    AudioOutputBalanceCommand,      //  OBA   <balance?>
    AudioOutputFadeCommand,         //  OFA   <fade?>
    AudioOutputVolumeEvent,         // [OVL]  <volume>
    AudioOutputMuteEvent,           // [OMU]  <mute>
    AudioOutputBalanceEvent,        // [OBA]  <balance>
    AudioOutputFadeEvent,           // [OFA]  <fade>

    BluetoothSlotCommand,           //  TSL
    BluetoothSerialEchoCommand,     //  TEC   <slot> <state?>
    BluetoothSerialTransmitCommand, //  TTX   <slot> <command>
    BluetoothNameCommand,           //  TNA   <slot> <name?>
    BluetoothPinEnableCommand,      //  TPE   <slot> <state?>
    BluetoothPinCommand,            //  TPN   <slot> <pin?>
    BluetoothConnectionCommand,     //  TCN   <slot>
    BluetoothPlayModeCommand,       //  TMO   <slot>
    BluetoothPlayCommand,           //  TPL   <slot>
    BluetoothPauseCommand,          //  TPA   <slot>
    BluetoothStopCommand,           //  TST   <slot>
    BluetoothPrevTrackCommand,      //  TPR   <slot>
    BluetoothNextTrackCommand,      //  TNE   <slot>
    BluetoothTrackInfoCommand,      //  TIN   <slot>
    BluetoothSlotEvent,             // [TSL]  <slot>
    BluetoothSerialEchoEvent,       // [TEC]  <slot> <state>
    BluetoothSerialTransmitEvent,   // [TTX]  <slot> <command>
    BluetoothSerialReceiveEvent,    // [TRX]  <slot> <indication>
    BluetoothNameEvent,             // [TNA]  <slot> <name>
    BluetoothPinEnableEvent,        // [TPE]  <slot> <state>
    BluetoothPinEvent,              // [TPN]  <slot> <pin>
    BluetoothConnectionEvent,       // [TCN]  <slot> <status>
    BluetoothPlayModeEvent,         // [TMO]  <slot> <mode>
    BluetoothTrackTimeEvent,        // [TTM]  <slot> <time> <length>
    BluetoothTitleEvent,            // [TTI]  <slot> <title>
    BluetoothArtistEvent,           // [TAR]  <slot> <atist>
    BluetoothAlbumEvent,            // [TAL]  <slot> <album>

    ClockDatetimeCommand,           //  CDT   <datetime?>
    ClockTickModeCommand,           //  CTK   <mode?>
    ClockDatetimeEvent,             // [CDT]  <datetime>
    ClockTickModeEvent,             // [CTK]  <mode>

    EffectsPlayCommand,             //  FPL   <effect>
    EffectsCompleteEvent,           // [FST]

    EqualiserOnCommand,             //  QON   <state?>
    EqualiserFrequenciesCommand,    //  QFR   <frequencies[]?>
    EqualiserLevelsCommand,         //  QLV   <levels[]?>
    EqualiserOnEvent,               // [QON]  <state>
    EqualiserFrequenciesEvent,      // [QFR]  <frequencies[]>
    EqualiserLevelsEvent,           // [QLV]  <levels[]>

    /*
    MediaDriveCommand,              //  MDV   <drive?>
    MediaPathCommand,               //  MPT   <path?>
    MediaFileCountCommand,          //  MCO
    MediaListCommand,               //  MLS   <offset?> <limit?>
    MediaPlayModeCommand,           //  MMO   <action?>
    MediaPlayCommand,               //  MPL
    MediaPauseCommand,              //  MPA
    MediaStopCommand,               //  MST
    MediaPrevTrackCommand,          //  MPR
    MediaNextTrackCommand,          //  MNE
    MediaRewindCommand,             //  MRW   <state>
    MediaFastforwardCommand,        //  MFF   <state>
    MediaShuffleCommand,            //  MSH   <mode?>
    MediaRepeatCommand,             //  MRP   <mode?>
    MediaDriveDetectedEvent,        // [MDD]  <drive>
    MediaDriveRemovedEvent,         // [MDR]  <drive>
    MediaDriveEvent,                // [MDV]  <drive>
    MediaPathEvent,                 // [MPT]  <path>
    MediaFileCountEvent,            // [MCO]  <total>
    MediaPageHeaderEvent,           // [MPH]  <total> <offset> <limit>
    MediaPageItemEvent,             // [MPI]  <file>
    MediaPlayModeCommandEvent,      // [MMO]  <mode>
    MediaShuffleEvent,              // [MSH]  <mode>
    MediaRepeatEvent,               // [MRP]  <mode>
    MediaTrackTimeEvent,            // [MTM]  <time> <length>
    MediaTrackNumberEvent,          // [MNO]  <number>
    MediaTrackTitleEvent,           // [MTT]  <title>
    MediaArtistEvent,               // [MAR]  <artist>
    MediaAlbumEvent,                // [MAL]  <album>
    MediaYearEvent,                 // [MYR]  <year>
    */
  
    PowerBatteryCommand,            //  PBT   <state?>
    PowerAccessoriesCommand,        //  PAC
    PowerBatteryEvent,              // [PBT]  <state>
    PowerAccessoriesEvent,          // [PAC]  <state>

    RadioSlotCommand,               //  RSL
    RadioBandListCommand,           //  RBL   <slot>
    RadioBandCommand,               //  RBA   <slot> <band?>
    RadioFrequencyCommand,          //  RFR   <slot> <frequency?>
    RadioSeekDownCommand,           //  RPR   <slot>
    RadioSeekUpCommand,             //  RNE   <slot>
    RadioRdsCommand,                //  RDS   <slot>
    RadioStereoCommand,             //  RST   <slot>
    RadioSignalStrengthCommand,     //  RSI   <slot>
    RadioStatusUpdateCommand,       //  RSU   <slot> <state?>
    RadioSlotEvent,                 // [RSL]  <slot>
    RadioBandListEvent,             // [RBL]  <slot> <bands[]>
    RadioBandEvent,                 // [RBA]  <slot> <band>
    RadioFrequencyEvent,            // [RFR]  <slot> <frequency>
    RadioRdsEvent,                  // [RDS]  <slot> <message>
    RadioStereoEvent,               // [RST]  <slot> <state>
    RadioSignalStrengthEvent,       // [RSI]  <slot> <level>
    RadioStatusUpdateEvent,         // [RSU]  <slot> <state>

    SystemVersionCommand,           //  SVE
    SystemStatusCommand,            //  SYS   <action?> <key?>
    SystemListCommand,              //  SLS
    SystemAcknowledgeCommand,       //  SAK   <key>

    SystemLogEvent,                 // [SLG]  <level> <key> <message>
    SystemVersionEvent,             // [SVE]  <unit> <version>
    SystemStatusEvent,              // [SYS]  <status> <key>
    SystemListEvent,                // [SLS]  <keys[]>
    SystemAcknowledgeEvent,         // [SAK]  <key>

    // Front Panel

    EncoderChangedEvent,            // [ENC]  <index> <steps>

    ButtonDownEvent,                // [BDN]  <index>
    ButtonUpEvent,                  // [BUP]  <index>
    ButtonPressedEvent,             // [BPR]  <index>
    ButtonHeldEvent,                // [BHE]  <index>

    LedLevelsCommand,               //  LED   <levels[]?>
    LedLevelsEvent,                 // [LED]  <levels[]>

    UiVisModeCommand,               //  UVM   <mode?>
    UiVisFullScreenCommand,         //  UVF   <state?>
    UiVisModeEvent,                 // [UVM]  <mode>
    UiVisFullScreenEvent,           // [UVF]  <state>

    SettingsReadCommand,            //  NRD
    SettingsWriteCommand,           //  NWR
    SettingsItemCommand,            //  NSE   <key?> <value?>
    SettingsRemoveCommand,          //  NSX   <key>
    SettingsReadEvent,              // [NRD]
    SettingsWriteEvent,             // [NWR]
    SettingsItemEvent,              // [NSE]  <key> <value>
    SettingsListCompleteEvent,      // [NSC]
    SettingsRemoveEvent,            // [NSX]  <key>

    // ----------

    __COUNT,
  };

  const size_t MESSAGE_TYPE_COUNT = static_cast<size_t>(MessageType::__COUNT);

  // Core

  const char SERIAL_ECHO_COMMAND_CODE[]               PROGMEM =  "XEC";
  const char SERIAL_FLOW_CONTROL_COMMAND_CODE[]       PROGMEM =  "XFC";
  const char SERIAL_ECHO_EVENT_CODE[]                 PROGMEM = "[XEC]";
  const char SERIAL_FLOW_CONTROL_EVENT_CODE[]         PROGMEM = "[XFC]";

  // Main Unit

  const char ANALYSER_MODE_COMMAND_CODE[]             PROGMEM =  "AMO";
  const char ANALYSER_MODE_EVENT_CODE[]               PROGMEM = "[AMO]";
  const char ANALYSER_PEAKS_MONO_EVENT_CODE[]         PROGMEM = "[APM]";
  const char ANALYSER_PEAKS_STEREO_EVENT_CODE[]       PROGMEM = "[APS]";
  const char ANALYSER_FFT_MONO_EVENT_CODE[]           PROGMEM = "[AFM]";
  const char ANALYSER_FFT_STEREO_EVENT_CODE[]         PROGMEM = "[AFS]";
  const char ANALYSER_WAVEFORM_MONO_EVENT_CODE[]      PROGMEM = "[AWM]";
  const char ANALYSER_WAVEFORM_STEREO_EVENT_CODE[]    PROGMEM = "[AWS]";

  const char AUDIO_INPUT_PC_USB_COMMAND_CODE[]        PROGMEM =  "IPC";
  const char AUDIO_INPUT_LIST_COMMAND_CODE[]          PROGMEM =  "ILS";
  const char AUDIO_INPUT_INDEX_COMMAND_CODE[]         PROGMEM =  "IDX";
  const char AUDIO_INPUT_INFO_COMMAND_CODE[]          PROGMEM =  "INF";
  const char AUDIO_INPUT_PC_USB_EVENT_CODE[]          PROGMEM = "[IPC]";
  const char AUDIO_INPUT_LIST_EVENT_CODE[]            PROGMEM = "[ILS]";
  const char AUDIO_INPUT_INDEX_EVENT_CODE[]           PROGMEM = "[IDX]";
  const char AUDIO_INPUT_INFO_EVENT_CODE[]            PROGMEM = "[INF]";

  const char AUDIO_OUTPUT_VOLUME_COMMAND_CODE[]       PROGMEM =  "OVL";
  const char AUDIO_OUTPUT_MUTE_COMMAND_CODE[]         PROGMEM =  "OMU";
  const char AUDIO_OUTPUT_BALANCE_COMMAND_CODE[]      PROGMEM =  "OBA";
  const char AUDIO_OUTPUT_FADE_COMMAND_CODE[]         PROGMEM =  "OFA";
  const char AUDIO_OUTPUT_VOLUME_EVENT_CODE[]         PROGMEM = "[OVL]";
  const char AUDIO_OUTPUT_MUTE_EVENT_CODE[]           PROGMEM = "[OMU]";
  const char AUDIO_OUTPUT_BALANCE_EVENT_CODE[]        PROGMEM = "[OBA]";
  const char AUDIO_OUTPUT_FADE_EVENT_CODE[]           PROGMEM = "[OFA]";

  const char BLUETOOTH_SLOT_COMMAND_CODE[]            PROGMEM =  "TSL";
  const char BLUETOOTH_SERIAL_ECHO_COMMAND_CODE[]     PROGMEM =  "TEC";
  const char BLUETOOTH_SERIAL_TRANSMIT_COMMAND_CODE[] PROGMEM =  "TTX";
  const char BLUETOOTH_NAME_COMMAND_CODE[]            PROGMEM =  "TNA";
  const char BLUETOOTH_PIN_ENABLE_COMMAND_CODE[]      PROGMEM =  "TPE";
  const char BLUETOOTH_PIN_COMMAND_CODE[]             PROGMEM =  "TPN";
  const char BLUETOOTH_CONNECTION_COMMAND_CODE[]      PROGMEM =  "TCN";
  const char BLUETOOTH_PLAY_MODE_COMMAND_CODE[]       PROGMEM =  "TMO";
  const char BLUETOOTH_PLAY_COMMAND_CODE[]            PROGMEM =  "TPL";
  const char BLUETOOTH_PAUSE_COMMAND_CODE[]           PROGMEM =  "TPA";
  const char BLUETOOTH_STOP_COMMAND_CODE[]            PROGMEM =  "TST";
  const char BLUETOOTH_PREV_TRACK_COMMAND_CODE[]      PROGMEM =  "TPR";
  const char BLUETOOTH_NEXT_TRACK_COMMAND_CODE[]      PROGMEM =  "TNE";
  const char BLUETOOTH_TRACK_INFO_COMMAND_CODE[]      PROGMEM =  "TIN";
  const char BLUETOOTH_SLOT_EVENT_CODE[]              PROGMEM = "[TSL]";
  const char BLUETOOTH_SERIAL_ECHO_EVENT_CODE[]       PROGMEM = "[TEC]";
  const char BLUETOOTH_SERIAL_TRANSMIT_EVENT_CODE[]   PROGMEM = "[TTX]";
  const char BLUETOOTH_SERIAL_RECEIVE_EVENT_CODE[]    PROGMEM = "[TRX]";
  const char BLUETOOTH_NAME_EVENT_CODE[]              PROGMEM = "[TNA]";
  const char BLUETOOTH_PIN_ENABLE_EVENT_CODE[]        PROGMEM = "[TPE]";
  const char BLUETOOTH_PIN_EVENT_CODE[]               PROGMEM = "[TPN]";
  const char BLUETOOTH_CONNECTION_EVENT_CODE[]        PROGMEM = "[TCN]";
  const char BLUETOOTH_PLAY_MODE_EVENT_CODE[]         PROGMEM = "[TMO]";
  const char BLUETOOTH_TRACK_TIME_EVENT_CODE[]        PROGMEM = "[TTM]";
  const char BLUETOOTH_TITLE_EVENT_CODE[]             PROGMEM = "[TTI]";
  const char BLUETOOTH_ARTIST_EVENT_CODE[]            PROGMEM = "[TAR]";
  const char BLUETOOTH_ALBUM_EVENT_CODE[]             PROGMEM = "[TAL]";

  const char CLOCK_DATETIME_COMMAND_CODE[]            PROGMEM =  "CDT";
  const char CLOCK_TICK_MODE_COMMAND_CODE[]           PROGMEM =  "CTK";
  const char CLOCK_DATETIME_EVENT_CODE[]              PROGMEM = "[CDT]";
  const char CLOCK_TICK_MODE_EVENT_CODE[]             PROGMEM = "[CTK]";

  const char EFFECTS_PLAY_COMMAND_CODE[]              PROGMEM =  "FPL";
  const char EFFECTS_COMPLETE_EVENT_CODE[]            PROGMEM = "[FST]";

  const char EQUALISER_ON_COMMAND_CODE[]              PROGMEM =  "QON";
  const char EQUALISER_FREQUENCIES_COMMAND_CODE[]     PROGMEM =  "QFR";
  const char EQUALISER_LEVELS_COMMAND_CODE[]          PROGMEM =  "QLV";
  const char EQUALISER_ON_EVENT_CODE[]                PROGMEM = "[QON]";
  const char EQUALISER_FREQUENCIES_EVENT_CODE[]       PROGMEM = "[QFR]";
  const char EQUALISER_LEVELS_EVENT_CODE[]            PROGMEM = "[QLV]";

  /*
  const char MEDIA_DRIVE_COMMAND_CODE[]               PROGMEM =  "MDV";
  const char MEDIA_PATH_COMMAND_CODE[]                PROGMEM =  "MPT";
  const char MEDIA_FILE_COUNT_COMMAND_CODE[]          PROGMEM =  "MCO";
  const char MEDIA_LIST_COMMAND_CODE[]                PROGMEM =  "MLS";
  const char MEDIA_PLAY_MODE_COMMAND_CODE[]           PROGMEM =  "MMO";
  const char MEDIA_PLAY_COMMAND_CODE[]                PROGMEM =  "MPL";
  const char MEDIA_PAUSE_COMMAND_CODE[]               PROGMEM =  "MPA";
  const char MEDIA_STOP_COMMAND_CODE[]                PROGMEM =  "MST";
  const char MEDIA_PREV_TRACK_COMMAND_CODE[]          PROGMEM =  "MPR";
  const char MEDIA_NEXT_TRACK_COMMAND_CODE[]          PROGMEM =  "MNE";
  const char MEDIA_REWIND_COMMAND_CODE[]              PROGMEM =  "MRW";
  const char MEDIA_FASTFORWARD_COMMAND_CODE[]         PROGMEM =  "MFF";
  const char MEDIA_SHUFFLE_COMMAND_CODE[]             PROGMEM =  "MSH";
  const char MEDIA_REPEAT_COMMAND_CODE[]              PROGMEM =  "MRP";
  const char MEDIA_DRIVE_DETECTED_EVENT_CODE[]        PROGMEM = "[MDD]";
  const char MEDIA_DRIVE_REMOVED_EVENT_CODE[]         PROGMEM = "[MDR]";
  const char MEDIA_DRIVE_EVENT_CODE[]                 PROGMEM = "[MDV]";
  const char MEDIA_PATH_EVENT_CODE                    PROGMEM = "[MPT]";
  const char MEDIA_FILE_COUNT_EVENT_CODE[]            PROGMEM = "[MCO]";
  const char MEDIA_PAGE_HEADER_EVENT_CODE[]           PROGMEM = "[MPH]";
  const char MEDIA_PAGE_ITEM_EVENT_CODE[]             PROGMEM = "[MPI]";
  const char MEDIA_PLAY_MODE_COMMAND_EVENT_CODE[]     PROGMEM = "[MMO]";
  const char MEDIA_SHUFFLE_EVENT_CODE[]               PROGMEM = "[MSH]";
  const char MEDIA_REPEAT_EVENT_CODE[]                PROGMEM = "[MRP]";
  const char MEDIA_TRACK_TIME_EVENT_CODE[]            PROGMEM = "[MTM]";
  const char MEDIA_TRACK_NUMBER_EVENT_CODE[]          PROGMEM = "[MNO]";
  const char MEDIA_TRACK_TITLE_EVENT_CODE[]           PROGMEM = "[MTT]";
  const char MEDIA_ARTIST_EVENT_CODE[]                PROGMEM = "[MAR]";
  const char MEDIA_ALBUM_EVENT_CODE[]                 PROGMEM = "[MAL]";
  const char MEDIA_YEAR_EVENT_CODE[]                  PROGMEM = "[MYR]";
  */

  const char POWER_BATTERY_COMMAND_CODE[]             PROGMEM =  "PBT";
  const char POWER_ACCESSORIES_COMMAND_CODE[]         PROGMEM =  "PAC";
  const char POWER_BATTERY_EVENT_CODE[]               PROGMEM = "[PBT]";
  const char POWER_ACCESSORIES_EVENT_CODE[]           PROGMEM = "[PAC]";

  const char RADIO_SLOT_COMMAND_CODE[]                PROGMEM =  "RSL";
  const char RADIO_BAND_LIST_COMMAND_CODE[]           PROGMEM =  "RBL";
  const char RADIO_BAND_COMMAND_CODE[]                PROGMEM =  "RBA";
  const char RADIO_FREQUENCY_COMMAND_CODE[]           PROGMEM =  "RFR";
  const char RADIO_SEEK_DOWN_COMMAND_CODE[]           PROGMEM =  "RPR";
  const char RADIO_SEEK_UP_COMMAND_CODE[]             PROGMEM =  "RNE";
  const char RADIO_RDS_COMMAND_CODE[]                 PROGMEM =  "RDS";
  const char RADIO_STEREO_COMMAND_CODE[]              PROGMEM =  "RST";
  const char RADIO_SIGNAL_STRENGTH_COMMAND_CODE[]     PROGMEM =  "RSI";
  const char RADIO_STATUS_UPDATE_COMMAND_CODE[]       PROGMEM =  "RSU";
  const char RADIO_SLOT_EVENT_CODE[]                  PROGMEM = "[RSL]";
  const char RADIO_BAND_LIST_EVENT_CODE[]             PROGMEM = "[RBL]";
  const char RADIO_BAND_EVENT_CODE[]                  PROGMEM = "[RBA]";
  const char RADIO_FREQUENCY_EVENT_CODE[]             PROGMEM = "[RFR]";
  const char RADIO_RDS_EVENT_CODE[]                   PROGMEM = "[RDS]";
  const char RADIO_STEREO_EVENT_CODE[]                PROGMEM = "[RST]";
  const char RADIO_SIGNAL_STRENGTH_EVENT_CODE[]       PROGMEM = "[RSI]";
  const char RADIO_STATUS_UPDATE_EVENT_CODE[]         PROGMEM = "[RSU]";

  const char SYSTEM_VERSION_COMMAND_CODE[]            PROGMEM =  "SVE";
  const char SYSTEM_STATUS_COMMAND_CODE[]             PROGMEM =  "SYS";
  const char SYSTEM_LIST_COMMAND_CODE[]               PROGMEM =  "SLS";
  const char SYSTEM_ACKNOWLEDGE_COMMAND_CODE[]        PROGMEM =  "SAK";
  const char SYSTEM_LOG_EVENT_CODE[]                  PROGMEM = "[SLG]";
  const char SYSTEM_VERSION_EVENT_CODE[]              PROGMEM = "[SVE]";
  const char SYSTEM_STATUS_EVENT_CODE[]               PROGMEM = "[SYS]";
  const char SYSTEM_LIST_EVENT_CODE[]                 PROGMEM = "[SLS]";
  const char SYSTEM_ACKNOWLEDGE_EVENT_CODE[]          PROGMEM = "[SAK]";

  const char ENCODER_CHANGED_EVENT_CODE[]             PROGMEM = "[ENC]";

  const char BUTTON_DOWN_EVENT_CODE[]                 PROGMEM = "[BDN]";
  const char BUTTON_UP_EVENT_CODE[]                   PROGMEM = "[BUP]";
  const char BUTTON_PRESSED_EVENT_CODE[]              PROGMEM = "[BPR]";
  const char BUTTON_HELD_EVENT_CODE[]                 PROGMEM = "[BHE]";

  const char LED_LEVELS_COMMAND_CODE[]                PROGMEM =  "LED";
  const char LED_LEVELS_EVENT_CODE[]                  PROGMEM = "[LED]";

  const char UI_VIS_MODE_COMMAND_CODE[]               PROGMEM =  "UVM";
  const char UI_VIS_FULL_SCREEN_COMMAND_CODE[]        PROGMEM =  "UVF";
  const char UI_VIS_MODE_EVENT_CODE[]                 PROGMEM = "[UVM]";
  const char UI_VIS_FULL_SCREEN_EVENT_CODE[]          PROGMEM = "[UVF]";

  const char SETTINGS_READ_COMMAND_CODE[]             PROGMEM =  "NRD";
  const char SETTINGS_WRITE_COMMAND_CODE[]            PROGMEM =  "NWR";
  const char SETTINGS_ITEM_COMMAND_CODE[]             PROGMEM =  "NSE";
  const char SETTINGS_REMOVE_COMMAND_CODE[]           PROGMEM =  "NSX";
  const char SETTINGS_READ_EVENT_CODE[]               PROGMEM = "[NRD]";
  const char SETTINGS_WRITE_EVENT_CODE[]              PROGMEM = "[NWR]";
  const char SETTINGS_ITEM_EVENT_CODE[]               PROGMEM = "[NSE]";
  const char SETTINGS_LIST_COMPLETE_EVENT_CODE[]      PROGMEM = "[NSC]";
  const char SETTINGS_REMOVE_EVENT_CODE[]             PROGMEM = "[NSX]";

  const char* const MESSAGE_TYPE_NAMES[MESSAGE_TYPE_COUNT]
  {
    // Core

    SERIAL_ECHO_COMMAND_CODE,
    SERIAL_FLOW_CONTROL_COMMAND_CODE,
    SERIAL_ECHO_EVENT_CODE,
    SERIAL_FLOW_CONTROL_EVENT_CODE,

    // Main Unit

    ANALYSER_MODE_COMMAND_CODE,
    ANALYSER_MODE_EVENT_CODE,
    ANALYSER_PEAKS_MONO_EVENT_CODE,
    ANALYSER_PEAKS_STEREO_EVENT_CODE,
    ANALYSER_FFT_MONO_EVENT_CODE,
    ANALYSER_FFT_STEREO_EVENT_CODE,
    ANALYSER_WAVEFORM_MONO_EVENT_CODE,
    ANALYSER_WAVEFORM_STEREO_EVENT_CODE,

    AUDIO_INPUT_PC_USB_COMMAND_CODE,
    AUDIO_INPUT_LIST_COMMAND_CODE,
    AUDIO_INPUT_INDEX_COMMAND_CODE,
    AUDIO_INPUT_INFO_COMMAND_CODE,
    AUDIO_INPUT_PC_USB_EVENT_CODE,
    AUDIO_INPUT_LIST_EVENT_CODE,
    AUDIO_INPUT_INDEX_EVENT_CODE,
    AUDIO_INPUT_INFO_EVENT_CODE,

    AUDIO_OUTPUT_VOLUME_COMMAND_CODE,
    AUDIO_OUTPUT_MUTE_COMMAND_CODE,
    AUDIO_OUTPUT_BALANCE_COMMAND_CODE,
    AUDIO_OUTPUT_FADE_COMMAND_CODE,
    AUDIO_OUTPUT_VOLUME_EVENT_CODE,
    AUDIO_OUTPUT_MUTE_EVENT_CODE,
    AUDIO_OUTPUT_BALANCE_EVENT_CODE,
    AUDIO_OUTPUT_FADE_EVENT_CODE,

    BLUETOOTH_SLOT_COMMAND_CODE,
    BLUETOOTH_SERIAL_ECHO_COMMAND_CODE,
    BLUETOOTH_SERIAL_TRANSMIT_COMMAND_CODE,
    BLUETOOTH_NAME_COMMAND_CODE,
    BLUETOOTH_PIN_ENABLE_COMMAND_CODE,
    BLUETOOTH_PIN_COMMAND_CODE,
    BLUETOOTH_CONNECTION_COMMAND_CODE,
    BLUETOOTH_PLAY_MODE_COMMAND_CODE,
    BLUETOOTH_PLAY_COMMAND_CODE,
    BLUETOOTH_PAUSE_COMMAND_CODE,
    BLUETOOTH_STOP_COMMAND_CODE,
    BLUETOOTH_PREV_TRACK_COMMAND_CODE,
    BLUETOOTH_NEXT_TRACK_COMMAND_CODE,
    BLUETOOTH_TRACK_INFO_COMMAND_CODE,
    BLUETOOTH_SLOT_EVENT_CODE,
    BLUETOOTH_SERIAL_ECHO_EVENT_CODE,
    BLUETOOTH_SERIAL_TRANSMIT_EVENT_CODE,
    BLUETOOTH_SERIAL_RECEIVE_EVENT_CODE,
    BLUETOOTH_NAME_EVENT_CODE,
    BLUETOOTH_PIN_ENABLE_EVENT_CODE,
    BLUETOOTH_PIN_EVENT_CODE,
    BLUETOOTH_CONNECTION_EVENT_CODE,
    BLUETOOTH_PLAY_MODE_EVENT_CODE,
    BLUETOOTH_TRACK_TIME_EVENT_CODE,
    BLUETOOTH_TITLE_EVENT_CODE,
    BLUETOOTH_ARTIST_EVENT_CODE,
    BLUETOOTH_ALBUM_EVENT_CODE,

    CLOCK_DATETIME_COMMAND_CODE,
    CLOCK_TICK_MODE_COMMAND_CODE,
    CLOCK_DATETIME_EVENT_CODE,
    CLOCK_TICK_MODE_EVENT_CODE,

    EFFECTS_PLAY_COMMAND_CODE,
    EFFECTS_COMPLETE_EVENT_CODE,

    EQUALISER_ON_COMMAND_CODE,
    EQUALISER_FREQUENCIES_COMMAND_CODE,
    EQUALISER_LEVELS_COMMAND_CODE,
    EQUALISER_ON_EVENT_CODE,
    EQUALISER_FREQUENCIES_EVENT_CODE,
    EQUALISER_LEVELS_EVENT_CODE,

    /*
    MEDIA_DRIVE_COMMAND_CODE,
    MEDIA_PATH_COMMAND_CODE,
    MEDIA_FILE_COUNT_COMMAND_CODE,
    MEDIA_LIST_COMMAND_CODE,
    MEDIA_PLAY_MODE_COMMAND_CODE,
    MEDIA_PLAY_COMMAND_CODE,
    MEDIA_PAUSE_COMMAND_CODE,
    MEDIA_STOP_COMMAND_CODE,
    MEDIA_PREV_TRACK_COMMAND_CODE,
    MEDIA_NEXT_TRACK_COMMAND_CODE,
    MEDIA_REWIND_COMMAND_CODE,
    MEDIA_FASTFORWARD_COMMAND_CODE,
    MEDIA_SHUFFLE_COMMAND_CODE,
    MEDIA_REPEAT_COMMAND_CODE,
    MediaDriveDetectedEventCode,
    MEDIA_DRIVE_REMOVED_EVENT_CODE,
    MEDIA_DRIVE_EVENT_CODE,
    MEDIA_PATH_EVENT_CODE,
    MEDIA_FILE_COUNT_EVENT_CODE,
    MEDIA_PAGE_HEADER_EVENT_CODE,
    MEDIA_PAGE_ITEM_EVENT_CODE,
    MEDIA_PLAY_MODE_COMMAND_EVENT_CODE,
    MEDIA_SHUFFLE_EVENT_CODE,
    MEDIA_REPEAT_EVENT_CODE,
    MEDIA_TRACK_TIME_EVENT_CODE,
    MEDIA_TRACK_NUMBER_EVENT_CODE,
    MEDIA_TRACK_TITLE_EVENT_CODE,
    MEDIA_ARTIST_EVENT_CODE,
    MEDIA_ALBUM_EVENT_CODE,
    MEDIA_YEAR_EVENT_CODE,
    */
    
    POWER_BATTERY_COMMAND_CODE,
    POWER_ACCESSORIES_COMMAND_CODE,
    POWER_BATTERY_EVENT_CODE,
    POWER_ACCESSORIES_EVENT_CODE,

    RADIO_SLOT_COMMAND_CODE,
    RADIO_BAND_LIST_COMMAND_CODE,
    RADIO_BAND_COMMAND_CODE,
    RADIO_FREQUENCY_COMMAND_CODE,
    RADIO_SEEK_DOWN_COMMAND_CODE,
    RADIO_SEEK_UP_COMMAND_CODE,
    RADIO_RDS_COMMAND_CODE,
    RADIO_STEREO_COMMAND_CODE,
    RADIO_SIGNAL_STRENGTH_COMMAND_CODE,
    RADIO_STATUS_UPDATE_COMMAND_CODE,
    RADIO_SLOT_EVENT_CODE,
    RADIO_BAND_LIST_EVENT_CODE,
    RADIO_BAND_EVENT_CODE,
    RADIO_FREQUENCY_EVENT_CODE,
    RADIO_RDS_EVENT_CODE,
    RADIO_STEREO_EVENT_CODE,
    RADIO_SIGNAL_STRENGTH_EVENT_CODE,
    RADIO_STATUS_UPDATE_EVENT_CODE,

    SYSTEM_VERSION_COMMAND_CODE,
    SYSTEM_STATUS_COMMAND_CODE,
    SYSTEM_LIST_COMMAND_CODE,
    SYSTEM_ACKNOWLEDGE_COMMAND_CODE,
    SYSTEM_LOG_EVENT_CODE,
    SYSTEM_VERSION_EVENT_CODE,
    SYSTEM_STATUS_EVENT_CODE,
    SYSTEM_LIST_EVENT_CODE,
    SYSTEM_ACKNOWLEDGE_EVENT_CODE,

    // Front Panel

    ENCODER_CHANGED_EVENT_CODE,

    BUTTON_DOWN_EVENT_CODE,
    BUTTON_UP_EVENT_CODE,
    BUTTON_PRESSED_EVENT_CODE,
    BUTTON_HELD_EVENT_CODE,

    LED_LEVELS_COMMAND_CODE,
    LED_LEVELS_EVENT_CODE,

    UI_VIS_MODE_COMMAND_CODE,
    UI_VIS_FULL_SCREEN_COMMAND_CODE,
    UI_VIS_MODE_EVENT_CODE,
    UI_VIS_FULL_SCREEN_EVENT_CODE,

    SETTINGS_READ_COMMAND_CODE,
    SETTINGS_WRITE_COMMAND_CODE,
    SETTINGS_ITEM_COMMAND_CODE,
    SETTINGS_REMOVE_COMMAND_CODE,
    SETTINGS_READ_EVENT_CODE,
    SETTINGS_WRITE_EVENT_CODE,
    SETTINGS_ITEM_EVENT_CODE,
    SETTINGS_LIST_COMPLETE_EVENT_CODE,
    SETTINGS_REMOVE_EVENT_CODE,
  };
}
