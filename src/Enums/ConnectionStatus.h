#pragma once

#include "../Platform.h"

namespace mas
{
  enum class ConnectionStatus:uint8_t
  {
    Unsupported,
    Standby,
    Connecting,
    Connected,
    Streaming,

    __COUNT,
  };

  const char CONNECTION_STATUS_UNSUPPORTED_NAME[]   PROGMEM = "unsupported";
  const char CONNECTION_STATUS_STANDBY_NAME[]       PROGMEM = "standby";
  const char CONNECTION_STATUS_CONNECTING_NAME[]    PROGMEM = "connecting";
  const char CONNECTION_STATUS_CONNECTED_NAME[]     PROGMEM = "connected";
  const char CONNECTION_STATUS_STREAMING_NAME[]     PROGMEM = "streaming";

  const size_t CONNECTION_STATUS_COUNT = static_cast<size_t>(ConnectionStatus::__COUNT);

  const char* const CONNECTION_STATUS_NAMES[CONNECTION_STATUS_COUNT]
  {
    CONNECTION_STATUS_UNSUPPORTED_NAME,
    CONNECTION_STATUS_STANDBY_NAME,
    CONNECTION_STATUS_CONNECTING_NAME,
    CONNECTION_STATUS_CONNECTED_NAME,
    CONNECTION_STATUS_STREAMING_NAME,
  };
}
