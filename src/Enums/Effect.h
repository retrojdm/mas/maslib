#pragma once

#include "../Platform.h"

namespace mas
{
  enum class Effect:uint8_t
  {
    None,
    Beep,
    XdrToneburst,

    __COUNT,
  };

  const char EFFECT_NONE_NAME[]           PROGMEM = "none";
  const char EFFECT_BEEP_NAME[]           PROGMEM = "beep";
  const char EFFECT_XDR_TONEBURST_NAME[]  PROGMEM = "xdr";

  const size_t EFFECT_COUNT = static_cast<size_t>(Effect::__COUNT);

  const char* const EFFECT_NAMES[EFFECT_COUNT]
  {
    EFFECT_NONE_NAME,
    EFFECT_BEEP_NAME,
    EFFECT_XDR_TONEBURST_NAME,
  };
}