#pragma once

namespace mas
{
  enum class ParseMode:uint8_t
  {
    Normal,
    Escaping,
    EscapingHex,

    __COUNT,
  };
}
