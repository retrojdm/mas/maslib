#pragma once

#include "../Platform.h"

namespace mas
{
  enum class LogLevel:uint8_t
  {
    Information,
    Warning,
    Error,

    __COUNT,
  };

  const char LOG_LEVEL_INFORMATION_NAME[]   PROGMEM = "I";
  const char LOG_LEVEL_WARNING_NAME[]       PROGMEM = "W";
  const char LOG_LEVEL_ERROR_NAME[]         PROGMEM = "E";

  const size_t LOG_LEVEL_COUNT = static_cast<size_t>(LogLevel::__COUNT);

  const char* const LOG_LEVEL_NAMES[LOG_LEVEL_COUNT]
  {
    LOG_LEVEL_INFORMATION_NAME,
    LOG_LEVEL_WARNING_NAME,
    LOG_LEVEL_ERROR_NAME,
  };
}