#pragma once

#include <PubSub.h>
#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Module.h"
#include "../Enums/LogLevel.h"
#include "../Enums/SystemAction.h"
#include "../Enums/SystemStatus.h"
#include "../Helpers/EnumHelper.h"
#include "../Helpers/SerialisationHelper.h"
#include "../Messages/System/Commands/SystemStatusCommand.h"
#include "../Messages/System/Commands/SystemAcknowledgeCommand.h"
#include "../Messages/System/Events/SystemAcknowledgeEvent.h"
#include "../Messages/System/Events/SystemLogEvent.h"
#include "../Messages/System/Events/SystemStatusEvent.h"
#include "../Models/Message.h"

namespace mas
{
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class ModuleBase :
    public pubsub::Publisher<Message *>,
    public pubsub::Subscriber<Message *>
  {
  public:
    ModuleBase(Broker<Message *> & broker, const char key, char* const errorBuffer);
    virtual void begin() = 0;
    virtual void loop() = 0;
    virtual void notify(Message * pMessage);
    virtual void onStart();
    virtual void onStop();

  protected:
    void _logInformation(const char* const text);
    void _logWarning(const char* const text);
    void _logError(const char* const text);
    
    SystemStatus _status;
    char _key;

    // A buffer that can be used for composing or transferring error messages.
    // Instead of reserving a new buffer per function, this buffer is provided to save SRAM.
    char* const _error;
  
  private:
    void _onSystemStatusCommand(SystemStatusCommand * pCommand);
    void _onSystemAcknowledgeCommand(SystemAcknowledgeCommand * pCommand);
  };
}
