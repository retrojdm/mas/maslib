#pragma once

#if !defined(ARDUINO_TEENSY41)
#include <avr/wdt.h>
#endif

#include <Arduino.h>
#include <PubSub.h>
#include "../../Constants/DataSize.h"
#include "../../Constants/Localisation.h"
#include "../../Enums/LogLevel.h"
#include "../../Enums/OffOn.h"
#include "../../Helpers/EnumHelper.h"
#include "../../Helpers/SerialisationHelper.h"
#include "../../Messages/Serial/Commands/SerialEchoCommand.h"
#include "../../Messages/Serial/Commands/SerialFlowControlCommand.h"
#include "../../Messages/Serial/Events/SerialEchoEvent.h"
#include "../../Messages/Serial/Events/SerialFlowControlEvent.h"
#include "../../Messages/System/Events/SystemLogEvent.h"
#include "../../Models/Message.h"

namespace mas
{
  class SerialPort : public pubsub::Publisher<Message *>
  {
  public:
    Stream *pSerial;
    int8_t index;
    const char* name;

    SerialPort(Broker<Message *> & broker, Stream *pStream, const char* const displayName);
    void begin(int8_t portIndex);
    void loop();
    void queue(Message * pMessage);
  
  private:
    static const uint16_t AWAITING_ACK_TIMEOUT = 3000;
    static const char ASCII_ETX = 0x03; // ASCII End of Text control code.
    static const char ASCII_ACK = 0x06; // ASCII Acknowledge control code.

    void _onSerialEchoCommand(const SerialEchoCommand * pCommand);
    void _onSerialFlowControlCommand(const SerialFlowControlCommand * pCommand);
    void _onSerialFlowControlEvent(const SerialFlowControlEvent * pCommand);

    void _transmit(Message * pMessage);
    void _transmitError(const char* const text);
    void _reset();
    void _appendCharacter(const char c);
    void _processCharacter(const char c);    
    void _processBuffer();
    void _handleMessage(Message * pMessage);
    void _resetMicrocontroller();

    bool _echo;
    bool _useFlowControl;

    char _error[DataSize::MESSAGE_PARAMETERS_SIZE];
    char _buffer[SerialisationHelper::SERIALISATION_BUFFER];
    uint16_t _bufferIndex;
    bool _overflowError;
    bool _awaitingAcknowledge;
    uint32_t _awaitingAcknowledgeMillis;
    bool _hasQueuedMessage;
    Message * _pQueuedMessage;
    Message * _pLastTransmittedMessage;
  };
}
