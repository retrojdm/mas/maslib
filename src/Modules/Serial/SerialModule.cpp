#include "SerialModule.h"

namespace mas
{
  // Constructor ///////////////////////////////////////////////////////////////////////////////////////////////////////

  SerialModule::SerialModule(
    Broker<Message *> & broker, char* const errorBuffer, SerialPort * ports, const size_t count)
    : ModuleBase(broker, Module::SERIAL_KEY, errorBuffer)
  {
    _ports = ports;
    _count = count;
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void SerialModule::begin()
  {
    subscribe(Module::ALL_KEY);
        
    for (uint8_t i = 0; i < _count; i++)
    {
      _ports[i].begin(i);
    }
  }

  // Should be called in the sketch's .loop() function.
  void SerialModule::loop()
  {
    // The serial module is always active.
    for (uint8_t i = 0; i < _count; i++)
    {
      _ports[i].loop();
    }
  }

  // Called when the broker notifies this module about a message it's subscribed to.
  void SerialModule::notify(Message * pMessage)
  {
    ModuleBase::notify(pMessage);

    // The serial module is always active.
    for (uint8_t i = 0; i < _count; i++)
    {
      if (pMessage->originPortIndex != i) {

        // Transmission is handled in each port's loop.
        _ports[i].queue(pMessage);
      }
    }
  }
}
