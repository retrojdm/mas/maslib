#include "SerialPort.h"

namespace mas
{
  // Constructor ///////////////////////////////////////////////////////////////////////////////////////////////////////

  SerialPort::SerialPort(
    Broker<Message *> & broker, Stream *pStream, const char* const displayName)
    : Publisher<Message *>(broker),
    pSerial(pStream),
    index(-1),
    name(displayName),
    _echo(false),
    _useFlowControl(false),
    _error(),
    _buffer(),
    _bufferIndex(),
    _overflowError(),
    _awaitingAcknowledge(),
    _awaitingAcknowledgeMillis(),
    _hasQueuedMessage(),
    _pQueuedMessage(),
    _pLastTransmittedMessage()
  {
  }

  void SerialPort::begin(int8_t portIndex)
  {
    memset(_buffer, '\0', DataSize::MESSAGE_PARAMETERS_SIZE);
    index = portIndex;
    _reset();
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  // Should be called by the Serial module's .loop() function.
  void SerialPort::loop()
  {
    // The loop is blocking if we're awaiting an ACK.
    while (pSerial->available() || _hasQueuedMessage)
    {
      if (pSerial->available())
      {
        char c = pSerial->read();
        _processCharacter(c);
      }

      if (_useFlowControl && _awaitingAcknowledge)
      {
        // If we time-out waiting for an ACK, reset.
        if (millis() - _awaitingAcknowledgeMillis > AWAITING_ACK_TIMEOUT)
        {
          _resetMicrocontroller();
        }
        else
        {
          continue;
        }
      }

      if (_hasQueuedMessage)
      {
        _transmit(_pQueuedMessage);
        _hasQueuedMessage = false;
      }
    }
  }

  // "Queues" a message for transmission in the loop.
  void SerialPort::queue(Message * pMessage)
  {
    _hasQueuedMessage = true;
    _pQueuedMessage = pMessage;
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  // XEC <state?>
  // Get or set the command echo state. Only effects the serial port you're currently on.
  void SerialPort::_onSerialEchoCommand(const SerialEchoCommand * pCommand)
  {
    if (pCommand->state.hasValue())
    {
      _echo = pCommand->state.value();
    }
    
    _transmit(new(std::nothrow) SerialEchoEvent(_echo));
  }

  // XFC <state?>
  // Get or set a value indicating whether ETX/ACK flow control is enabled. Only effects the serial port you're
  // currently on.
  void SerialPort::_onSerialFlowControlCommand(const SerialFlowControlCommand * pCommand)
  {
    if (pCommand->state.hasValue())
    {
      _useFlowControl = pCommand->state.value();
    }

    _transmit(new(std::nothrow) SerialFlowControlEvent(_useFlowControl));
  }

  // [XFC] <state>
  // A value indicating whether ETX/ACK flow control is enabled.
  void SerialPort::_onSerialFlowControlEvent(const SerialFlowControlEvent * pEvent)
  {
    _useFlowControl = pEvent->state;
  }

  // Transmit a message to the port (not on the queue).
  void SerialPort::_transmit(Message * pMessage)
  {
    char text[SerialisationHelper::SERIALISATION_BUFFER];
    SerialisationHelper::serialise(pMessage, text);
    
    pSerial->print(text);
    pSerial->write('\r');
    pSerial->write('\n');

    _pLastTransmittedMessage = pMessage;

    if (_useFlowControl)
    {
      _awaitingAcknowledge = true;
      _awaitingAcknowledgeMillis = millis();
      pSerial->write(ASCII_ETX);
    }

    pSerial->flush();
  }

  // Transmit an error message formatted as a system log directly to the port.
  void SerialPort::_transmitError(const char* const text)
  {
    _transmit(new(std::nothrow) SystemLogEvent(LogLevel::Error, Module::SERIAL_KEY, text));
  }

  void SerialPort::_reset()
  {
    _buffer[0] = '\0';
    _bufferIndex = 0;
    _overflowError = false;
  }

  // Returns `true` if there was an overflowError.
  void SerialPort::_appendCharacter(const char c)
  {
    if (_overflowError)
    {
      return;
    }

    // Leave room for a null terminator.
    if (_bufferIndex == SerialisationHelper::SERIALISATION_BUFFER)
    {
      _overflowError = true;
      return;
    }

    _buffer[_bufferIndex++] = c;
  }

  // Process a single character received.
  void SerialPort::_processCharacter(const char c)
  {
    if (c == ASCII_ETX)
    {
      if (_useFlowControl)
      {
        pSerial->write(ASCII_ACK);
      }

      return;
    }

    if (c == ASCII_ACK)
    {
      _awaitingAcknowledge = false;
      return;
    }
    
    if (c == '\r')
    {
      return;
    }

    if (c == '\n' || c == '\0')
    {
      _processBuffer();
      _reset();
    }
    else
    {
      _appendCharacter(c);
    }
  }

  // Processes the received "line" when a newline is encountered.
  void SerialPort::_processBuffer()
  {
    char error[DataSize::MESSAGE_PARAMETERS_SIZE];

    error[0] = '\0';
    _buffer[_bufferIndex] = '\0';

    if (_echo)
    {
      pSerial->println(_buffer);
    }

    if (_overflowError)
    {
      // Leave room for newline or null terminator.
      sprintf_P(error, Localisation::ERROR_TOO_LONG, name, SerialisationHelper::SERIALISATION_BUFFER - 1);

      _transmitError(_buffer);
      _transmitError(error);
      return;
    }

    Message * pMessage = nullptr;
    if (!SerialisationHelper::tryParse(_buffer, pMessage, error))
    {
      _transmitError(_buffer);
      _transmitError(error);
      return;
    }

    pMessage->originPortIndex = index;
    _handleMessage(pMessage);

    publish(pMessage);
  }

  // The serial ports aren't really modules, so instead of the broker distributing message to the notify() function, we
  // handle the message here.
  void SerialPort::_handleMessage(Message * pMessage)
  {
    switch (pMessage->type())
    {
    case MessageType::SerialEchoCommand:
      _onSerialEchoCommand(static_cast<SerialEchoCommand *>(pMessage));
      break;

    case MessageType::SerialFlowControlCommand:
      _onSerialFlowControlCommand(static_cast<SerialFlowControlCommand *>(pMessage));
      break;

    case MessageType::SerialFlowControlEvent:
      _onSerialFlowControlEvent(static_cast<SerialFlowControlEvent *>(pMessage));
      break;

    default:
      break;
    }
  }

  void SerialPort::_resetMicrocontroller()
  {
    #if defined(ARDUINO_TEENSY41)
    // Reset Teensy 4.1.
    SCB_AIRCR = 0x05FA0004;
    #else
    // Abuse the watchdog timer to force a reset.
    wdt_enable(WDTO_15MS);
    while(1)
    {
      // Since we're not calling `wdt_reset()` in here, we'll time out and reset.
    }
    #endif
  }
}