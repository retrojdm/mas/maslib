#include "ModuleBase.h"

namespace mas
{
  // Class constructor /////////////////////////////////////////////////////////////////////////////////////////////////

  ModuleBase::ModuleBase(Broker<Message *> & broker, const char key, char* const errorBuffer)
    : Publisher(broker),
    Subscriber(broker),
    _status(SystemStatus::Stopped),
    _key(key),
    _error(errorBuffer)
  {
  }

  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  void ModuleBase::notify(Message * pMessage)
  {
    switch (pMessage->type())
    {
      case MessageType::SystemStatusCommand:
        _onSystemStatusCommand(static_cast<SystemStatusCommand *>(pMessage));
        break;

      // Note: <key> is required, but we're allowing zero paramters, so that a single error response can be emitted by
      // the system module, rather than all modules spamming the message queue with the same error.
      case MessageType::SystemAcknowledgeCommand:
        _onSystemAcknowledgeCommand(static_cast<SystemAcknowledgeCommand *>(pMessage));
        break;

      default:
        break;
    }
  }

  void ModuleBase::onStart()
  {
    _status = SystemStatus::Active;
  }

  void ModuleBase::onStop()
  {
    _status = SystemStatus::Stopped;
  }

  // Protected functions ///////////////////////////////////////////////////////////////////////////////////////////////

  // Emits an information log event.
  void ModuleBase::_logInformation(const char* const text)
  {
    publish(new(std::nothrow) SystemLogEvent(LogLevel::Information, _key, text));
  }

  // Emits a warning log event.
  void ModuleBase::_logWarning(const char* const text)
  {
    publish(new(std::nothrow) SystemLogEvent(LogLevel::Warning, _key, text));
  }

  // Emits an error log event.
  void ModuleBase::_logError(const char* const text)
  {
    publish(new(std::nothrow) SystemLogEvent(LogLevel::Error, _key, text));
  }

  // Private functions /////////////////////////////////////////////////////////////////////////////////////////////////

  // SYS <action?> <key?>
  // `start` or `stop` all, or a specified module.
  void ModuleBase::_onSystemStatusCommand(SystemStatusCommand * pCommand)
  {
    if (!pCommand->action.hasValue())
    {
      publish(new(std::nothrow) SystemStatusEvent(_status, _key));
      return;
    }

    if (!pCommand->key.hasValue() || pCommand->key.value() == _key)
    {
      switch (pCommand->action.value())
      {
        case SystemAction::Start:
          onStart();
          break;

        case SystemAction::Stop:
          onStop();
          break;

        default:
          break;
      }

      publish(new(std::nothrow) SystemStatusEvent(_status, _key));
    }
  }

  // SAK <key>
  // Requests that a given module acknowledges that the command was received. Useful to avoid overwhelming the serial
  // buffer.
  void ModuleBase::_onSystemAcknowledgeCommand(SystemAcknowledgeCommand * pCommand)
  {
    if (pCommand->key != _key)
    {
      return;
    }
    
    publish(new(std::nothrow) SystemAcknowledgeEvent(_key));
  }
}