#pragma once

#include "../Platform.h"

namespace mas::ParameterNames
{
  // Max length needs to be set for copying from PROGMEM to temp SRAM variable for use in sprintf in ValidationHelper.
  static const uint8_t MAX_LENGTH = 12;

  //                                        Max 12 chars: "123456789012"
  static constexpr const char ACTION[]          PROGMEM = "action";
  static constexpr const char ALBUM[]           PROGMEM = "album";
  static constexpr const char ARTIST[]          PROGMEM = "artist";
  static constexpr const char BALANCE[]         PROGMEM = "balance";
  static constexpr const char BAND[]            PROGMEM = "band";
  static constexpr const char BANDS[]           PROGMEM = "bands";
  static constexpr const char CAPACITY[]        PROGMEM = "cap";
  static constexpr const char COMMAND[]         PROGMEM = "command";
  static constexpr const char COUNT[]           PROGMEM = "count";
  static constexpr const char DATA[]            PROGMEM = "data";
  static constexpr const char DATETIME[]        PROGMEM = "datetime";
  static constexpr const char DEVICEID[]        PROGMEM = "dev";
  static constexpr const char EFFECT[]          PROGMEM = "effect";
  static constexpr const char FADE[]            PROGMEM = "fade";
  static constexpr const char FPS[]             PROGMEM = "fps";
  static constexpr const char FREQUENCIES[]     PROGMEM = "frequencies";
  static constexpr const char FREQUENCY[]       PROGMEM = "frequency";
  static constexpr const char ID[]              PROGMEM = "id";
  static constexpr const char IDS[]             PROGMEM = "ids";
  static constexpr const char INDEX[]           PROGMEM = "index";
  static constexpr const char INDICATION[]      PROGMEM = "indication";
  static constexpr const char KEY[]             PROGMEM = "key";
  static constexpr const char KEYS[]            PROGMEM = "keys";
  static constexpr const char LEFT[]            PROGMEM = "left";
  static constexpr const char LENGTH[]          PROGMEM = "length";
  static constexpr const char LEVEL[]           PROGMEM = "level";
  static constexpr const char LEVELS[]          PROGMEM = "levels";
  static constexpr const char MANUFACTURERID[]  PROGMEM = "man";
  static constexpr const char MESSAGE[]         PROGMEM = "message";
  static constexpr const char MODE[]            PROGMEM = "mode";
  static constexpr const char NAME[]            PROGMEM = "name";
  static constexpr const char PIN[]             PROGMEM = "pin";
  static constexpr const char RIGHT[]           PROGMEM = "right";
  static constexpr const char SIZE[]            PROGMEM = "size";
  static constexpr const char SLOT[]            PROGMEM = "slot";
  static constexpr const char STATE[]           PROGMEM = "state";
  static constexpr const char STATUS[]          PROGMEM = "status";
  static constexpr const char STEPS[]           PROGMEM = "steps";
  static constexpr const char STEREO[]          PROGMEM = "stereo";
  static constexpr const char SUFFIX[]          PROGMEM = "suffix";
  static constexpr const char TIME[]            PROGMEM = "time";
  static constexpr const char TITLE[]           PROGMEM = "title";
  static constexpr const char TYPE[]            PROGMEM = "type";
  static constexpr const char TYPES[]           PROGMEM = "types";
  static constexpr const char VALUE[]           PROGMEM = "value";
  static constexpr const char UNIT[]            PROGMEM = "unit";
  static constexpr const char VERSION[]         PROGMEM = "version";
  static constexpr const char VOLUME[]          PROGMEM = "volume";
}