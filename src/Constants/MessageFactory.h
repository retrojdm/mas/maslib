#pragma once

#include <new>
#include "../Enums/MessageType.h"
#include "../Messages/AllMessages.h"

#define CREATE(x) []() -> Message* { return new(std::nothrow) x(); }

namespace mas
{
  typedef Message* (*MessageCreator)();
  const MessageCreator MESSAGE_FACTORY[MESSAGE_TYPE_COUNT]
  {
    // Core

    CREATE(SerialEchoCommand),                //  XEC   <state?>
    CREATE(SerialFlowControlCommand),         //  XFC   <state?>
    CREATE(SerialEchoEvent),                  // [XEC]  <state>
    CREATE(SerialFlowControlEvent),           // [XFC]  <state>

    // Main Unit

    CREATE(AnalyserModeCommand),              //  AMO   <mode?> <stereo?> <fps?> <size?>
    CREATE(AnalyserModeEvent),                // [AMO]  <mode> <stereo?> <fps?> <size?>
    CREATE(AnalyserPeaksMonoEvent),           // [APM]  <value>
    CREATE(AnalyserPeaksStereoEvent),         // [APS]  <left> <right>
    CREATE(AnalyserFftMonoEvent),             // [AFM]  <data>
    CREATE(AnalyserFftStereoEvent),           // [AFS]  <left> <right>
    CREATE(AnalyserWaveformMonoEvent),        // [AWM]  <data>
    CREATE(AnalyserWaveformStereoEvent),      // [AWS]  <left> <right>
  
    CREATE(AudioInputPcUsbCommand),           //  IPC
    CREATE(AudioInputListCommand),            //  ILS   <index?>
    CREATE(AudioInputIndexCommand),           //  IDX
    CREATE(AudioInputInfoCommand),            //  INF   <index?>
    CREATE(AudioInputPcUsbEvent),             // [IPC]  <state>
    CREATE(AudioInputListEvent),              // [ILS]  <types[]>
    CREATE(AudioInputIndexEvent),             // [IDX]  <index>
    CREATE(AudioInputInfoEvent),              // [INF]  <index> <slot> <type> <suffix?>

    CREATE(AudioOutputVolumeCommand),         //  OVL   <volume?>
    CREATE(AudioOutputMuteCommand),           //  OMU   <mute?>
    CREATE(AudioOutputBalanceCommand),        //  OBA   <balance?>
    CREATE(AudioOutputFadeCommand),           //  OFA   <fade?>
    CREATE(AudioOutputVolumeEvent),           // [OVL]  <volume>
    CREATE(AudioOutputMuteEvent),             // [OMU]  <mute>
    CREATE(AudioOutputBalanceEvent),          // [OBA]  <balance>
    CREATE(AudioOutputFadeEvent),             // [OFA]  <fade>

    CREATE(BluetoothSlotCommand),             //  TSL
    CREATE(BluetoothSerialEchoCommand),       //  TEC   <slot> <state?>
    CREATE(BluetoothSerialTransmitCommand),   //  TTX   <slot> <command>
    CREATE(BluetoothNameCommand),             //  TNA   <slot> <name?>
    CREATE(BluetoothPinEnableCommand),        //  TPE   <slot> <state?>
    CREATE(BluetoothPinCommand),              //  TPN   <slot> <pin?>
    CREATE(BluetoothConnectionCommand),       //  TCN   <slot>
    CREATE(BluetoothPlayModeCommand),         //  TMO   <slot>
    CREATE(BluetoothPlayCommand),             //  TPL   <slot>
    CREATE(BluetoothPauseCommand),            //  TPA   <slot>
    CREATE(BluetoothStopCommand),             //  TST   <slot>
    CREATE(BluetoothPrevTrackCommand),        //  TPR   <slot>
    CREATE(BluetoothNextTrackCommand),        //  TNE   <slot>
    CREATE(BluetoothTrackInfoCommand),        //  TIN   <slot>
    CREATE(BluetoothSlotEvent),               // [TSL]  <slot> 
    CREATE(BluetoothSerialEchoEvent),         // [TEC]  <slot> <state>
    CREATE(BluetoothSerialTransmitEvent),     // [TTX]  <slot> <command>
    CREATE(BluetoothSerialReceiveEvent),      // [TRX]  <slot> <indication>
    CREATE(BluetoothNameEvent),               // [TNA]  <slot> <name>
    CREATE(BluetoothPinEnableEvent),          // [TPE]  <slot> <state>
    CREATE(BluetoothPinEvent),                // [TPN]  <slot> <pin>
    CREATE(BluetoothConnectionEvent),         // [TCN]  <slot> <status>
    CREATE(BluetoothPlayModeEvent),           // [TMO]  <slot> <mode>
    CREATE(BluetoothTrackTimeEvent),          // [TTM]  <slot> <time> <length>
    CREATE(BluetoothTitleEvent),              // [TTI]  <slot> <title>
    CREATE(BluetoothArtistEvent),             // [TAR]  <slot> <atist>
    CREATE(BluetoothAlbumEvent),              // [TAL]  <slot> <album>
    
    CREATE(ClockDatetimeCommand),             //  CDT   <datetime?>
    CREATE(ClockTickModeCommand),             //  CTK   <mode?>
    CREATE(ClockDatetimeEvent),               // [CDT]  <datetime>
    CREATE(ClockTickModeEvent),               // [CTK]  <mode>

    CREATE(EffectsPlayCommand),               //  FPL   <effect>
    CREATE(EffectsCompleteEvent),             // [FST]

    CREATE(EqualiserOnCommand),               //  QON   <state?>
    CREATE(EqualiserFrequenciesCommand),      //  QFR   <frequencies[]?>
    CREATE(EqualiserLevelsCommand),           //  QLV   <levels[]?>
    CREATE(EqualiserOnEvent),                 // [QON]  <state>
    CREATE(EqualiserFrequenciesEvent),        // [QFR]  <frequencies[]>
    CREATE(EqualiserLevelsEvent),             // [QLV]  <levels[]>

    /*
    CREATE(MediaDriveCommand),                //  MDV   <drive?>
    CREATE(MediaPathCommand),                 //  MPT   <path?>
    CREATE(MediaFileCountCommand),            //  MCO
    CREATE(MediaListCommand),                 //  MLS   <offset?> <limit?>
    CREATE(MediaPlayModeCommand),             //  MMO   <action?>
    CREATE(MediaPlayCommand),                 //  MPL
    CREATE(MediaPauseCommand),                //  MPA
    CREATE(MediaStopCommand),                 //  MST
    CREATE(MediaPrevTrackCommand),            //  MPR
    CREATE(MediaNextTrackCommand),            //  MNE
    CREATE(MediaRewindCommand),               //  MRW   <state>
    CREATE(MediaFastforwardCommand),          //  MFF   <state>
    CREATE(MediaShuffleCommand),              //  MSH   <mode?>
    CREATE(MediaRepeatCommand),               //  MRP   <mode?>
    CREATE(MediaDriveDetectedEvent),          // [MDD]  <drive>
    CREATE(MediaDriveRemovedEvent),           // [MDR]  <drive>
    CREATE(MediaDriveEvent),                  // [MDV]  <drive>
    CREATE(MediaPathEvent,),                  // [MPT]  <path>
    CREATE(MediaFileCountEvent),              // [MCO]  <total>
    CREATE(MediaPageHeaderEvent),             // [MPH]  <total> <offset> <limit>
    CREATE(MediaPageItemEvent),               // [MPI]  <file>
    CREATE(MediaPlayModeCommandEvent),        // [MMO]  <mode>
    CREATE(MediaShuffleEvent),                // [MSH]  <mode>
    CREATE(MediaRepeatEvent),                 // [MRP]  <mode>
    CREATE(MediaTrackTimeEvent),              // [MTM]  <time> <length>
    CREATE(MediaTrackNumberEvent),            // [MNO]  <number>
    CREATE(MediaTrackTitleEvent),             // [MTT]  <title>
    CREATE(MediaArtistEvent),                 // [MAR]  <artist>
    CREATE(MediaAlbumEvent),                  // [MAL]  <album>
    CREATE(MediaYearEvent),                   // [MYR]  <year>
    */

    CREATE(PowerBatteryCommand),              //  PBT   <state?>
    CREATE(PowerAccessoriesCommand),          //  PAC
    CREATE(PowerBatteryEvent),                // [PBT]  <state>
    CREATE(PowerAccessoriesEvent),            // [PAC]  <state>

    CREATE(RadioSlotCommand),                 //  RSL
    CREATE(RadioBandListCommand),             //  RBL   <slot>
    CREATE(RadioBandCommand),                 //  RBA   <slot> <band?>
    CREATE(RadioFrequencyCommand),            //  RFR   <slot> <frequency?>
    CREATE(RadioSeekDownCommand),             //  RPR   <slot>
    CREATE(RadioSeekUpCommand),               //  RNE   <slot>
    CREATE(RadioRdsCommand),                  //  RDS   <slot>
    CREATE(RadioStereoCommand),               //  RST   <slot>
    CREATE(RadioSignalStrengthCommand),       //  RSI   <slot>
    CREATE(RadioStatusUpdateCommand),         //  RSU   <slot> <state?>
    CREATE(RadioSlotEvent),                   // [RSL]  <slot>
    CREATE(RadioBandListEvent),               // [RBL]  <slot> <bands[]>
    CREATE(RadioBandEvent),                   // [RBA]  <slot> <band>
    CREATE(RadioFrequencyEvent),              // [RFR]  <slot> <frequency>
    CREATE(RadioRdsEvent),                    // [RDS]  <slot> <message>
    CREATE(RadioStereoEvent),                 // [RST]  <slot> <state>
    CREATE(RadioSignalStrengthEvent),         // [RSI]  <slot> <level>
    CREATE(RadioStatusUpdateEvent),           // [RSU]  <slot> <state>

    CREATE(SystemVersionCommand),             //  SVE
    CREATE(SystemStatusCommand),              //  SYS   <action?> <key?>
    CREATE(SystemListCommand),                //  SLS
    CREATE(SystemAcknowledgeCommand),         //  SAK   <key>
    CREATE(SystemLogEvent),                   // [SLG]  <level> <key> <message>
    CREATE(SystemVersionEvent),               // [SVE]  <unit> <version>
    CREATE(SystemStatusEvent),                // [SYS]  <status> <key?>
    CREATE(SystemListEvent),                  // [SLS]  <keys[]>
    CREATE(SystemAcknowledgeEvent),           // [SAK]  <key>

    // Front Panel

    CREATE(EncoderChangedEvent),              // [ENC]  <index> <steps>

    CREATE(ButtonDownEvent),                  // [BDN]  <index>
    CREATE(ButtonUpEvent),                    // [BUP]  <index>
    CREATE(ButtonPressedEvent),               // [BPR]  <index>
    CREATE(ButtonHeldEvent),                  // [BHE]  <index>

    CREATE(LedLevelsCommand),                 //  LED   <levels[]?>
    CREATE(LedLevelsEvent),                   // [LED]  <levels[]>

    CREATE(UiVisModeCommand),                 //  UVM   <mode?>
    CREATE(UiVisFullScreenCommand),           //  UVF   <state?>
    CREATE(UiVisModeEvent),                   // [UVM]  <mode>
    CREATE(UiVisFullScreenEvent),             // [UVF]  <state>

    CREATE(SettingsReadCommand),              //  NRD
    CREATE(SettingsWriteCommand),             //  NWR
    CREATE(SettingsItemCommand),              //  NSE   <key?> <value?>
    CREATE(SettingsRemoveCommand),            //  NSX   <key>
    CREATE(SettingsReadEvent),                // [NRD]
    CREATE(SettingsWriteEvent),               // [NWR]    
    CREATE(SettingsItemEvent),                // [NSE]  <key> <value>
    CREATE(SettingsListCompleteEvent),        // [NSC]
    CREATE(SettingsRemoveEvent),              // [NSX]  <key>
  };
}