#pragma once

#include "../Platform.h"

namespace mas::IntegerFormats
{
  // Max length needs to be set for copying from PROGMEM to temp SRAM variable for use in sprintf in ValidationHelper
  static const uint8_t MAX_LENGTH = 3;

  //                            Max 3 chars: "---"
  static constexpr const char U[]  PROGMEM = "%u";
  static constexpr const char D[]  PROGMEM = "%d";
  static constexpr const char LU[] PROGMEM = "%lu";
}