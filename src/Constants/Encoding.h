#pragma once

#include "../Platform.h"

#include "../Models/SpecialCharacter.h"

// See this description of Base64 from:
// https://www.microfocus.com/documentation/enterprise-developer/ed60/ES-WIN/BKCJCJDEFNS009.html
//
// ---------------------------------------------------------------------------------------------------------------------
// Base64 is a scheme for converting binary data to printable ASCII characters, namely the upper- and lower-case 
// Roman alphabet characters (A–Z, a–z), the numerals (0–9), and the "+" and "/" symbols, with the "=" symbol as a
// special suffix code.
//
// The data's original length must be a multiple of eight bits. To convert data to base64, the first byte is placed in
// the most significant eight bits of a 24-bit buffer, the next in the middle eight, and the third in the least
// significant eight bits. If there are fewer than three bytes to encode, the corresponding buffer bits will be zero.
// The buffer is then used, six bits at a time, most significant first, as indices into the string
// "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" and each character thus selected is output.
//
// This is repeated until the input data is exhausted. If at the end only one or two input bytes remain, then only the
// first two or three characters of the output are used, and the output is then padded with two or one "=" characters
// respectively. This prevents anyone adding extra bits to the reconstructed data.
//
// The resulting base64-encoded data is approximately 33% longer than the original data, and typically appears as
// seemingly random characters.
//
// Base64 encoding is specified in full in RFC 1421 and RFC 2045.
// ---------------------------------------------------------------------------------------------------------------------

namespace mas
{
  namespace Encoding
  {
    const char ARRAY_SEPARATOR = ',';
    const char SETTING_KEY_SLOT_SEPARATOR = ':';
    
    // We use a placeholder when we serialise some empty value.
    // Eg: To make the comma-separated arrays, or empty I/O card IDs more readable on the serial monitor.
    static constexpr const char* EMPTY_VALUE_PLACEHOLDER = "-";

    // These are the special characters that are escaped with their own escape codes.
    // All other non-printable characters will be escapped with "\xhh" hex codes.
    static const size_t SPECIAL_CHARACTER_COUNT = 5;
    constexpr SpecialCharacter SPECIAL_CHARACTERS[SPECIAL_CHARACTER_COUNT] =
    {
      SpecialCharacter('\n', 'n'),  // New Line
      SpecialCharacter('\r', 'r'),  // Carriage Return
      SpecialCharacter('\t', 't'),  // Tab
      SpecialCharacter('\\', '\\'), // Backlash
      SpecialCharacter('\"', '"'),  // Double Quote
    };

    const uint8_t INVALID_CHAR = 255;

    constexpr const char HEX_CHARS[] = "0123456789ABCDEF";

    // This is a criminally inefficient use of SRAM, but it's very performant.
    constexpr uint8_t ASCII_TO_HEX_VALUE[128] =
    {
      INVALID_CHAR, //  [  0]  '<NUL>'
      INVALID_CHAR, //  [  1]  '<SOH>'
      INVALID_CHAR, //  [  2]  '<STX>'
      INVALID_CHAR, //  [  3]  '<ETX>'
      INVALID_CHAR, //  [  4]  '<EOT>'
      INVALID_CHAR, //  [  5]  '<ENQ>'
      INVALID_CHAR, //  [  6]  '<ACK>'
      INVALID_CHAR, //  [  7]  '<BEL>'
      INVALID_CHAR, //  [  8]  '<BS>'
      INVALID_CHAR, //  [  9]  '<HT>'
      INVALID_CHAR, //  [ 10]  '<LF>'
      INVALID_CHAR, //  [ 11]  '<VT>'
      INVALID_CHAR, //  [ 12]  '<FF>'
      INVALID_CHAR, //  [ 13]  '<CR>'
      INVALID_CHAR, //  [ 14]  '<SO>'
      INVALID_CHAR, //  [ 15]  '<SI>'
      INVALID_CHAR, //  [ 16]  '<DLE>'
      INVALID_CHAR, //  [ 17]  '<DC1>'
      INVALID_CHAR, //  [ 18]  '<DC2>'
      INVALID_CHAR, //  [ 19]  '<DC3>'
      INVALID_CHAR, //  [ 20]  '<DC4>'
      INVALID_CHAR, //  [ 21]  '<NAK>'
      INVALID_CHAR, //  [ 22]  '<SYN>'
      INVALID_CHAR, //  [ 23]  '<ETB>'
      INVALID_CHAR, //  [ 24]  '<CAN>'
      INVALID_CHAR, //  [ 25]  '<EM>'
      INVALID_CHAR, //  [ 26]  '<SUB>'
      INVALID_CHAR, //  [ 27]  '<ESC>'
      INVALID_CHAR, //  [ 28]  '<FS>'
      INVALID_CHAR, //  [ 29]  '<GS>'
      INVALID_CHAR, //  [ 30]  '<RS>'
      INVALID_CHAR, //  [ 31]  '<US>'
      INVALID_CHAR, //  [ 32]  ' '
      INVALID_CHAR, //  [ 33]  '!'
      INVALID_CHAR, //  [ 34]  '"'
      INVALID_CHAR, //  [ 35]  '#'
      INVALID_CHAR, //  [ 36]  '$'
      INVALID_CHAR, //  [ 37]  '%'
      INVALID_CHAR, //  [ 38]  '&'
      INVALID_CHAR, //  [ 39]  '''
      INVALID_CHAR, //  [ 40]  '('
      INVALID_CHAR, //  [ 41]  ')'
      INVALID_CHAR, //  [ 42]  '*'
      INVALID_CHAR, //  [ 43]  '+'
      INVALID_CHAR, //  [ 44]  ','
      INVALID_CHAR, //  [ 45]  '-'
      INVALID_CHAR, //  [ 46]  '.'
      INVALID_CHAR, //  [ 47]  '/'
      0, //  [ 48]  '0'
      1, //  [ 49]  '1'
      2, //  [ 50]  '2'
      3, //  [ 51]  '3'
      4, //  [ 52]  '4'
      5, //  [ 53]  '5'
      6, //  [ 54]  '6'
      7, //  [ 55]  '7'
      8, //  [ 56]  '8'
      9, //  [ 57]  '9'
      INVALID_CHAR, //  [ 58]  ':'
      INVALID_CHAR, //  [ 59]  ';'
      INVALID_CHAR, //  [ 60]  '<'
      INVALID_CHAR, //  [ 61]  '='
      INVALID_CHAR, //  [ 62]  '>'
      INVALID_CHAR, //  [ 63]  '?'
      INVALID_CHAR, //  [ 64]  '@'
      10, //  [ 65]  'A'
      11, //  [ 66]  'B'
      12, //  [ 67]  'C'
      13, //  [ 68]  'D'
      14, //  [ 69]  'E'
      15, //  [ 70]  'F'
      INVALID_CHAR, //  [ 71]  'G'
      INVALID_CHAR, //  [ 72]  'H'
      INVALID_CHAR, //  [ 73]  'I'
      INVALID_CHAR, //  [ 74]  'J'
      INVALID_CHAR, //  [ 75]  'K'
      INVALID_CHAR, //  [ 76]  'L'
      INVALID_CHAR, //  [ 77]  'M'
      INVALID_CHAR, //  [ 78]  'N'
      INVALID_CHAR, //  [ 79]  'O'
      INVALID_CHAR, //  [ 80]  'P'
      INVALID_CHAR, //  [ 81]  'Q'
      INVALID_CHAR, //  [ 82]  'R'
      INVALID_CHAR, //  [ 83]  'S'
      INVALID_CHAR, //  [ 84]  'T'
      INVALID_CHAR, //  [ 85]  'U'
      INVALID_CHAR, //  [ 86]  'V'
      INVALID_CHAR, //  [ 87]  'W'
      INVALID_CHAR, //  [ 88]  'X'
      INVALID_CHAR, //  [ 89]  'Y'
      INVALID_CHAR, //  [ 90]  'Z'
      INVALID_CHAR, //  [ 91]  '['
      INVALID_CHAR, //  [ 92]  '\'
      INVALID_CHAR, //  [ 93]  ']'
      INVALID_CHAR, //  [ 94]  '^'
      INVALID_CHAR, //  [ 95]  '_'
      INVALID_CHAR, //  [ 96]  '`'
      10, //  [ 97]  'a'
      11, //  [ 98]  'b'
      12, //  [ 99]  'c'
      13, //  [100]  'd'
      14, //  [101]  'e'
      15, //  [102]  'f'
      INVALID_CHAR, //  [103]  'g'
      INVALID_CHAR, //  [104]  'h'
      INVALID_CHAR, //  [105]  'i'
      INVALID_CHAR, //  [106]  'j'
      INVALID_CHAR, //  [107]  'k'
      INVALID_CHAR, //  [108]  'l'
      INVALID_CHAR, //  [109]  'm'
      INVALID_CHAR, //  [110]  'n'
      INVALID_CHAR, //  [111]  'o'
      INVALID_CHAR, //  [112]  'p'
      INVALID_CHAR, //  [113]  'q'
      INVALID_CHAR, //  [114]  'r'
      INVALID_CHAR, //  [115]  's'
      INVALID_CHAR, //  [116]  't'
      INVALID_CHAR, //  [117]  'u'
      INVALID_CHAR, //  [118]  'v'
      INVALID_CHAR, //  [119]  'w'
      INVALID_CHAR, //  [120]  'x'
      INVALID_CHAR, //  [121]  'y'
      INVALID_CHAR, //  [122]  'z'
      INVALID_CHAR, //  [123]  '{'
      INVALID_CHAR, //  [124]  '|'
      INVALID_CHAR, //  [125]  '}'
      INVALID_CHAR, //  [126]  '~'
      INVALID_CHAR, //  [127]  '<DEL>'
    };

    //                                     0000000000111111111122222222223333333333444444444455555555556666
    //                                     0123456789012345678901234567890123456789012345678901234567890123
    constexpr const char BASE64_CHARS[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    // This is a criminally inefficient use of SRAM, but it's very performant.
    constexpr uint8_t ASCII_TO_BASE64_VALUE[128] =
    {
      INVALID_CHAR, //  [  0]  '<NUL>'
      INVALID_CHAR, //  [  1]  '<SOH>'
      INVALID_CHAR, //  [  2]  '<STX>'
      INVALID_CHAR, //  [  3]  '<ETX>'
      INVALID_CHAR, //  [  4]  '<EOT>'
      INVALID_CHAR, //  [  5]  '<ENQ>'
      INVALID_CHAR, //  [  6]  '<ACK>'
      INVALID_CHAR, //  [  7]  '<BEL>'
      INVALID_CHAR, //  [  8]  '<BS>'
      INVALID_CHAR, //  [  9]  '<HT>'
      INVALID_CHAR, //  [ 10]  '<LF>'
      INVALID_CHAR, //  [ 11]  '<VT>'
      INVALID_CHAR, //  [ 12]  '<FF>'
      INVALID_CHAR, //  [ 13]  '<CR>'
      INVALID_CHAR, //  [ 14]  '<SO>'
      INVALID_CHAR, //  [ 15]  '<SI>'
      INVALID_CHAR, //  [ 16]  '<DLE>'
      INVALID_CHAR, //  [ 17]  '<DC1>'
      INVALID_CHAR, //  [ 18]  '<DC2>'
      INVALID_CHAR, //  [ 19]  '<DC3>'
      INVALID_CHAR, //  [ 20]  '<DC4>'
      INVALID_CHAR, //  [ 21]  '<NAK>'
      INVALID_CHAR, //  [ 22]  '<SYN>'
      INVALID_CHAR, //  [ 23]  '<ETB>'
      INVALID_CHAR, //  [ 24]  '<CAN>'
      INVALID_CHAR, //  [ 25]  '<EM>'
      INVALID_CHAR, //  [ 26]  '<SUB>'
      INVALID_CHAR, //  [ 27]  '<ESC>'
      INVALID_CHAR, //  [ 28]  '<FS>'
      INVALID_CHAR, //  [ 29]  '<GS>'
      INVALID_CHAR, //  [ 30]  '<RS>'
      INVALID_CHAR, //  [ 31]  '<US>'
      INVALID_CHAR, //  [ 32]  ' '
      INVALID_CHAR, //  [ 33]  '!'
      INVALID_CHAR, //  [ 34]  '"'
      INVALID_CHAR, //  [ 35]  '#'
      INVALID_CHAR, //  [ 36]  '$'
      INVALID_CHAR, //  [ 37]  '%'
      INVALID_CHAR, //  [ 38]  '&'
      INVALID_CHAR, //  [ 39]  '''
      INVALID_CHAR, //  [ 40]  '('
      INVALID_CHAR, //  [ 41]  ')'
      INVALID_CHAR, //  [ 42]  '*'
      62, //  [ 43]  '+'
      255, //  [ 44]  ','
      INVALID_CHAR, //  [ 45]  '-'
      INVALID_CHAR, //  [ 46]  '.'
      63, //  [ 47]  '/'
      52, //  [ 48]  '0'
      53, //  [ 49]  '1'
      54, //  [ 50]  '2'
      55, //  [ 51]  '3'
      56, //  [ 52]  '4'
      57, //  [ 53]  '5'
      58, //  [ 54]  '6'
      59, //  [ 55]  '7'
      60, //  [ 56]  '8'
      61, //  [ 57]  '9'
      INVALID_CHAR, //  [ 58]  ':'
      INVALID_CHAR, //  [ 59]  ';'
      INVALID_CHAR, //  [ 60]  '<'
      INVALID_CHAR, //  [ 61]  '='
      INVALID_CHAR, //  [ 62]  '>'
      INVALID_CHAR, //  [ 63]  '?'
      INVALID_CHAR, //  [ 64]  '@'
      0, //  [ 65]  'A'
      1, //  [ 66]  'B'
      2, //  [ 67]  'C'
      3, //  [ 68]  'D'
      4, //  [ 69]  'E'
      5, //  [ 70]  'F'
      6, //  [ 71]  'G'
      7, //  [ 72]  'H'
      8, //  [ 73]  'I'
      9, //  [ 74]  'J'
      10, //  [ 75]  'K'
      11, //  [ 76]  'L'
      12, //  [ 77]  'M'
      13, //  [ 78]  'N'
      14, //  [ 79]  'O'
      15, //  [ 80]  'P'
      16, //  [ 81]  'Q'
      17, //  [ 82]  'R'
      18, //  [ 83]  'S'
      19, //  [ 84]  'T'
      20, //  [ 85]  'U'
      21, //  [ 86]  'V'
      22, //  [ 87]  'W'
      23, //  [ 88]  'X'
      24, //  [ 89]  'Y'
      25, //  [ 90]  'Z'
      INVALID_CHAR, //  [ 91]  '['
      INVALID_CHAR, //  [ 92]  '\'
      INVALID_CHAR, //  [ 93]  ']'
      INVALID_CHAR, //  [ 94]  '^'
      INVALID_CHAR, //  [ 95]  '_'
      INVALID_CHAR, //  [ 96]  '`'
      26, //  [ 97]  'a'
      27, //  [ 98]  'b'
      28, //  [ 99]  'c'
      29, //  [100]  'd'
      30, //  [101]  'e'
      31, //  [102]  'f'
      32, //  [103]  'g'
      33, //  [104]  'h'
      34, //  [105]  'i'
      35, //  [106]  'j'
      36, //  [107]  'k'
      37, //  [108]  'l'
      38, //  [109]  'm'
      39, //  [110]  'n'
      40, //  [111]  'o'
      41, //  [112]  'p'
      42, //  [113]  'q'
      43, //  [114]  'r'
      44, //  [115]  's'
      45, //  [116]  't'
      46, //  [117]  'u'
      47, //  [118]  'v'
      48, //  [119]  'w'
      49, //  [120]  'x'
      50, //  [121]  'y'
      52, //  [122]  'z'
      INVALID_CHAR, //  [123]  '{'
      INVALID_CHAR, //  [124]  '|'
      INVALID_CHAR, //  [125]  '}'
      INVALID_CHAR, //  [126]  '~'
      INVALID_CHAR, //  [127]  '<DEL>'
    };
  }
}
