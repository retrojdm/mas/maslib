#pragma once

namespace mas
{
  namespace SettingKeys
  {
    // General /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Eg: "-,RAS,BLU,LIO,LOT"
    const char IO_CARDS[] PROGMEM = "ioc";

    // Audio input /////////////////////////////////////////////////////////////////////////////////////////////////////

    // Eg: "0".
    const char AUDIO_INPUT_INDEX[] PROGMEM = "idx";

    // Audio output ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Eg: "255".
    const char AUDIO_OUTPUT_VOLUME[] PROGMEM = "ovl";
    
    // Eg: "off".    
    const char AUDIO_OUTPUT_MUTE[] PROGMEM = "omu";

    // Eg: "+2".
    const char AUDIO_OUTPUT_BALANCE[] PROGMEM = "oba";

    // Eg: "-3".
    const char AUDIO_OUTPUT_FADE[] PROGMEM = "ofa";

    // Equaliser ///////////////////////////////////////////////////////////////////////////////////////////////////////

    // Eg: "on".
    const char EQUALISER_ON[] PROGMEM = "qon";

    // The EQ supports up to 8 frequencies.
    // Eg: "50,6300".
    const char EQUALISER_FREQUENCIES[] PROGMEM = "qfr";

    // The EQ supports up to 8 levels.
    // Eg: "12,3".
    const char EQUALISER_LEVELS[] PROGMEM = "qlv";

    // UI //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Eg: "waveform".
    const char UI_VIS_MODE[] PROGMEM = "uvm";

    // Eg: "on".
    const char UI_VIS_FULL_SCREEN[] PROGMEM = "uvf";

    // Radio ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Note, these will all need a <slot> address. Eg: "rad:1"
    //
    // Eg: "0x11".
    const char RADIO_I2C[] PROGMEM = "rad";

    // Eg: "a,f".
    const char RADIO_BANDS[] PROGMEM = "rbl";

    // Eg: "f".
    const char RADIO_BAND[] PROGMEM = "rba";

    // Eg: "a531,f9770".
    const char RADIO_FREQUENCIES[] PROGMEM = "rfr";

    // We only support up to 6 presets.
    // Eg: "f9090,f9250,f9410,f9770,f10290,f10770".
    const char RADIO_PRESET_N[] PROGMEM = "rpr";
  }
}
