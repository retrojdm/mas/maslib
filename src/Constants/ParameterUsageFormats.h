#pragma once

#include "../Platform.h"

namespace mas::ParameterUsageFormats
{
  #if defined(ARDUINO_AVR_ATmega1284)
  static constexpr const char REQUIRED[]  PROGMEM = "<%S>";
  static constexpr const char OPTIONAL[]  PROGMEM = "<%S?>";
  static constexpr const char ARRAY[]     PROGMEM = "<%S[]>";
  #else
  static constexpr const char REQUIRED[]  PROGMEM = "<%s>";
  static constexpr const char OPTIONAL[]  PROGMEM = "<%s?>";
  static constexpr const char ARRAY[]     PROGMEM = "<%s[]>";
  #endif
}