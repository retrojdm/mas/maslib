#pragma once

namespace mas
{
  // Shared (serial and system) and Main Unit module constants.
  //
  // Front Panels will need to maintain their own modules.
  //
  // Note:  The Main Board can't subscribe to, send commands to, or handle events from Front Panel modules.
  //
  namespace Module
  {
      // Core
      static const char ALL_KEY             = 0; // the broker.h distribute() function needs this to be zero.
      static const char SERIAL_KEY          = 'X';

      // Main unit
      // System module runs on the Main Unit, but many system messages can be sent by other modules.
      static const char SYSTEM_KEY          = 'S';
      static const char SETTINGS_KEY        = 'N';
      static const char CLOCK_KEY           = 'C';
      static const char POWER_KEY           = 'P';
      static const char AUDIO_INPUT_KEY     = 'I';
      static const char AUDIO_OUTPUT_KEY    = 'O';
      static const char EQUALISER_KEY       = 'Q';
      static const char ANALYSER_KEY        = 'A';
      static const char EFFECTS_KEY         = 'F';
      static const char RADIO_KEY           = 'R';
      static const char BLUETOOTH_KEY       = 'T';
      static const char MEDIA_KEY           = 'M';

      // Front panel
      static const char BUTTON_KEY          = 'B';
      static const char ENCODER_KEY         = 'E';
      static const char LED_KEY             = 'L';
      static const char UI_KEY              = 'U';
  }
}
