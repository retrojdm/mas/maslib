#pragma once

#include "../Enums/RadioBand.h"
#include "../Models/RadioBandDefinition.h"

namespace mas
{
  constexpr RadioBandDefinition RADIO_BAND_DEFINITIONS[RADIO_BAND_COUNT] =
  {
    //                   min,   max, step, divisor
    RadioBandDefinition( 520,  1750,    5,       1), // AM
    RadioBandDefinition(8750, 10800,   10,     100), // FM
  };
}
