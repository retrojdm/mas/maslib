#pragma once

#include "../Platform.h"

namespace mas
{
  namespace DataSize
  {
    // Publish/Subscriber subscriptions ////////////////////////////////////////////////////////////////////////////////

    // Assuming every module is subscribed to at least the System topic, this should be greater than or equal to the
    // number of modules on a single unit (since there's a broker on each).
    static const size_t SUBSCRIPTIONS_MAX = 48;

    // Messages ////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Command and event codes.
    // This is used only for parsing. All codes are defined in the `MessageType` enum.
    static const size_t MESSAGE_CODE_LENGTH = 3;

    // Message parameters buffer size (includes null-terminator).
    //
    // 48 bytes would be a rough maximum because it's a multiple of 8 to fit neatly within 8-byte alignement boundaries,
    // and allows a little extra room for serialisation overhead like the code, separators, quotes, and escape chars -
    // while staying within the Serial RX and TX buffer sizes on an ATMEGA1284 (64 bytes).
    //
    // Note: Excessive escape characters within a string parameter can make the transmitted message overflow the RX and
    // TX buffer.
    static const size_t MESSAGE_PARAMETERS_SIZE = 48;

    // The max total size of a message object in memory.
    // Used as the block size for our static memory pool for messages.
    // We want room for the `Message` base class, plus whatever parameters we might need in any derived class.
    // Keep in mind that Optional<T> classes will required more space again.
    //
    // This will probably be MESSAGE_PARAMETERS_SIZE + 40.
    // See the unit test in `SerialisationHelperTests` named `messageSize`.
    //
    #if !defined(ARDUINO)
    // We're using 128 to satisfy unit tests. The value is larger because of larger byte-alignment on Linux.
    static const size_t MESSAGE_SIZE = 128;
    #else
    // We actually only need 88 when compiled on an Arduino;
    static const size_t MESSAGE_SIZE = 88;
    #endif

    // Allowing for every module to publish at least one message per loop, this should be greater than or equal to the
    // number of modules across all units.
    //
    // If we multiply MESSAGES_MAX by MESSAGE_SIZE, we get the amount of SRAM
    // needed.
    //
    // Eg: 24 x 88 = 2,112 bytes.
    //
    // This buffer has to be the same on both the Main Unit and Front Panel. Keep in mind that the ATMEGA1284 used on
    // the Front Panel only has 16K of SRAM, and we don't want to use ALL of it for the message buffer.
    //
    static const size_t MESSAGES_MAX = 24;

    // I/O Cards ///////////////////////////////////////////////////////////////////////////////////////////////////////

    // This is where the '5' in "MU50" comes from.
    static const size_t CARD_SLOTS = 5;

    // +2 to allow for the PC USB and Media audio inputs.
    static const size_t AUDIO_INPUT_MAX = CARD_SLOTS + 2;

    // I/O Card ID's are always a 3-character uppercase code (including the null-terminator).
    // Eg: "BLU", "LIN", "LOT", "LIO", "RAD", or "RAS".
    static const size_t CARD_ID_SIZE = 4;

    // Settings ////////////////////////////////////////////////////////////////////////////////////////////////////////

    // I/O Card and EEPROM settings are key/value pairs.
    // Allows enough space even in the insane scenario of 5x radio cards, each having 5x settings.
    // i.e: 11 main settings + (5 x 5) radio settings = 36
    static const size_t SETTINGS_MAX = 36;

    // Setting keys are 3 chars, an optional separator and slot, and the null-terminator.
    // Eg: "ovl", or "rad:1"
    static const size_t SETTING_KEY_SIZE = 6;

    // We have to fit the <key> and <value> (including the space separator and any escape chars) into MESSAGE_PARAMETERS_SIZE.
    // Note, the biggest we'll ever have is the 6x radio presets, which take up 42 chars (including the null terminator).
    // Eg: f09090,f09250,f09410,f09770,f10290,f10770
    // Luckily, this is exactly how much space we have.
    static const size_t SETTING_VALUE_SIZE = MESSAGE_PARAMETERS_SIZE - SETTING_KEY_SIZE;

    // Mono and stereo base64 max data sizes.
    static const size_t ANALYSER_MONO = MESSAGE_PARAMETERS_SIZE;
    static const size_t ANALYSER_STEREO = ANALYSER_MONO / 2;
  }
}
