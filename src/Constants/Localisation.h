#pragma once

#include "../Platform.h"

namespace mas
{
  namespace Localisation
  {
    // General
    const char ERROR_USAGE[] PROGMEM = "Usage: %s";
    const char ERROR_MEMORY[] PROGMEM = "Not enough memory for %s.";
    const char ERROR_COULDNT_PARSE[] PROGMEM = "Can't parse: %s";
    const char ERROR_TOO_MANY_ELEMENTS[] PROGMEM = "Too many %s. Max %u.";
    const char ERROR_WRONG_NUMBER_OF_ELEMENTS[] PROGMEM = "Wrong number of %s. Expected %u.";

    // DatetimeHelper
    const char ERROR_INVALID_DATE_FORMAT[] PROGMEM = "Expected date format YYYY-MM-DDThh:mm:ss";
    const char ERROR_INVALID_YEAR[] PROGMEM = "Invalid year. Expected %u - %u.";

    // EnumHelper
    const char ERROR_INVALID_ENUM[] PROGMEM = "Invalid %s. [%s]";

    // SerialisationHelper
    const char ERROR_MESSAGE_NOT_NULL[] PROGMEM = "Message not initially null.";
    const char ERROR_COULDNT_CREATE_MESSAGE[] PROGMEM = "Couldn't create %s.";
    const char ERROR_EXPECTED_SEPARATOR[] PROGMEM = "Expected space before params.";
    const char ERROR_ESCAPE_CODES[] PROGMEM = "Escape codes must be within quotes";
    const char ERROR_NON_PRINTABLE[] PROGMEM = "Non-printable characters must be escaped.";
    const char ERROR_UNRECOGNISED_ESCAPE_CODE[] PROGMEM = "Unrecognised escape code: \\%c";
    const char ERROR_UNRECOGNISED_HEX_DIGIT[] PROGMEM = "Unrecognised hex digit: %c";
    const char ERROR_EXPECTED_CLOSING_QUOTE[] PROGMEM = "Expected closing quote.";    
    const char ERROR_MISSING_HEX_PREFIX[] PROGMEM = "Hex value is missing the 0x prefix.";    
    const char ERROR_WRONG_NUMBER_OF_HEX_DIGITS[] PROGMEM = "Expected 1-8 hex digit. Received %u.";    
    const char ERROR_UNRECOGNISED_CODE[] PROGMEM = "Unrecognised code: %s.";    

    // ValidationHelper
    // The range %s-%s will be replaced with the integer parameter's format (eg: %u, %ld, %d, or %ld) before finally
    // injecting the value.
    const char ERROR_INVALID_VALUE[] PROGMEM = "Expected %s: %s-%s.";

    // SerialPort
    const char ERROR_TOO_LONG[] PROGMEM = "%s message too long. Max %u.";

    // Char
    const char ERROR_EXPECTED_SINGLE_CHARACTER[] PROGMEM = "Expected single character.";

    // String
    const char ERROR_STRING_LENGTH_RANGE[] PROGMEM = "String must be %u-%u chars.";
    const char ERROR_STRING_LENGTH_EXACT[] PROGMEM = "String must be %u chars.";

    // Base64
    const char ERROR_BASE64_LENGTH_RANGE[] PROGMEM = "Data must be %u-%u chars.";
    const char ERROR_BASE64_LENGTH_EXACT[] PROGMEM = "Data must be %u chars.";
    const char ERROR_BASE64_CHAR[] PROGMEM = "Unrecongnised base 64 char '%c'.";
  }
}
