#include "../Models/Array.h"
#include "StringHelper.h"

namespace mas::StringHelper
{
  void join(char separator, char const * const * list, uint8_t count, char* const output)
  {
    char* pOut = output;
    for (uint8_t i = 0; i < count; i++)
    {
      char const * input = list[i];
      if (i > 0)
      {
        *(pOut++) = separator;
      }

      while (*input)
      {
        *(pOut++) = *(input++);
      }
    }

    *pOut = '\0';
  }

  // PROGMEM version.
  void join_P(char separator, char const * const * list_P, uint8_t count, char* const output)
  {
    char* pOut = output;
    for (uint8_t i = 0; i < count; i++)
    {
      char const * input_P = list_P[i];
      if (i > 0)
      {
        *(pOut++) = separator;
      }

      char c = pgm_read_byte_near(input_P);
      while (c)
      {
        *(pOut++) = c;
        c = pgm_read_byte_near(++input_P);
      }
    }

    *pOut = '\0';
  }
}
