#include "DatetimeHelper.h"

namespace mas::DatetimeHelper
{
  // Gets the time from the RTC and output it in ISO 8601 format (i.e.: "YYYY-MM-DDThh:mm:ss").
  void toIso(time_t t, char* const output)
  {
    TimeElements tm;
    breakTime(t, tm);

    sprintf(output, ISO_FORMAT, tm.Year + MIN_YEAR, tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second);
  }

  // Try to parse the input in ISO 8601 (eg: "YYYY-MM-DDThh:mm:ss"), and set the RTC.
  bool tryParseIso(const char* const input, time_t &outT, char* const outError)
  {
    int YYYY;
    int MM;
    int DD;
    int hh;
    int mm;
    int ss;

    TimeElements tm;
    sscanf(input, ISO_FORMAT, &YYYY, &MM, &DD, &hh, &mm, &ss);

    if (YYYY < 0 || YYYY > 9999 ||
      MM < 1 || MM > 12 ||
      DD < 1 || DD > 31 ||
      hh < 0 || hh > 23 ||
      mm < 0 || mm > 59 ||
      ss < 0 || ss > 59)
    {
      strlcpy_P(outError, Localisation::ERROR_INVALID_DATE_FORMAT, DataSize::MESSAGE_PARAMETERS_SIZE);
      return false;
    }

    if (YYYY < MIN_YEAR || YYYY > MIN_YEAR + 99)
    {
      sprintf_P(outError, Localisation::ERROR_INVALID_YEAR, MIN_YEAR, MIN_YEAR + 99);
      return false;
    }

    tm.Year = YYYY - MIN_YEAR;
    tm.Month = MM;
    tm.Day = DD;
    tm.Hour = hh;
    tm.Minute = mm;
    tm.Second = ss;

    outT = makeTime(tm);
    return true;
  }
}
