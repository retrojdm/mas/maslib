#pragma once

#include "../Platform.h"
#include "../Constants/Localisation.h"
#include "../Constants/ParameterNames.h"
#include "../Constants/IntegerFormats.h"
#include "../Enums/OffOn.h"
#include "../Models/Array.h"
#include "EnumHelper.h"

namespace mas::ValidationHelper
{
  // Functions /////////////////////////////////////////////////////////////////////////////////////////////////////////

  bool tryParseBoolean(const char* const input, bool &outState, char* const outError);

  // Template functions ////////////////////////////////////////////////////////////////////////////////////////////////
  
  // Tries to parse the given input as an integer (with optional constraints).
  template<typename T>
  bool tryParseInteger(
    const char* const input,
    const char* const name_P,
    T min,
    T max,
    const char* const format_P,
    T &outValue,
    char* const outError)
  {
    char pattern[DataSize::MESSAGE_PARAMETERS_SIZE];
    outError[0] = '\0';
    
    outValue = atol(input);
    if (outValue < min || outValue > max)
    {
      char name[ParameterNames::MAX_LENGTH + 1];
      char format[IntegerFormats::MAX_LENGTH + 1];
      strlcpy_P(name, name_P, ParameterNames::MAX_LENGTH + 1);
      strlcpy_P(format, format_P, IntegerFormats::MAX_LENGTH + 1);    
      sprintf_P(pattern, Localisation::ERROR_INVALID_VALUE, name, format, format);
      sprintf(outError, pattern, min, max);
      return false;
    }

    return true;
  }

  // Tries to parse the given input as an integer (with optional constraints).
  // If the input starts with '+' or '-', the value will be treated as relative to the original.
  template<typename T>
  bool tryParseIntegerRelative(
    const char* const input,
    const char* const name_P,
    T min,
    T max,
    bool wrap,
    const char* const format_P,
    T &outValue,
    char* const outError)
  {
    char pattern[DataSize::MESSAGE_PARAMETERS_SIZE];
    outError[0] = '\0';

    int output = atol(input);
    bool relative = ((input[0] == '-') || (input[0] == '+'));
    if (!relative && (output < min || output > max))
    {
      char name[ParameterNames::MAX_LENGTH + 1];
      char format[IntegerFormats::MAX_LENGTH + 1];
      strlcpy_P(name, name_P, ParameterNames::MAX_LENGTH + 1);
      strlcpy_P(format, format_P, IntegerFormats::MAX_LENGTH + 1);    
      sprintf_P(pattern, Localisation::ERROR_INVALID_VALUE, name, format, format);
      sprintf(outError, pattern, min, max);
      return false;
    }

    if (relative)
    {
      output = outValue + output;
      if (wrap)
      {
        if (output < min)
        {
          output = max + output - min + 1;
        }

        if (output > max)
        {
          output = min + output - max - 1;
        }
      }
      else
      {
        if (output < min)
        {
          output = min;
        }

        if (output > max)
        {
          output = max;
        }
      }
    }

    outValue = output;
    return true;
  }

  // Tries to parse the given input as an array if integers.
  template<typename T>
  bool tryParseArrayOfIntegers(
    const char* const input,
    const char* const name_P,
    T min,
    T max,
    const char* const format_P,
    Array<T>* pOutArray,
    char* const outError)
    {
      pOutArray->setLength(0);
      if (input[0] == '\0')
      {
        return true;
      }

      char buffer[DataSize::MESSAGE_PARAMETERS_SIZE];
      strlcpy(buffer, input, DataSize::MESSAGE_PARAMETERS_SIZE);

      char delimiters[2] = { Encoding::ARRAY_SEPARATOR, '\0' };
      char* token = strtok(buffer, delimiters);
      while (token != NULL && pOutArray->length() < pOutArray->size())
      {
        T value;
        if (!ValidationHelper::tryParseInteger(token, name_P, min, max, format_P, value, outError))
        {
          return false; 
        }

        pOutArray->add(value);
        token = strtok(NULL, delimiters);
      }

      return true;
    }
}
