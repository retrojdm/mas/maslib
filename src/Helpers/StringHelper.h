#pragma once

#include "../Platform.h"
#include "../Models/Array.h"

namespace mas::StringHelper
{
  void join(char separator, char const * const * list, uint8_t count, char* const output);
  void join_P(char separator, char const * const * list_P, uint8_t count, char* const output);
  
  template <size_t InputMaxLength, size_t OutputTokenMaxLength>
  bool split(
    char const * input,
    char separator,
    Array<char *> &outTokens,
    char * const outError)
  {
    outTokens.clear();
    if (input[0] == '\0')
    {
      return true;
    }

    char buffer[InputMaxLength];
    memcpy(buffer, input, InputMaxLength);
    buffer[InputMaxLength - 1] = '\0';

    char delimiters[2] = { separator, '\0' };
    char* end;
    char* token = strtok_r(buffer, delimiters, &end);

    while (token != NULL && outTokens.length() < outTokens.size())
    {
      outTokens.add((const char *)token);
      token = strtok_r(NULL, delimiters, &end);
    }

    return true;
  }
}
