#include "ValidationHelper.h"

namespace mas::ValidationHelper
{
  // Tries to parse the given input as bool from an OffOn enum.  
  bool tryParseBoolean(const char* const input, bool &outState, char* const outError)
  {
    outError[0] = '\0';
    outState = false;

    OffOn offOnValue = OffOn::Off;
    if (!EnumHelper::tryParseEnum(
      input, "state", OFF_ON_NAMES, OFF_ON_COUNT, offOnValue, outError))
    {
      return false;
    }

    outState = offOnValue == OffOn::On;
    return true;
  }
}
