#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Encoding.h"
#include "../Constants/Localisation.h"
#include "ArrayHelper.h"
#include "StringHelper.h"

namespace mas::EnumHelper
{ 
  // Tries to parse the given input as an enum, then gets the next input (the character after the given
  // input's null-terminator);
  template<typename T>
  bool tryParseEnum(
    const char* const input,
    const char* const name,
    const char* const * names_P,
    size_t count,
    T &outValue,
    char* const outError)
  {
    outError[0] = '\0';

    int16_t index = ArrayHelper::findIndex(
      names_P, count, [input] (const char* const name_P) { return strcmp_P(input, name_P) == 0; });

    if (index == -1)
    {
      char enumlist[DataSize::MESSAGE_PARAMETERS_SIZE];
      StringHelper::join_P(Encoding::ARRAY_SEPARATOR, names_P, count, enumlist);
      char output[DataSize::MESSAGE_PARAMETERS_SIZE * 3]; // Room for template + 2 args.
      sprintf_P(output, Localisation::ERROR_INVALID_ENUM, name, enumlist);
      strlcpy(outError, output, DataSize::MESSAGE_PARAMETERS_SIZE);
      return false;
    }

    outValue = (T)index;
    return true;
  }
}