#pragma once

#include "../Platform.h"

namespace mas::ArrayHelper
{
  // Gets the index of the first item in the array that satisfied the predicate (lambda) condition.
  template<typename T, typename Predicate>
  int16_t findIndex(T const * array, uint8_t size, Predicate predicate)
  {
    for (size_t i = 0; i < size; i++)
    {
      if (predicate(array[i]))
      {
        return static_cast<int16_t>(i);
      }
    }

    return -1;
  }

  // This generic helper function is provided to make constructing char * arrays from 2D buffers easier.
  template <uint8_t ARRAY_SIZE, uint8_t STRING_LENGTH>
  void convertToPointerArray(const char (&source)[ARRAY_SIZE][STRING_LENGTH], char* outDest[ARRAY_SIZE]) {
    for (uint8_t i = 0; i < ARRAY_SIZE; i++) {
      outDest[i] = const_cast<char*>(source[i]);
    }
}
}