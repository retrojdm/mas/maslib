#include "SerialisationHelper.h"

namespace mas
{
  // Public functions //////////////////////////////////////////////////////////////////////////////////////////////////

  // Returns true of the given character is a hexidecimal character.
  bool SerialisationHelper::isHexChar(char c)
  {
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
  }
  
  // Serialises a message.
  void SerialisationHelper::serialise(Message * pMessage, char* const output)
  {
    int16_t charsLeft = SERIALISATION_BUFFER - 1;
    char* pOut = const_cast<char*>(output);
    char buffer[DataSize::MESSAGE_PARAMETERS_SIZE];

    // MESSAGE_TYPE_NAMES are in PROGMEM. We have to copy to SRAM first.
    char code[DataSize::MESSAGE_CODE_LENGTH + 3];
    strlcpy_P(code, MESSAGE_TYPE_NAMES[static_cast<uint16_t>(pMessage->type())], sizeof(code));

    escape(code, pOut, charsLeft);

    for (uint8_t i = 0; i < pMessage->maxParameters(); i++)
    {
      const Parameter &parameter = *pMessage->parameters()[i];

      if (parameter.toString(pMessage->memberPtr(i), buffer))
      {
        *(pOut++) = SEPARATOR;
        escape(buffer, pOut, charsLeft);
      }
    }
  }

  // Tries to parse a serialised message.
  // Note: To avoid memory leaks, `outMessage` should initially be nullptr.
  bool SerialisationHelper::tryParse(const char* const input, Message * &outMessage, char* const outError)
  {
    char parameter[DataSize::MESSAGE_PARAMETERS_SIZE];
    char* pIn = const_cast<char*>(input);
    
    if (outMessage != nullptr)
    {
      strlcpy_P(outError, Localisation::ERROR_MESSAGE_NOT_NULL, DataSize::MESSAGE_PARAMETERS_SIZE);
      return false;
    }
    
    MessageType type = MessageType::__COUNT;
    if (!_parseCode(pIn, type, outError))
    {
      return false;
    };

    // We assume the outMessage is initially nullptr. If it's not, we'll have memory leaks.
    // With a very limited memory pool, we'll know about that very quickly.
    outMessage = MESSAGE_FACTORY[static_cast<uint8_t>(type)]();
    if (outMessage == nullptr)
    {
      char name[DataSize::MESSAGE_PARAMETERS_SIZE];
      strlcpy_P(name, MESSAGE_TYPE_NAMES[static_cast<uint8_t>(type)], DataSize::MESSAGE_PARAMETERS_SIZE);
      sprintf_P(outError, Localisation::ERROR_COULDNT_CREATE_MESSAGE, name);
      return false;
    }

    uint8_t count = 0;
    if (!(*pIn == '\r' || *pIn == '\n' || *pIn == '\0'))
    {
      if (*pIn != SEPARATOR)
      {
        strlcpy_P(outError, Localisation::ERROR_EXPECTED_SEPARATOR, DataSize::MESSAGE_PARAMETERS_SIZE);
        delete outMessage;
        outMessage = nullptr;
        return false;
      }

      pIn++;

      while (*pIn)
      {      
        if (count >= outMessage->maxParameters())
        {
          usage(outMessage, parameter);
          sprintf_P(outError, Localisation::ERROR_USAGE, parameter);
          delete outMessage;
          outMessage = nullptr;
          return false;
        }
        
        if (!_unescapeParameter(outMessage, pIn, parameter, outError))
        {
          delete outMessage;
          outMessage = nullptr;
          return false;
        };

        const Parameter * const pParameter = outMessage->parameters()[count];
        if (!pParameter->tryParse(outMessage->memberPtr(count), parameter, outError))
        {
          delete outMessage;
          outMessage = nullptr;
          return false;
        }

        count++;
      }
    }

    if (count < outMessage->minParameters())
    {
      usage(outMessage, parameter);
      sprintf_P(outError, Localisation::ERROR_USAGE, parameter);
      delete outMessage;
      outMessage = nullptr;
      return false;
    }
    
    return true;
  }

  bool SerialisationHelper::tryParseHex(const char* const input, uint32_t &output, char* const outError)
  {
    outError[0] = '\0';
    output = 0x0000;
    char* pCh = const_cast<char*>(input);
    uint8_t digits = 0;

    if (input[0] != '0' || input[1] != 'x')
    {
      strlcpy_P(outError, Localisation::ERROR_MISSING_HEX_PREFIX, DataSize::MESSAGE_PARAMETERS_SIZE);
      return false;
    }

    pCh += 2;
    while (*pCh)
    {
      char c = *(pCh++);

      if (!isHexChar(c))
      {
        sprintf_P(outError, Localisation::ERROR_UNRECOGNISED_HEX_DIGIT, c);
        return false;
      }

      uint8_t nibble = Encoding::ASCII_TO_HEX_VALUE[static_cast<uint8_t>(c)];
      output <<= 4;
      output |= nibble;

      digits++;
    }

    if (digits == 0 || digits > 8)
    {
      sprintf_P(outError, Localisation::ERROR_WRONG_NUMBER_OF_HEX_DIGITS, digits);
      return false;
    }

    return true;
  }

  bool SerialisationHelper::tryParseSlotSettingKey(
    const char* const input, char* const outKey, int8_t &outSlot, char* const outError)
  {
    const uint8_t MAX_COUNT = 2;

    outKey[0] = '\0';
    outSlot = -1;

    char tokenBuffer[MAX_COUNT][DataSize::SETTING_KEY_SIZE];
    char * ptrBuffer[MAX_COUNT];    
    ArrayHelper::convertToPointerArray(tokenBuffer, ptrBuffer);
    Array<char *> tokens(ptrBuffer, MAX_COUNT);

    if (!StringHelper::split<DataSize::SETTING_KEY_SIZE, DataSize::SETTING_KEY_SIZE>(
      input, Encoding::SETTING_KEY_SLOT_SEPARATOR, tokens, outError))
    {
      return false;
    };

    if (tokens.length() < 1)
    {
      // No key or slot, but not technically an error.
      return true;
    }

    strlcpy(outKey, tokens[0], DataSize::SETTING_KEY_SIZE);

    if (tokens.length() < 2)
    {
      // No slot, but not technically an error.
      return true;
    }

    uint8_t slot = -1;
    if (!ValidationHelper::tryParseInteger<uint8_t>(
      tokens[1], PSTR("value"), 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U, slot, outError))
    {
      return false;
    };

    outSlot = slot;
    return true;
  }

  // Outputs a string, escaped if neccessary (i.e.: Is blank, contains spaces, or special characters).
  // Increments the char output cursor.
  void SerialisationHelper::escape(const char* const input, char* &pOut, int16_t &outCharsLeft)
  {
    const char* pIn = input;
    char* const outputStart = pOut;
    bool encountredSpecialCharOrSeparator = false;
    bool full = false;
    
    while (*pIn)
    {
      char c = *(pIn++);

      if (full)
      {
        continue;
      }

      int8_t index = ArrayHelper::findIndex(
        Encoding::SPECIAL_CHARACTERS,
        Encoding::SPECIAL_CHARACTER_COUNT,
        [c] (const SpecialCharacter &x) { return x.character == c; });

      // Escape special chars.
      if (index != -1)
      {
        int8_t charsToDeduct = 2 + (encountredSpecialCharOrSeparator ? 0 : 2);
        if (outCharsLeft < charsToDeduct)
        {
          full = true;
          continue;
        }

        encountredSpecialCharOrSeparator = true;
        outCharsLeft -= charsToDeduct;

        *(pOut++) = ESCAPE_CHAR;
        *(pOut++) = Encoding::SPECIAL_CHARACTERS[index].escapeCode;
      }

      // Hex-encode non-printable chars.
      else if (c < FIRST_PRINTABLE_CHAR || c > LAST_PRINTABLE_CHAR)
      {
        int8_t charsToDeduct = 4 + (encountredSpecialCharOrSeparator ? 0 : 2);
        if (outCharsLeft < charsToDeduct)
        {
          full = true;
          continue;
        }

        encountredSpecialCharOrSeparator = true;
        outCharsLeft -= charsToDeduct;

        *(pOut++) = ESCAPE_CHAR;
        *(pOut++) = ESCAPE_HEX_CHAR;
        *(pOut++) = Encoding::HEX_CHARS[(c & 0xF0) >> 4];
        *(pOut++) = Encoding::HEX_CHARS[c & 0x0F];
      }

      // Print space, and remember to surround with quotes later.
      else if (c == SEPARATOR)
      {
        int8_t charsToDeduct = 1 + (encountredSpecialCharOrSeparator ? 0 : 2);
        if (outCharsLeft < charsToDeduct)
        {
          full = true;
          continue;
        }

        encountredSpecialCharOrSeparator = true;
        outCharsLeft -= charsToDeduct;

        *(pOut++) = c;
      }

      // Just print printable chars.
      else 
      {
        if (outCharsLeft < 1)
        {
          full = true;
          continue;
        }

        *(pOut++) = c;
        outCharsLeft--;
      }
    }

    pIn++;

    if (pOut == outputStart)
    {
      // This is a weird case. We usually account for quotes with the `charsToDeduct` variable, but we need to manually
      // deduct for the quotes that surround an empty string.
      outCharsLeft -= 2;
    }

    // Shift forware by one char, and surround with double-quotes.
    if (encountredSpecialCharOrSeparator || pOut == outputStart)
    {
      char* pChar = pOut;
      while (pChar >= outputStart)
      {
        *(pChar + 1) = *pChar;
        pChar--;
      }

      *outputStart = QUOTE_CHAR;
      pOut++;
      *(pOut++) = QUOTE_CHAR;
    }

    *pOut = '\0';
  }

  void SerialisationHelper::usage(Message * pMessage, char * const output)
  {
    strlcpy_P(output, MESSAGE_TYPE_NAMES[static_cast<uint16_t>(pMessage->type())], DataSize::MESSAGE_PARAMETERS_SIZE);
    char separator[2] = { SEPARATOR, '\0' };

    char token[20];
    for (uint8_t i = 0; i < pMessage->maxParameters(); i++)
    {
      const Parameter * const pParameter = pMessage->parameters()[i];
      pParameter->usage(token);
      strcat(output, separator);
      strcat(output, token);
    }
  }

  // "Private" functions ///////////////////////////////////////////////////////////////////////////////////////////////

  // Parse the command or event code.
  bool SerialisationHelper::_parseCode(char* &pIn, MessageType &outType, char* const outError)
  {    
    char code[DataSize::MESSAGE_CODE_LENGTH + 3];
    char* pOut = code;
    for (uint8_t i = 0; i < DataSize::MESSAGE_CODE_LENGTH + 2; i++)
    {
      char c = *pIn;

      // We check before incrementing the pointer, so that we can perform another check on the same char in the main
      // tryParse function.
      if (c == '\0' || c == '\n' || c == '\r' || c == SEPARATOR)
      {
        break;
      }

      pIn++;
      *(pOut++) = _toUpper(c);
    }

    *(pOut++) = '\0';

    int16_t index = ArrayHelper::findIndex(
      MESSAGE_TYPE_NAMES, // PROGMEM
      MESSAGE_TYPE_COUNT,
      [code] (const char* const code_P) { return strcmp_P(code, code_P) == 0; });

    if (index == -1)
    {
      sprintf_P(outError, Localisation::ERROR_UNRECOGNISED_CODE, code);
      return false;
    }

    outType = static_cast<MessageType>(index);
    return true;
  }

  // Unescape a single parameter.
  bool SerialisationHelper::_unescapeParameter(
    Message * pMessage, char* &pIn, char* const output, char* const outError)
  {
    output[0] = '\0';
    outError[0] = '\0';

    bool withinQuotes = false;
    uint8_t hexChars = 0;
    char hexValue = 0;
    int8_t index;
    ParseMode mode = ParseMode::Normal;
    char* pOut = output;

    // Ignore additional spaces.
    while (*pIn == SEPARATOR)
    {
      pIn++;
    }

    bool valid = true;
    bool done = false;
    int8_t nibble = 0;

    while (*pIn && !done)
    {
      char c = *(pIn++);
      switch (mode)
      {
        case ParseMode::Normal:
          if (c == SEPARATOR)
          {
            if (!withinQuotes)
            {
              done = true;
            }
            else
            {
              *(pOut++) = c;
            }
          }
          else if (c == QUOTE_CHAR)
          {
            withinQuotes = !withinQuotes;
          }
          else if (c == ESCAPE_CHAR)
          {
            if (!withinQuotes)
            {
              strlcpy_P(outError, Localisation::ERROR_ESCAPE_CODES, DataSize::MESSAGE_PARAMETERS_SIZE);
              valid = false;
              done = true;
              continue;
            }

            mode = ParseMode::Escaping;
          }
          else if (c > FIRST_PRINTABLE_CHAR && c <= LAST_PRINTABLE_CHAR)
          {
            *(pOut++) = c;
          }
          else
          {
            strlcpy_P(outError, Localisation::ERROR_NON_PRINTABLE, DataSize::MESSAGE_PARAMETERS_SIZE);
            valid = false;
            done = true;
          }
          
          break;

        case ParseMode::Escaping:
          if (c == ESCAPE_HEX_CHAR)
          {
            mode = ParseMode::EscapingHex;
            hexChars = 0;
            hexValue = 0;
            continue;
          }
          
          index = ArrayHelper::findIndex(
            Encoding::SPECIAL_CHARACTERS,
            Encoding::SPECIAL_CHARACTER_COUNT,
            [c] (const SpecialCharacter &x) { return x.escapeCode == c; });

          if (index == -1)
          {
            sprintf_P(outError, Localisation::ERROR_UNRECOGNISED_ESCAPE_CODE, c);
            valid = false;
            done = true;
            continue;
          }

          *(pOut++) = Encoding::SPECIAL_CHARACTERS[index].character;
          mode = ParseMode::Normal;
          break;

        case ParseMode::EscapingHex:
          if (!isHexChar(c))
          {
            sprintf_P(outError, Localisation::ERROR_UNRECOGNISED_HEX_DIGIT, c);
            valid = false;
            done = true;
            continue;
          }

          nibble = Encoding::ASCII_TO_HEX_VALUE[static_cast<uint8_t>(c)];
          hexValue <<= 4;
          hexValue |= nibble;

          hexChars++;
          if (hexChars == 2)
          {
            mode = ParseMode::Normal;
          }

          break;

        default:
          break;
      }
    }

    if (withinQuotes)
    {
      strlcpy_P(outError, Localisation::ERROR_EXPECTED_CLOSING_QUOTE, DataSize::MESSAGE_PARAMETERS_SIZE);
      valid = false;
    }

    *(pOut++) = '\0';

    return valid;
  }

  // Converts a character to it's uppercase.
  char SerialisationHelper::_toUpper(char c)
  {
    if (c >= 'a' && c <= 'z')
    {
      c -= ('a' - 'A');
    }

    return c;
  }
}
