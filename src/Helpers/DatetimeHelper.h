#pragma once

#if defined(ARDUINO)
#include <TimeLib.h>
#endif

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Localisation.h"

namespace mas::DatetimeHelper
{
  static const int16_t MIN_YEAR = 1970;
  static constexpr const char ISO_FORMAT[] = "%04u-%02u-%02uT%02u:%02u:%02u"; // Not in PROGMEM, for use with sscanf

  void toIso(time_t t, char* const output);
  bool tryParseIso(const char* const input, time_t &outT, char* const outError);
}
