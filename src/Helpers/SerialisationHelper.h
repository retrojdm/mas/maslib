#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Encoding.h"
#include "../Constants/Localisation.h"
#include "../Constants/MessageFactory.h"
#include "../Constants/Module.h"
#include "../Enums/LogLevel.h"
#include "../Enums/MessageType.h"
#include "../Enums/ParseMode.h"
#include "../Models/Message.h"
#include "ArrayHelper.h"
#include "ValidationHelper.h"

namespace mas::SerialisationHelper
{
  // Same size as the Serial RX and TX buffers on an ATMEGA1284.
  const uint8_t SERIALISATION_BUFFER = 64;

  bool isHexChar(char c);
  void serialise(Message * pMessage, char* const output);
  bool tryParse(const char* const input, Message * &outMessage, char* const outError);
  bool tryParseHex(const char* const input, uint32_t &output, char* const outError);
  bool tryParseSlotSettingKey(const char* const input, char* const outKey, int8_t &outSlot, char* const outError);
  void escape(const char* const input, char* &pOut, int16_t &outCharsLeft);
  void usage(Message * pMessage, char * const output);

  const char EVENT_OPEN = '[';
  const char EVENT_CLOSE = ']';
  const char SEPARATOR = ' ';
  const char ESCAPE_CHAR = '\\';
  const char ESCAPE_HEX_CHAR = 'x';
  const char QUOTE_CHAR = '"';
  const char FIRST_PRINTABLE_CHAR = ' ';
  const char LAST_PRINTABLE_CHAR = '~';

  bool _parseCode(char* &pIn, MessageType &outType, char* const outError);
  bool _unescapeParameter(Message * pMessage, char* &pIn, char* const output, char* const outError);
  char _toUpper(char c);
}
