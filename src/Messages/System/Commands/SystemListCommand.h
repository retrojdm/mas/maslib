#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // SLS
  // List all known modules.
  class SystemListCommand : public Message
  {
  public:
    SystemListCommand() {}
    
    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemListCommand; }
  };
}