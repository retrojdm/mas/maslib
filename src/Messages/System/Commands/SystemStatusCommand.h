#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/SystemAction.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalChar.h"
#include "../../../Parameters/OptionalEnum.h"

namespace mas
{
  // SYS <action?> <key?>
  // `start` or `stop` all, or a specified module.
  class SystemStatusCommand : public Message
  {
  public:
    Optional<SystemAction> action;
    Optional<char> key;

    SystemStatusCommand() : action(), key() {}
    SystemStatusCommand(const SystemAction action) : action(action), key() {}
    SystemStatusCommand(const SystemAction action, const char key) : action(action), key(key) {}

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemStatusCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&action;
        case 1: return (void*)&key;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const OptionalEnum ACTION = OptionalEnum(
      ParameterNames::ACTION, SYSTEM_ACTION_NAMES, SYSTEM_ACTION_COUNT);

    static constexpr const OptionalChar KEY = OptionalChar(
      ParameterNames::KEY);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &ACTION,
      &KEY,
    };
  };
}