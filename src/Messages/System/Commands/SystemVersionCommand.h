#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"

namespace mas
{
  // SVE
  // Get firmware version.
  class SystemVersionCommand : public Message
  {
  public:
    SystemVersionCommand() {}
    
    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemVersionCommand; }
  };
}