#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/LogLevel.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Char.h"
#include "../../../Parameters/Enum.h"
#include "../../../Parameters/String.h"

namespace mas
{  
  // [SLG] <level> <key> <message>
  // System log. [I]nfo, [W]arning, [E]rror.
  class SystemLogEvent : public Message
  {
  public:
    LogLevel level;
    char key;
    char message[DataSize::MESSAGE_PARAMETERS_SIZE];

    SystemLogEvent() : level(), key()
    {
      message[0] = '\0';
    }

    SystemLogEvent(const LogLevel level, const char key, const char* const message) : level(level), key(key)
    {
      strlcpy(this->message, message, DataSize::MESSAGE_PARAMETERS_SIZE);
    }

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemLogEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&level;
        case 1: return (void*)&key;
        case 2: return (void*)message;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 3;
    static const uint8_t MAX_PARAMETERS = 3;

    static constexpr const Enum LEVEL = Enum(
      ParameterNames::LEVEL, LOG_LEVEL_NAMES, LOG_LEVEL_COUNT);

    static constexpr const Char KEY = Char(
      ParameterNames::KEY);

    static constexpr const String MESSAGE = String(
      ParameterNames::MESSAGE, 0, DataSize::MESSAGE_PARAMETERS_SIZE);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &LEVEL,
      &KEY,
      &MESSAGE,
    };
  };
}