#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/LogLevel.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/String.h"

namespace mas
{
  // [SVE] <unit> <version>
  // Main unit firmware version.
  class SystemVersionEvent : public Message
  {
  public:
    char unit[12]; // Eg: MU50 or FP20-OLED, etc
    char version[12]; // Semantic versioning (MAJOR.MINOR.PATCH) as a string, Eg: 1.2.0

    SystemVersionEvent()
    {
      unit[0] = '\0';
      version[0] = '\0';
    }

    SystemVersionEvent(const char* const unit, const char* const version)
    {
      strlcpy(this->unit, unit, sizeof(this->unit));
      strlcpy(this->version, version, sizeof(this->version));
    }

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemVersionEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)unit;
        case 1: return (void*)version;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const String UNIT = String(
      ParameterNames::UNIT, 0, sizeof(unit) - 1);

    static constexpr const String VERSION = String(
      ParameterNames::VERSION, 0, sizeof(version) - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &UNIT,
      &VERSION,
    };
  };
}