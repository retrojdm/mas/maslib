#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Enums/SystemStatus.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Char.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // [SYS] <status> <key>
  // Status of a module.
  class SystemStatusEvent : public Message
  {
  public:
    SystemStatus status;
    char key;

    SystemStatusEvent() : status(), key() {}
    SystemStatusEvent(SystemStatus status) : status(status), key() {}
    SystemStatusEvent(SystemStatus status, const char key) : status(status), key(key) {}

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemStatusEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&status;
        case 1: return (void*)&key;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Enum STATUS = Enum(
      ParameterNames::STATUS, SYSTEM_STATUS_NAMES, SYSTEM_STATUS_COUNT);

    static constexpr const Char KEY = Char(
      ParameterNames::KEY);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &STATUS,
      &KEY,
    };
  };
}