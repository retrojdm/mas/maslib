#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/ArrayOfChar.h"

namespace mas
{
  // [SLS] <keys[]>
  // Pipe-separated list of known modules (keys).
  class SystemListEvent : public Message
  {
  public:
    static const uint8_t KEY_COUNT_MAX = 20;

    Array<char> keys;

    SystemListEvent() : keys(_keys, KEY_COUNT_MAX) {}
    SystemListEvent(const Array<char> keys) : keys(_keys, keys) {}

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemListEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&keys;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const ArrayOfChar KEYS = ArrayOfChar(ParameterNames::KEYS);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &KEYS,
    };

    char _keys[KEY_COUNT_MAX];
  };
}