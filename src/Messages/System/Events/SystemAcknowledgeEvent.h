#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Char.h"

namespace mas
{
  // [SAK] <key>
  // Acknowledges that the given module recieved the command.
  class SystemAcknowledgeEvent : public Message
  {
  public:
    char key;

    SystemAcknowledgeEvent() : key() {}
    SystemAcknowledgeEvent(const char key) : key(key) {}

    char topic() const override { return Module::SYSTEM_KEY; }
    MessageType type() const override { return MessageType::SystemAcknowledgeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&key;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Char KEY = Char(ParameterNames::KEY);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &KEY,
    };
  };
}