#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Relative.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/OptionalRelativeInteger.h"

namespace mas
{
  // RFR <slot> <frequency?>
  // Get or set frequency (prefix with + or - to make relative).
  class RadioFrequencyCommand : public Message
  {
  public:
    uint8_t slot;
    Optional<Relative<int16_t>> frequency;

    RadioFrequencyCommand() : slot(), frequency() {}
    RadioFrequencyCommand(const uint8_t slot) : slot(slot), frequency() {}
    RadioFrequencyCommand(const uint8_t slot, const Relative<int16_t> frequency) : slot(slot), frequency(frequency) {}

    char topic() const override { return Module::RADIO_KEY; }
    MessageType type() const override { return MessageType::RadioFrequencyCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&frequency;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const OptionalRelativeInteger<int16_t> FREQUENCY = OptionalRelativeInteger<int16_t>(
      ParameterNames::FREQUENCY, -32768, 32767, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &FREQUENCY,
    };
  };
}