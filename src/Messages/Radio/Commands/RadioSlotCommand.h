#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // RSL
  // Get the current bluetooth card's slot index.
  class RadioSlotCommand : public Message
  {
  public:
    RadioSlotCommand() {}

    char topic() const override { return Module::POWER_KEY; }
    MessageType type() const override { return MessageType::RadioSlotCommand; }
  };
}