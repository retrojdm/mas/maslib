#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/RadioBand.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/ArrayOfEnum.h"

namespace mas
{
  // [RBL] <slot> <bands[]>
  // Pipe-separated list of list of supported bands.
  class RadioBandListEvent : public Message
  {
  public:
    uint8_t slot;
    Array<RadioBand> bands;

    RadioBandListEvent() : slot(), bands(_bands, RADIO_BAND_COUNT) {}
    RadioBandListEvent(const uint8_t slot, const Array<RadioBand> bands) : slot(slot), bands(_bands, bands) {}

    char topic() const override { return Module::RADIO_KEY; }
    MessageType type() const override { return MessageType::RadioBandListEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&bands;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const ArrayOfEnum BANDS = ArrayOfEnum(
      ParameterNames::BANDS, RADIO_BAND_NAMES, RADIO_BAND_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &BANDS,
    };

    RadioBand _bands[RADIO_BAND_COUNT];
  };
}