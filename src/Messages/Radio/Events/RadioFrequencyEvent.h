#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [RFR] <slot> <frequency>
  // Frequency.
  class RadioFrequencyEvent : public Message
  {
  public:
    uint8_t slot;
    uint16_t frequency;

    RadioFrequencyEvent() : slot(), frequency() {}
    RadioFrequencyEvent(const uint8_t slot, const uint16_t frequency) : slot(slot), frequency(frequency) {}

    char topic() const override { return Module::RADIO_KEY; }
    MessageType type() const override { return MessageType::RadioFrequencyEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&frequency;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const Integer<uint16_t> FREQUENCY = Integer<uint16_t>(
      ParameterNames::FREQUENCY, 0, 65535, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &FREQUENCY,
    };
  };
}