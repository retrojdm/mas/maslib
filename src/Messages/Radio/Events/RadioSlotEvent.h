#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/String.h"

namespace mas
{
  // [RSL] <slot>
  // The current radio card's slot index.
  class RadioSlotEvent : public Message
  {
  public:
    uint8_t slot;

    RadioSlotEvent(): slot()
    {
    }

    RadioSlotEvent(const uint8_t slot): slot(slot)
    {
    }

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::RadioSlotEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Integer<int8_t> SLOT = Integer<int8_t>(
      ParameterNames::SLOT, -1, DataSize::CARD_SLOTS - 1, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
    };
  };
}