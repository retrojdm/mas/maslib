#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // [XEC] <state>
  // A value indicating whether serial command echo is enabled.
  class SerialEchoEvent : public Message
  {
  public:
    bool state;

    SerialEchoEvent() : state() {}
    SerialEchoEvent(const bool state) : state(state) {}

    char topic() const override { return Module::SERIAL_KEY; }
    MessageType type() const override { return MessageType::SerialEchoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&state;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Enum STATE = Enum(
      ParameterNames::STATE, OFF_ON_NAMES, OFF_ON_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &STATE,
    };
  };
}