#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalEnum.h"

namespace mas
{
  // XFC <state?>
  // Get or set a value indicating whether ETX/ACK flow control is enabled. Only effects the serial port you're
  // currently on.
  class SerialFlowControlCommand : public Message
  {
  public:
    Optional<bool> state;

    SerialFlowControlCommand() : state() {}
    SerialFlowControlCommand(const bool state) : state(state) {}
    
    char topic() const override { return Module::SERIAL_KEY; }
    MessageType type() const override { return MessageType::SerialFlowControlCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&state;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalEnum STATE = OptionalEnum(
      ParameterNames::STATE, OFF_ON_NAMES, OFF_ON_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &STATE,
    };
  };
}