#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/ArrayOfInteger.h"

namespace mas
{
  // QLV <levels[]>
  // Get or set comma-separated list of EQ levels.
  class EqualiserLevelsCommand : public Message
  {
  public:
    static const int8_t LEVEL_VALUE_MAX = 12;
    static const uint8_t LEVEL_COUNT_MAX = 8;

    Array<int8_t> levels;

    EqualiserLevelsCommand() : levels(_levels, LEVEL_COUNT_MAX) {}
    EqualiserLevelsCommand(const Array<int8_t> levels) : levels(_levels, levels) {}

    char topic() const override { return Module::EQUALISER_KEY; }
    MessageType type() const override { return MessageType::EqualiserLevelsCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&levels;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const ArrayOfInteger<int8_t> LEVELS =
      ArrayOfInteger<int8_t>(ParameterNames::LEVELS, -LEVEL_VALUE_MAX, LEVEL_VALUE_MAX, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &LEVELS,
    };

    int8_t _levels[LEVEL_COUNT_MAX];
  };
}