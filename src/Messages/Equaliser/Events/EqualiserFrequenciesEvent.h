#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/ArrayOfInteger.h"

namespace mas
{
  // [QFR] <frequencies[]>
  // A comma-separated list of EQ center frequencies.
  class EqualiserFrequenciesEvent : public Message
  {
  public:
    static const uint8_t FREQUENCY_COUNT_MAX = 8;
    
    Array<uint16_t> frequencies;

    EqualiserFrequenciesEvent() : frequencies(_frequencies, FREQUENCY_COUNT_MAX) {}
    EqualiserFrequenciesEvent(const Array<uint16_t> frequencies) : frequencies(_frequencies, frequencies) {}

    char topic() const override { return Module::EQUALISER_KEY; }
    MessageType type() const override { return MessageType::EqualiserFrequenciesEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&frequencies;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const ArrayOfInteger<uint16_t> FREQUENCIES =
      ArrayOfInteger<uint16_t>(ParameterNames::FREQUENCIES, 50, 22050, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &FREQUENCIES,
    };

    uint16_t _frequencies[FREQUENCY_COUNT_MAX];
  };
}