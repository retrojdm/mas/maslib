#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/Effect.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // FPL <effect>
  // Play sound effect. Eg: beep or XDR toneburst etc.
  class EffectsPlayCommand : public Message
  {
  public:
    Effect effect;

    EffectsPlayCommand() : effect() {}
    EffectsPlayCommand(const Effect effect) : effect(effect) {}

    char topic() const override { return Module::EFFECTS_KEY; }
    MessageType type() const override { return MessageType::EffectsPlayCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&effect;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Enum EFFECT = Enum(
      ParameterNames::EFFECT, EFFECT_NAMES, EFFECT_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &EFFECT,
    };
  };
}