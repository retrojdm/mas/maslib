#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Enums/ClockTickMode.h"
#include "../../../Models/Message.h"

namespace mas
{
  // [FST]
  // Effect complete.
  class EffectsCompleteEvent : public Message
  {
  public:
    EffectsCompleteEvent() {}
    
    char topic() const override { return Module::EFFECTS_KEY; }
    MessageType type() const override { return MessageType::EffectsCompleteEvent; }
  };
}