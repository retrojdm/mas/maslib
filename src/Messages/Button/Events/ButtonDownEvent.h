#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [BDN] <index>
  // Button down.
  class ButtonDownEvent : public Message
  {

  public:
    uint8_t index;

    ButtonDownEvent() : index() {}
    ButtonDownEvent(const uint8_t index) : index(index) {}

    char topic() const override { return Module::BUTTON_KEY; }
    MessageType type() const override { return MessageType::ButtonDownEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&index;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Integer<uint8_t> INDEX = Integer<uint8_t>(
      ParameterNames::INDEX, 0, 255, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &INDEX,
    };
  };
}