#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/AnalyserMode.h"
#include "../../../Enums/StereoMode.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Enum.h"
#include "../../../Parameters/OptionalEnum.h"
#include "../../../Parameters/OptionalInteger.h"

namespace mas
{
  // [AMO] <mode> <stereo?> <fps?> <size?>
  // Audio analyser mode, stereo mode, frame rate, and size.
  class AnalyserModeEvent : public Message
  {
  public:
    AnalyserMode mode;
    Optional<bool> stereo;
    Optional<uint8_t> fps;
    Optional<uint8_t> size;

    AnalyserModeEvent() : mode(), stereo(), fps(), size() {}
    AnalyserModeEvent(const AnalyserMode mode) : mode(mode), stereo(), fps(), size() {}
    AnalyserModeEvent(const AnalyserMode mode, const bool stereo, const uint8_t fps) :
      mode(mode), stereo(stereo), fps(fps), size() {}

    AnalyserModeEvent(
      const AnalyserMode mode, const Optional<bool> stereo, const Optional<uint8_t> fps, const Optional<uint8_t> size) :
      mode(mode), stereo(stereo), fps(fps), size(size) {}

    char topic() const override { return Module::ANALYSER_KEY; }
    MessageType type() const override { return MessageType::AnalyserModeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&mode;
        case 1: return (void*)&stereo;
        case 2: return (void*)&fps;
        case 3: return (void*)&size;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 4;

    static constexpr const Enum MODE = Enum(
      ParameterNames::MODE, ANALYSER_MODE_NAMES, ANALYSER_MODE_COUNT);

    static constexpr const OptionalEnum STEREO = OptionalEnum(
      ParameterNames::STEREO, STEREO_MODE_NAMES, STEREO_MODE_COUNT);

    static constexpr const OptionalInteger<uint8_t> FPS = OptionalInteger<uint8_t>(
      ParameterNames::FPS, 1, 30, IntegerFormats::U);

    static constexpr const OptionalInteger<uint8_t> SIZE = OptionalInteger<uint8_t>(
      ParameterNames::SIZE, 3, DataSize::MESSAGE_PARAMETERS_SIZE - 1, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &MODE,
      &STEREO,
      &FPS,
      &SIZE,
    };
  };
}