#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [APM] <value>
  // Audio analyser mono Fourier Transform data (base 64).
  class AnalyserPeaksMonoEvent : public Message
  {
  public:
    uint8_t value;

    AnalyserPeaksMonoEvent() : value() {}
    AnalyserPeaksMonoEvent(const uint8_t value) : value(value) {}

    char topic() const override { return Module::ANALYSER_KEY; }
    MessageType type() const override { return MessageType::AnalyserPeaksMonoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&value;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Integer<uint8_t> VALUE = Integer<uint8_t>(
      ParameterNames::VALUE, 0, 255, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &VALUE,
    };
  };
}