#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [APS] <left> <right>
  // Audio analyser stereo peak data (0..255 per channel).
  class AnalyserPeaksStereoEvent : public Message
  {
  public:
    uint8_t left;
    uint8_t right;

    AnalyserPeaksStereoEvent() : left(), right() {}
    AnalyserPeaksStereoEvent(const uint8_t left, const uint8_t right) : left(left), right(right) {}

    char topic() const override { return Module::ANALYSER_KEY; }
    MessageType type() const override { return MessageType::AnalyserPeaksStereoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&left;
        case 1: return (void*)&right;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> LEFT = Integer<uint8_t>(
      ParameterNames::LEFT, 0, 255, IntegerFormats::U);

    static constexpr const Integer<uint8_t> RIGHT = Integer<uint8_t>(
      ParameterNames::RIGHT, 0, 255, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &LEFT,
      &RIGHT,
    };
  };
}