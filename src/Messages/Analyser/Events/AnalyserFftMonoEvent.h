#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Base64.h"

namespace mas
{
  // [AFM] <data>
  // Audio analyser mono Fourier Transform data (base 64).
  class AnalyserFftMonoEvent : public Message
  {
  public:
    Array<uint8_t> data;

    AnalyserFftMonoEvent() : data(_data, DataSize::ANALYSER_MONO) {}
    AnalyserFftMonoEvent(Array<uint8_t> data) : data(_data, data) {}

    char topic() const override { return Module::ANALYSER_KEY; }
    MessageType type() const override { return MessageType::AnalyserFftMonoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }
    
    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&data;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Base64 DATA = Base64(ParameterNames::DATA, 3, DataSize::ANALYSER_MONO);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &DATA,
    };

    uint8_t _data[DataSize::ANALYSER_MONO];
  };
}