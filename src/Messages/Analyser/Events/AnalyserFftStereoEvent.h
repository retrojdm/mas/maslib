#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Base64.h"

namespace mas
{
  // [AFS] <left> <right>
  // Audio analyser stereo Fourier Transform data (base 64).
  class AnalyserFftStereoEvent : public Message
  {
  public:
    Array<uint8_t> left;
    Array<uint8_t> right;

    AnalyserFftStereoEvent() : left(_left, DataSize::ANALYSER_STEREO), right(_right, DataSize::ANALYSER_STEREO) {}
    AnalyserFftStereoEvent(Array<uint8_t> left, Array<uint8_t> right) : left(_left, left), right(_right, right) {}

    char topic() const override { return Module::ANALYSER_KEY; }
    MessageType type() const override { return MessageType::AnalyserFftStereoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS;  }
    
    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&left;
        case 1: return (void*)&right;
        default: return nullptr;
      }
    }
 
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Base64 LEFT = Base64(ParameterNames::LEFT, 3, DataSize::ANALYSER_STEREO);
    static constexpr const Base64 RIGHT = Base64(ParameterNames::RIGHT, 3, DataSize::ANALYSER_STEREO);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &LEFT,
      &RIGHT,
    };
    
    uint8_t _left[DataSize::ANALYSER_STEREO];
    uint8_t _right[DataSize::ANALYSER_STEREO];
  };
}