#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"

namespace mas
{
  // ILS
  // List audio source types.
  class AudioInputListCommand : public Message
  {
  public:
    AudioInputListCommand() {}

    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputListCommand; }
  };
}