#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"

namespace mas
{
  // ICO
  // Get audio source count.
  class AudioInputCountCommand : public Message
  {
  public:
    AudioInputCountCommand() {}
    
    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputCountCommand; }
  };
}