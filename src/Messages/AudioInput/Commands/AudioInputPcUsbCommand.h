#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"

namespace mas
{
  // IPC
  // Get PC USB connection state.
  class AudioInputPcUsbCommand : public Message
  {
  public:
    AudioInputPcUsbCommand() {}
    
    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputPcUsbCommand; }
  };
}