#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Relative.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalRelativeInteger.h"

namespace mas
{
  // IDX <index?>
  // Get or set audio source index. Prefix value with '+' or '-' to make relative.
  class AudioInputIndexCommand : public Message
  {
  public:
    Optional<Relative<int8_t>> index;

    AudioInputIndexCommand() : index() {}
    AudioInputIndexCommand(const Relative<int8_t> index) : index(index) {}

    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputIndexCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&index;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;
    
    static constexpr const OptionalRelativeInteger<int8_t> INDEX = OptionalRelativeInteger<int8_t>(
      ParameterNames::INDEX, -4, 4, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &INDEX,
    };
  };
}