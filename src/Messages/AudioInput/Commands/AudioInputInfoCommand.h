#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalInteger.h"

namespace mas
{
  // INF <index?>
  // Get current or specific audio source info.
  class AudioInputInfoCommand : public Message
  {
  public:
    Optional<uint8_t> index;

    AudioInputInfoCommand() : index() {}
    AudioInputInfoCommand(const Optional<uint8_t> index) : index(index) {}

    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputInfoCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&index;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const char INDEX_NAME[] PROGMEM = "index";

    static constexpr const OptionalInteger<uint8_t> INDEX = OptionalInteger<uint8_t>(
      ParameterNames::INDEX, 0, DataSize::AUDIO_INPUT_MAX - 1, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &INDEX,
    };
  };
}