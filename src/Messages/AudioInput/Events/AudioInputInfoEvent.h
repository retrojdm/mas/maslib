#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/AudioInputType.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/Enum.h"
#include "../../../Parameters/OptionalInteger.h"

namespace mas
{
  // [INF] <index> <type> <slot> <id> <suffix?>
  // `<type>` can be: `PC`, `MED`, `LIN`, `RAD`, or `BLU`.
  // `<slot>` is `-1` when N/A.
  // `<id>` is "-" when N/A. Otherwise it's something like `BLU`, `RAD`, `RAT`, `RAS`, `LIN`, `LIO`, etc.
  // `<suffix>` is used when multiple of the same type exist.
  class AudioInputInfoEvent : public Message
  {
  public:
    uint8_t index;
    AudioInputType inputType;
    int8_t slot;
    char id[DataSize::CARD_ID_SIZE];
    Optional<uint8_t> suffix;

    AudioInputInfoEvent() :
      index(), inputType(), slot(), suffix()
    {
      id[0] = '\0';
    }

    AudioInputInfoEvent(const uint8_t index, const AudioInputType type, const uint8_t slot, const char* const id) :
      index(index), inputType(type), slot(slot), suffix()
    {
      strlcpy(this->id, id, DataSize::CARD_ID_SIZE);
    }

    AudioInputInfoEvent(const uint8_t index, const AudioInputType type, const uint8_t slot, const char* const id, uint8_t suffix) :
      index(index), inputType(type), slot(slot), suffix(suffix)
    {
      strlcpy(this->id, id, DataSize::CARD_ID_SIZE);
    }

    AudioInputInfoEvent(const uint8_t index, const AudioInputType type, const uint8_t slot, const char* const id, Optional<uint8_t> suffix) :
      index(index), inputType(type), slot(slot), suffix(suffix)
    {
      strlcpy(this->id, id, DataSize::CARD_ID_SIZE);
    }

    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputInfoEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }
    
    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&index;
        case 1: return (void*)&inputType;
        case 2: return (void*)&slot;
        case 3: return (void*)id;
        case 4: return (void*)&suffix;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 4;
    static const uint8_t MAX_PARAMETERS = 5;

    static constexpr const Integer<uint8_t> INDEX = Integer<uint8_t>(
      ParameterNames::INDEX, 0, DataSize::AUDIO_INPUT_MAX - 1, IntegerFormats::U);

    static constexpr const Enum TYPE = Enum(
      ParameterNames::TYPE, AUDIO_INPUT_TYPE_NAMES, AUDIO_INPUT_TYPE_COUNT);

    static constexpr const Integer<int8_t> SLOT = Integer<int8_t>(
      ParameterNames::SLOT, -1, DataSize::CARD_SLOTS - 1, IntegerFormats::D);

    // Min size is 1 to accomodate ID = '-'.
    static constexpr const String ID = String(
      ParameterNames::ID, 1, DataSize::CARD_ID_SIZE - 1, Encoding::EMPTY_VALUE_PLACEHOLDER);

    static constexpr const OptionalInteger<uint8_t> SUFFIX = OptionalInteger<uint8_t>(
      ParameterNames::SUFFIX, -1, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &INDEX,
      &TYPE,
      &SLOT,
      &ID,
      &SUFFIX,
    };
  };
}