#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/AudioInputType.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/ArrayOfEnum.h"

namespace mas
{
  // [ILS] <types[]>
  // Pipe-separated list of audio sources.
  class AudioInputListEvent : public Message
  {
  public:
    Array<AudioInputType> types;

    AudioInputListEvent() : types(_types, DataSize::AUDIO_INPUT_MAX) {}
    AudioInputListEvent(const Array<AudioInputType> types) : types(_types, types) {}

    char topic() const override { return Module::AUDIO_INPUT_KEY; }
    MessageType type() const override { return MessageType::AudioInputListEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&types;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const ArrayOfEnum TYPES =
      ArrayOfEnum(ParameterNames::TYPES, AUDIO_INPUT_TYPE_NAMES, AUDIO_INPUT_TYPE_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &TYPES,
    };

    AudioInputType _types[DataSize::AUDIO_INPUT_MAX];
  };
}