#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // [NRD]
  // Acknowledge all setting were read from EEPROM.
  class SettingsReadEvent : public Message
  {
  public:
    SettingsReadEvent() {}
    
    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsReadEvent; }
  };
}