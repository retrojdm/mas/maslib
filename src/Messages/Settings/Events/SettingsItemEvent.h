#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/String.h"

namespace mas
{
  // [NSE] <key> <value>
  // A setting value.
  class SettingsItemEvent : public Message
  {
  public:
    char key[DataSize::SETTING_KEY_SIZE];
    char value[DataSize::SETTING_VALUE_SIZE];

    SettingsItemEvent()
    {
      key[0] = '\0';
      value[0] = '\0';
    }

    SettingsItemEvent(const char* const key, const char* const value)
    {
      strlcpy(this->key, key, DataSize::SETTING_KEY_SIZE);
      strlcpy(this->value, value, DataSize::SETTING_VALUE_SIZE);
    }

    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsItemEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)key;
        case 1: return (void*)value;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const String KEY = String(
      ParameterNames::KEY, 0, DataSize::SETTING_KEY_SIZE - 1);

    static constexpr const String VALUE = String(
      ParameterNames::VALUE, 0, DataSize::SETTING_VALUE_SIZE - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &KEY,
      &VALUE,
    };
  };
}