#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // [NSC]
  // Indicates that the settings listing is complete.
  class SettingsListCompleteEvent : public Message
  {
  public:

    SettingsListCompleteEvent() {}

    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsListCompleteEvent; }
  };
}