#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // [NRD]
  // Acknowledge all setting were written to EEPROM.
  class SettingsWriteEvent : public Message
  {
  public:
    SettingsWriteEvent() {}
    
    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsWriteEvent; }
  };
}