#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // NWR
  // Write from EEPROM.
  class SettingsWriteCommand : public Message
  {
  public:
    SettingsWriteCommand() {}
    
    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsWriteCommand; }
  };
}
