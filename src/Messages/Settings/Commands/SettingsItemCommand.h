#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/OptionalString.h"

namespace mas
{
  // NSE <key?> <value?>
  // Get all settings for the front panel, or get or set a setting.
  // Data is read/written from SRAM. You'll have to use `NRD` or `NWR` to read/write EEPROM.
  class SettingsItemCommand : public Message
  {
  public:
    Optional<char*> key;
    Optional<char*> value;

    SettingsItemCommand()
    : key(_key, sizeof(_key)), value(_value, sizeof(_value)) {}

    SettingsItemCommand(const char* const key)
    : key(_key, sizeof(_key), key), value(_value, sizeof(_value)) {}

    SettingsItemCommand(const char* const key, const char* const value)
    : key(_key, sizeof(_key), key), value(_value, sizeof(_value), value) {}

    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsItemCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&key;
        case 1: return (void*)&value;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const OptionalString KEY = OptionalString(
      ParameterNames::KEY, 0, DataSize::SETTING_KEY_SIZE - 1);

    static constexpr const OptionalString VALUE = OptionalString(
      ParameterNames::VALUE, 0, DataSize::SETTING_VALUE_SIZE - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &KEY,
      &VALUE,
    };

    char _key[DataSize::SETTING_KEY_SIZE];
    char _value[DataSize::SETTING_VALUE_SIZE];
  };
}