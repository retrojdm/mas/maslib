#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // NRD
  // Read from EEPROM.
  class SettingsReadCommand : public Message
  {
  public:
    SettingsReadCommand() {}
    
    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsReadCommand; }
  };
}
