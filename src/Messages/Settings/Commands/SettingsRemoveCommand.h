#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/String.h"

namespace mas
{
  // NSX <key>
  // Remove a setting.
  // Data is read/written from SRAM. You'll have to use `NRD` or `NWR` to read/write EEPROM.
  class SettingsRemoveCommand : public Message
  {
  public:
    char key[DataSize::SETTING_KEY_SIZE];

    SettingsRemoveCommand()
    {
      key[0] = '\0';
    }

    SettingsRemoveCommand(const char* const key)
    {
      strlcpy(this->key, key, DataSize::SETTING_KEY_SIZE);
    }

    char topic() const override { return Module::SETTINGS_KEY; }
    MessageType type() const override { return MessageType::SettingsRemoveCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)key;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const String KEY = String(
      ParameterNames::KEY, 0, DataSize::SETTING_KEY_SIZE - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &KEY,
    };
  };
}