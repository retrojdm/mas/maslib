#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/AnalyserMode.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // UVM <mode>
  // UI Visualisation analyser mode.
  class UiVisModeEvent : public Message
  {
  public:
    AnalyserMode mode;

    UiVisModeEvent() : mode() {}
    UiVisModeEvent(const AnalyserMode mode) : mode(mode) {}

    char topic() const override { return Module::UI_KEY; }
    MessageType type() const override { return MessageType::UiVisModeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&mode;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Enum MODE = Enum(
      ParameterNames::MODE, ANALYSER_MODE_NAMES, ANALYSER_MODE_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &MODE,
    };
  };
}