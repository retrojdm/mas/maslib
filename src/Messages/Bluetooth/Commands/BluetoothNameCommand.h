#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/DataSize.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Models/Optional.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/OptionalString.h"

namespace mas
{
  // TNA <slot> <name?>
  // Get or set name of this bluetooth device.
  class BluetoothNameCommand : public Message
  {
  public:
    uint8_t slot;
    Optional<char const *> name;

    BluetoothNameCommand() : slot(), name(_name, sizeof(_name)) {}
    BluetoothNameCommand(const uint8_t slot) : slot(slot), name(_name, sizeof(_name)) {}
    BluetoothNameCommand(const uint8_t slot, const char* const name) : slot(slot), name(_name, sizeof(_name), name) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothNameCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&name;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const OptionalString NAME = OptionalString(
      ParameterNames::NAME, 0, DataSize::MESSAGE_PARAMETERS_SIZE - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &NAME,
    };

    char _name[DataSize::MESSAGE_PARAMETERS_SIZE];
  };
}