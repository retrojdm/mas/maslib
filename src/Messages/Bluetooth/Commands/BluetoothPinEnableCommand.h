#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Enums/OffOn.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/OptionalEnum.h"

namespace mas
{
  // TPE <slot> <state?>
  // Get or set PIN enabled (required).
  class BluetoothPinEnableCommand : public Message
  {
  public:
    uint8_t slot;
    Optional<bool> state;

    BluetoothPinEnableCommand() : slot(), state() {}
    BluetoothPinEnableCommand(const uint8_t slot) : slot(slot), state() {}
    BluetoothPinEnableCommand(const uint8_t slot, const bool state) : slot(slot), state(state) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothPinEnableCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&state;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const OptionalEnum STATE = OptionalEnum(
      ParameterNames::STATE, OFF_ON_NAMES, OFF_ON_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &STATE,
    };
  };
}