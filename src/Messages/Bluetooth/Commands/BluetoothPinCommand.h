#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/OptionalString.h"

namespace mas
{
  // TPN <slot> <pin?>
  // Get or set pairing PIN.
  class BluetoothPinCommand : public Message
  {
  public:
    uint8_t slot;
    Optional<char*> pin;

    BluetoothPinCommand() : slot(), pin(_pin, sizeof(_pin)) {}
    BluetoothPinCommand(const uint8_t slot) : slot(slot), pin(_pin, sizeof(_pin)) {}
    BluetoothPinCommand(const uint8_t slot, const char* const pin): slot(slot), pin(_pin, sizeof(_pin), pin) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothPinCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&pin;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const OptionalString PIN = OptionalString(
      ParameterNames::PIN, 0, DataSize::MESSAGE_PARAMETERS_SIZE - 1);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &PIN,
    };

    char _pin[DataSize::MESSAGE_PARAMETERS_SIZE];
  };
}