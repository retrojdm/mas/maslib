#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // TSL
  // Get the current bluetooth card's slot index.
  class BluetoothSlotCommand : public Message
  {
  public:
    BluetoothSlotCommand() {}

    char topic() const override { return Module::POWER_KEY; }
    MessageType type() const override { return MessageType::BluetoothSlotCommand; }
  };
}