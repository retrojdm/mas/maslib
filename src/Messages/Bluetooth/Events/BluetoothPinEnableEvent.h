#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // [TPE] <slot> <state>
  // PIN enabled/required.
  class BluetoothPinEnableEvent : public Message
  {
  public:
    uint8_t slot;
    bool state;

    BluetoothPinEnableEvent() : slot(), state() {}
    BluetoothPinEnableEvent(const uint8_t slot, const bool state) : slot(slot), state(state) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothPinEnableEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&state;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const Enum STATE = Enum(
      ParameterNames::STATE, OFF_ON_NAMES, OFF_ON_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &STATE,
    };
  };
}