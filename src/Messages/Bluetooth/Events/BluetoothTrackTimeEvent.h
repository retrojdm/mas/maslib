#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [TTM] <slot> <time> <length>
  // AVRCP Track time and length in seconds.
  class BluetoothTrackTimeEvent : public Message
  {
  public:
    uint8_t slot;
    uint16_t time;
    uint16_t length;

    BluetoothTrackTimeEvent() : slot(), time(), length() {}

    BluetoothTrackTimeEvent(const uint8_t slot, const uint16_t time, const uint16_t length) :
      slot(slot), time(time), length(length) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothTrackTimeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&time;
        case 2: return (void*)&length;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 3;
    static const uint8_t MAX_PARAMETERS = 3;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const Integer<uint16_t> TIME = Integer<uint16_t>(
      ParameterNames::TIME, 0, 65535, IntegerFormats::U);

    static constexpr const Integer<uint16_t> LENGTH = Integer<uint16_t>(
      ParameterNames::LENGTH, 0, 65535, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &TIME,
      &LENGTH,
    };
  };
}