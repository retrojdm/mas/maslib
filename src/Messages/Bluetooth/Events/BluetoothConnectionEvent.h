#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/ConnectionStatus.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // [TCN] <slot> <status>
  // Connection status.
  class BluetoothConnectionEvent : public Message
  {
  public:
    uint8_t slot;
    ConnectionStatus status;

    BluetoothConnectionEvent() : slot(), status() {}
    BluetoothConnectionEvent(const uint8_t slot, const ConnectionStatus status) : slot(slot), status(status) {}

    char topic() const override { return Module::BLUETOOTH_KEY; }
    MessageType type() const override { return MessageType::BluetoothConnectionEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&slot;
        case 1: return (void*)&status;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> SLOT = Integer<uint8_t>(
      ParameterNames::SLOT, 0, DataSize::CARD_SLOTS - 1, IntegerFormats::U);

    static constexpr const Enum STATUS = Enum(
      ParameterNames::STATUS, CONNECTION_STATUS_NAMES, CONNECTION_STATUS_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &SLOT,
      &STATUS,
    };
  };
}