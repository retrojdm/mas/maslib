#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalEnum.h"

namespace mas
{
  // PBT <state?>
  // Get or set battery relay output.
  class PowerBatteryCommand : public Message
  {
  public:
    Optional<bool> state;

    PowerBatteryCommand() : state() {}
    PowerBatteryCommand(const bool state) : state() {}

    char topic() const override { return Module::POWER_KEY; }
    MessageType type() const override { return MessageType::PowerBatteryCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&state;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalEnum STATE = OptionalEnum(
      ParameterNames::STATE, OFF_ON_NAMES, OFF_ON_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &STATE,
    };
  };
}