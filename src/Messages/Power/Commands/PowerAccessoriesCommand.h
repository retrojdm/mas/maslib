#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Models/Message.h"

namespace mas
{
  // PAC
  // Get accessories optocoupler input.
  class PowerAccessoriesCommand : public Message
  {
  public:
    PowerAccessoriesCommand() {}

    char topic() const override { return Module::POWER_KEY; }
    MessageType type() const override { return MessageType::PowerAccessoriesCommand; }
  };
}