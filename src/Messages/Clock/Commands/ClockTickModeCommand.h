#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/ClockTickMode.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalEnum.h"

namespace mas
{
  // CTK <mode?>
  // <mode> can be: `off`, `min`, `sec`.
  class ClockTickModeCommand : public Message
  {
  public:
    Optional<ClockTickMode> mode;

    ClockTickModeCommand() : mode() {}
    ClockTickModeCommand(const ClockTickMode mode) : mode(mode) {}

    char topic() const override { return Module::CLOCK_KEY; }
    MessageType type() const override { return MessageType::ClockTickModeCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&mode;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalEnum MODE = OptionalEnum(
      ParameterNames::MODE, CLOCK_TICK_MODE_NAMES, CLOCK_TICK_MODE_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &MODE,
    };
  };
}