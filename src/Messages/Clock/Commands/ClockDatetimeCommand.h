#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalDatetime.h"

namespace mas
{
  // CDT <datetime?>
  // Get or set date/time.
  class ClockDatetimeCommand : public Message
  {
  public:
    Optional<time_t> datetime;

    ClockDatetimeCommand() : datetime() {}
    ClockDatetimeCommand(const time_t datetime) : datetime(datetime) {}

    char topic() const override { return Module::CLOCK_KEY; }
    MessageType type() const override { return MessageType::ClockDatetimeCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&datetime;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalDatetime DATETIME = OptionalDatetime(
      ParameterNames::DATETIME);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &DATETIME,
    };
  };
}