#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Enums/ClockTickMode.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Enum.h"

namespace mas
{
  // [CTK] <mode>
  // Clock tick event status
  class ClockTickModeEvent : public Message
  {
  public:
    ClockTickMode mode;

    ClockTickModeEvent() : mode() {}
    ClockTickModeEvent(const ClockTickMode mode) : mode(mode) {}

    char topic() const override { return Module::CLOCK_KEY; }
    MessageType type() const override { return MessageType::ClockTickModeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&mode;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Enum MODE = Enum(
      ParameterNames::MODE, CLOCK_TICK_MODE_NAMES, CLOCK_TICK_MODE_COUNT);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &MODE,
    };
  };
}