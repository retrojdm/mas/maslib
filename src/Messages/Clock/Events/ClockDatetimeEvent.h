#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Datetime.h"

namespace mas
{
  // [CDT] <datetime>
  // Date/time.
  class ClockDatetimeEvent : public Message
  {
  public:
    time_t datetime;

    ClockDatetimeEvent() : datetime() {}
    ClockDatetimeEvent(const time_t datetime) : datetime(datetime) {}

    char topic() const override { return Module::CLOCK_KEY; }
    MessageType type() const override { return MessageType::ClockDatetimeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&datetime;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Datetime DATETIME = Datetime(
      ParameterNames::DATETIME);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &DATETIME,
    };
  };
}