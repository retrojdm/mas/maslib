#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Relative.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalRelativeInteger.h"

namespace mas
{
  // OVL <volume?>
  // Get or set volume (0.255). Prefix value with '+' or '-' to make relative.
  class AudioOutputVolumeCommand : public Message
  {
  public:
    static const uint8_t VOLUME_MAX = 255;

    Optional<Relative<int16_t>> volume;

    AudioOutputVolumeCommand() : volume() {}
    AudioOutputVolumeCommand(const Relative<int16_t> volume) : volume(volume) {}

    char topic() const override { return Module::AUDIO_OUTPUT_KEY; }
    MessageType type() const override { return MessageType::AudioOutputVolumeCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&volume;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalRelativeInteger<int16_t> VOLUME = OptionalRelativeInteger<int16_t>(
      ParameterNames::VOLUME,
      -AudioOutputVolumeCommand::VOLUME_MAX,
      AudioOutputVolumeCommand::VOLUME_MAX,
      IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &VOLUME,
    };
  };
}