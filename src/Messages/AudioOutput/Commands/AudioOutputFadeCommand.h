#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Optional.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/OptionalInteger.h"

namespace mas
{
  // OFA <fade?>
  // Get or set front-rear fade (-127..127).
  class AudioOutputFadeCommand : public Message
  {
  public:
    static const int8_t FADE_MAX = 127;

    Optional<int8_t> fade;

    AudioOutputFadeCommand() : fade() {}
    AudioOutputFadeCommand(const int8_t fade) : fade(fade) {}

    char topic() const override { return Module::AUDIO_OUTPUT_KEY; }
    MessageType type() const override { return MessageType::AudioOutputFadeCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&fade;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const OptionalInteger<int8_t> FADE = OptionalInteger<int8_t>(
      ParameterNames::FADE,
      -AudioOutputFadeCommand::FADE_MAX,
      AudioOutputFadeCommand::FADE_MAX,
      IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &FADE,
    };
  };
}