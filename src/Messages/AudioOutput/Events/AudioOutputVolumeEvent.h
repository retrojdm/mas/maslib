#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [OVL] <volume>
  // Volume.
  class AudioOutputVolumeEvent : public Message
  {
  public:
    static const uint8_t VOLUME_MAX = 255;

    uint8_t volume;

    AudioOutputVolumeEvent() : volume() {}
    AudioOutputVolumeEvent(const uint8_t volume) : volume(volume) {}

    char topic() const override { return Module::AUDIO_OUTPUT_KEY; }
    MessageType type() const override { return MessageType::AudioOutputVolumeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&volume;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Integer<uint8_t> VOLUME = Integer<uint8_t>(
      ParameterNames::VOLUME, 0, VOLUME_MAX, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &VOLUME,
    };
  };
}