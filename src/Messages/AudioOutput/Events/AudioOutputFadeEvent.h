#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [OBA] <fade>
  // Front-rear fade.
  class AudioOutputFadeEvent : public Message
  {
  public:
    static const int8_t FADE_MAX = 127;

    uint8_t fade;

    AudioOutputFadeEvent() : fade() {}
    AudioOutputFadeEvent(const int8_t fade) : fade(fade) {}

    char topic() const override { return Module::AUDIO_OUTPUT_KEY; }
    MessageType type() const override { return MessageType::AudioOutputFadeEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&fade;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 1;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const Integer<int8_t> FADE = Integer<int8_t>(
      ParameterNames::FADE, -FADE_MAX, FADE_MAX, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &FADE,
    };

  };
}