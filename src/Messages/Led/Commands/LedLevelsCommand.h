#pragma once

#include "../../../Platform.h"
#include "../../../Constants/DataSize.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Enums/OffOn.h"
#include "../../../Models/Array.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/ArrayOfInteger.h"

namespace mas
{
  // LED <levels[]>
  // Get or set comma-separated list of LED levels.
  class LedLevelsCommand : public Message
  {
  public:
    static const uint8_t LED_COUNT_MAX = 6;

    Array<uint8_t> levels;

    LedLevelsCommand() : levels(_levels, LED_COUNT_MAX) {}
    LedLevelsCommand(const Array<uint8_t> levels) : levels(_levels, levels) {}

    char topic() const override { return Module::LED_KEY; }
    MessageType type() const override { return MessageType::LedLevelsCommand; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&levels;
        default: return nullptr;
      }
    }

  private:
    static const uint8_t MIN_PARAMETERS = 0;
    static const uint8_t MAX_PARAMETERS = 1;

    static constexpr const ArrayOfInteger<uint8_t> LEVELS =
      ArrayOfInteger<uint8_t>(ParameterNames::LEVELS, 0, 255, IntegerFormats::U);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &LEVELS,
    };

    uint8_t _levels[LED_COUNT_MAX];
  };
}