#pragma once

#include "../../../Platform.h"
#include "../../../Constants/Module.h"
#include "../../../Constants/ParameterNames.h"
#include "../../../Constants/IntegerFormats.h"
#include "../../../Models/Message.h"
#include "../../../Models/Parameter.h"
#include "../../../Parameters/Integer.h"

namespace mas
{
  // [ENC] <index> <steps>
  // Encoder changed. +/- indicated direction.
  class EncoderChangedEvent : public Message
  {
  public:
    uint8_t index;
    int8_t steps;

    EncoderChangedEvent() : index(), steps() {}
    EncoderChangedEvent(const uint8_t index, const int8_t steps) : index(index), steps(steps) {}

    char topic() const override { return Module::ENCODER_KEY; }
    MessageType type() const override { return MessageType::EncoderChangedEvent; }
    uint8_t minParameters() const override { return MIN_PARAMETERS; }
    uint8_t maxParameters() const override { return MAX_PARAMETERS; }
    const Parameter * const * parameters() const override { return PARAMETERS; }

    void* memberPtr(const uint8_t i) const override
    {
      switch (i)
      {
        case 0: return (void*)&index;
        case 1: return (void*)&steps;
        default: return nullptr;
      }
    }
  
  private:
    static const uint8_t MIN_PARAMETERS = 2;
    static const uint8_t MAX_PARAMETERS = 2;

    static constexpr const Integer<uint8_t> INDEX = Integer<uint8_t>(
      ParameterNames::INDEX, 0, 255, IntegerFormats::U);

    static constexpr const Integer<int8_t> STEPS = Integer<int8_t>(
      ParameterNames::STEPS, -128, 127, IntegerFormats::D);

    static constexpr const Parameter * const PARAMETERS[MAX_PARAMETERS] =
    {
      &INDEX,
      &STEPS,
    };
  };
}