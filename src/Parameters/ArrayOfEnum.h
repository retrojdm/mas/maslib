#pragma once

#include "../Platform.h"
#include "../Constants/Encoding.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Array.h"
#include "../Models/Parameter.h"

namespace mas
{
  class ArrayOfEnum : public Parameter
  {
  public:
    constexpr ArrayOfEnum(
      const char* const name_P,
      const char* const * enumNames_P,
      const size_t enumCount)
      :
      Parameter(name_P),
      enumNames_P(enumNames_P),
      enumCount(enumCount)
    {
    }

    const char* const * enumNames_P;
    const size_t enumCount;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Array<uint8_t>* pArray = reinterpret_cast<Array<uint8_t>*>(pMember);
      pArray->setLength(0);

      char buffer[DataSize::MESSAGE_PARAMETERS_SIZE];
      strlcpy(buffer, input, DataSize::MESSAGE_PARAMETERS_SIZE);

      char delimiters[2] = { Encoding::ARRAY_SEPARATOR, '\0' };
      char* token = strtok(buffer, delimiters);
      while (token != NULL && pArray->length() < pArray->size())
      {
        // Do not use with `MessageType`, which has an underlying type of `uint16_t`.
        uint8_t value;
        if (!EnumHelper::tryParseEnum(token, name_P, enumNames_P, enumCount, value, outError))
        {
          return false; 
        }

        pArray->add(value);
        token = strtok(NULL, delimiters);
      }

      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Array<uint8_t>* pArray = reinterpret_cast<Array<uint8_t>*>(pMember);
      output[0] = '\0';
    
      if (pArray->length() == 0)
      {
        return false; 
      }

      char separator[2] = { Encoding::ARRAY_SEPARATOR, '\0' };

      for (uint8_t i = 0; i < pArray->length(); i++)
      {
        strcat_P(output, enumNames_P[static_cast<uint8_t>((*pArray)[i])]);
        if (i < pArray->length() - 1)
        {
          strcat(output, separator);
        }
      }

      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::ARRAY, name_P);
    }
  };
}