#pragma once

#include "../Platform.h"
#include "../Constants/Encoding.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/ValidationHelper.h"
#include "../Models/Array.h"
#include "../Models/Parameter.h"

namespace mas
{
  template <typename T>
  class ArrayOfInteger : public Parameter
  {
  public:
    constexpr ArrayOfInteger(
      const char* const name_P,
      const T minValue,
      const T maxValue,
      const char* const format_P)
      : Parameter(name_P),
      minValue(minValue),
      maxValue(maxValue),
      format_P(format_P)
    {
    }

    const T minValue;
    const T maxValue;
    const char* const format_P;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Array<T>* pArray = reinterpret_cast<Array<T>*>(pMember);
      return ValidationHelper::tryParseArrayOfIntegers(
        input, name_P, minValue, maxValue, format_P, pArray, outError);
    }

    bool toString(void* pMember, char* const output) const
    {
      Array<T>* pArray = reinterpret_cast<Array<T>*>(pMember);
      output[0] = '\0';

      if (pArray->length() == 0)
      {
        return false; 
      }

      char separator[2] = { Encoding::ARRAY_SEPARATOR, '\0' };
      char token[12]; // Can accomodate an int32_t. i.e.: "-4294967296" plus null terminator.

      for (uint8_t i = 0; i < pArray->length(); i++)
      {
        sprintf_P(token, format_P, (*pArray)[i]);
        strcat(output, token);
        if (i < pArray->length() - 1)
        {
          strcat(output, separator);
        }
      }

      return true;
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::ARRAY, name_P);
    }
  };
}