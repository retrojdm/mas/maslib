#pragma once

#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/EnumHelper.h"
#include "../Models/Parameter.h"
#include "../Models/Optional.h"

namespace mas
{
  class Enum : public Parameter
  {
  public:
    constexpr Enum(
      const char* const name_P,
      const char* const * enumNames_P,
      const size_t enumCount)
      : Parameter(name_P),
      enumNames_P(enumNames_P),
      enumCount(enumCount)
    {
    }

    const char* const * enumNames_P;
    const size_t enumCount;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      // Do not use with `MessageType`, which has an underlying type of `uint16_t`.
      uint8_t* ptr = reinterpret_cast<uint8_t*>(pMember);
      uint8_t value;
      if (!EnumHelper::tryParseEnum(input, name_P, enumNames_P, enumCount, value, outError))
      {
        *ptr = 0;
        return false;
      }

      *ptr = value;
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      uint8_t* ptr = reinterpret_cast<uint8_t*>(pMember);
      uint8_t index = static_cast<uint8_t>(*ptr);
      if (index >= enumCount)
      {
        return false;
      }

      strlcpy_P(output, enumNames_P[index], DataSize::MESSAGE_PARAMETERS_SIZE);
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}