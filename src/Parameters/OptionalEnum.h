#pragma once

#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/EnumHelper.h"
#include "../Models/Parameter.h"
#include "../Models/Optional.h"

namespace mas
{
  // The underlying type of all of our enums is assumed to be uint8_t.
  class OptionalEnum : public Parameter
  {
  public:
    constexpr OptionalEnum(
      const char* const name_P,
      const char* const * enumNames,
      const size_t enumCount)
      : Parameter(name_P),
      enumNames(enumNames),
      enumCount(enumCount)
    {
    }

    const char* const * enumNames;
    const size_t enumCount;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Optional<uint8_t>* ptr = reinterpret_cast<Optional<uint8_t>*>(pMember);

      uint8_t value;
      if (!EnumHelper::tryParseEnum(input, name_P, enumNames, enumCount, value, outError))
      {
        ptr->reset();
        return false; 
      }

      ptr->set(value);
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Optional<uint8_t>* ptr = reinterpret_cast<Optional<uint8_t>*>(pMember);

      if (!ptr->hasValue())
      {
        output[0] = '\0';
        return false;
      }

      uint8_t index = static_cast<uint8_t>(ptr->value());
      if (index >= enumCount)
      {
        return false;
      }

      strlcpy_P(output, enumNames[index], DataSize::MESSAGE_PARAMETERS_SIZE);
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::OPTIONAL, name_P);
    }
  };
}