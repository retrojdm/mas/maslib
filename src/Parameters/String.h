#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Parameter.h"

namespace mas
{
  class String : public Parameter
  {
  public:
    constexpr String(
      const char* const name_P,
      const uint8_t minLength,
      const uint8_t maxLength,
      const char* const emptyValuePlaceholder = nullptr)
      : Parameter(name_P),
      minLength(minLength),
      maxLength(maxLength),
      emptyValuePlaceholder(emptyValuePlaceholder)
    {
    }

    const uint8_t minLength;
    const uint8_t maxLength;
    const char* const emptyValuePlaceholder;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      char* ptr = reinterpret_cast<char*>(pMember);

      uint8_t len = strlen(input);
      if (len < minLength || len > maxLength)
      {
        sprintf_P(
          outError,
          minLength == maxLength ? Localisation::ERROR_STRING_LENGTH_EXACT : Localisation::ERROR_STRING_LENGTH_RANGE,
          minLength,
          maxLength);

        return false;
      }

      if (emptyValuePlaceholder && strcmp(input, emptyValuePlaceholder) == 0)
      {
        ptr[0] = '\0';
      }
      else
      {
        strlcpy(ptr, input, maxLength + 1);
      }

      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      char* ptr = reinterpret_cast<char*>(pMember);

      if (emptyValuePlaceholder && ptr[0] == '\0')
      {
        strcpy(output, emptyValuePlaceholder);
      }
      else
      {
        strcpy(output, ptr);
      }

      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}