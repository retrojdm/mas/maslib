#pragma once

#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/ValidationHelper.h"
#include "../Models/Parameter.h"

namespace mas
{
  template<typename T>
  class Integer : public Parameter
  {
  public:
    constexpr Integer(
      const char* const name_P,
      const T minValue,
      const T maxValue,
      const char* const format_P)
      :
      Parameter(name_P),
      minValue(minValue),
      maxValue(maxValue),
      format_P(format_P)
    {
    }

    const T minValue;
    const T maxValue;
    const char* const format_P;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      T* ptr = reinterpret_cast<T*>(pMember);
      T value;
      if (!ValidationHelper::tryParseInteger(input, name_P, minValue, maxValue, format_P, value, outError))
      {
        *ptr = 0;
        return false;
      }

      *ptr = value;
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      T* ptr = reinterpret_cast<T*>(pMember);
      sprintf_P(output, format_P, *ptr);
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}