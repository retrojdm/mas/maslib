#pragma once

#include "../Platform.h"
#include "../Constants/Encoding.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Array.h"
#include "../Models/Parameter.h"

namespace mas
{
  class Base64 : public Parameter
  {
  public:
    constexpr Base64(
      const char* const name_P,
      const uint8_t minLength,
      const uint8_t maxLength)
    :
    Parameter(name_P),
    minLength(minLength),
    maxLength(maxLength)
    {
    }

    const uint8_t minLength;
    const uint8_t maxLength;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      uint8_t len = strlen(input);
      if (len < minLength || len > maxLength)
      {
        sprintf_P(
          outError,
          minLength == maxLength ? Localisation::ERROR_BASE64_LENGTH_EXACT : Localisation::ERROR_BASE64_LENGTH_RANGE,
          minLength,
          maxLength);

        return false;
      }

      Array<uint8_t>* pArray = reinterpret_cast<Array<uint8_t>*>(pMember);
      pArray->setLength(0);

      char* pIn = const_cast<char*>(input);
      for (uint8_t i = 0; i < len; i++)
      {
        char c = *pIn++;
        uint8_t value = Encoding::ASCII_TO_BASE64_VALUE[static_cast<uint8_t>(c)];
        if (value == Encoding::INVALID_CHAR)
        {
          sprintf_P(outError, Localisation::ERROR_BASE64_CHAR, c);
          return false;
        }

        pArray->add(value);
      }

      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Array<uint8_t>* pArray = reinterpret_cast<Array<uint8_t>*>(pMember);
      output[0] = '\0';

      if (pArray->length() == 0)
      {
        return false; 
      }

      char* pOut = output;
      for (uint8_t i = 0; i < pArray->length(); i++)
      {
        *(pOut++) = Encoding::BASE64_CHARS[(*pArray)[i]];
      }

      *pOut = '\0';
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}