#pragma once

#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/ValidationHelper.h"
#include "../Models/Optional.h"
#include "../Models/Parameter.h"

namespace mas
{
  template<typename T>
  class OptionalInteger : public Parameter
  {
  public:
    constexpr OptionalInteger(
      const char* const name_P,
      const T minValue,
      const T maxValue,
      const char* const format_P)
      :
      Parameter(name_P),
      minValue(minValue),
      maxValue(maxValue),
      format_P(format_P)
    {
    }

    const T minValue;
    const T maxValue;
    const char* const format_P;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Optional<T>* ptr = reinterpret_cast<Optional<T>*>(pMember);

      T value;
      if (!ValidationHelper::tryParseInteger(input, name_P, minValue, maxValue, format_P, value, outError))
      {
        ptr->reset();
        return false; 
      }

      ptr->set(value);
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Optional<T>* ptr = reinterpret_cast<Optional<T>*>(pMember);

      if (!ptr->hasValue())
      {
        output[0] = '\0';
        return false;
      }

      sprintf_P(output, format_P, ptr->value());
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::OPTIONAL, name_P);
    }
  };
}