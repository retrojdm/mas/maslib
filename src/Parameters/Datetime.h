#pragma once

#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/DatetimeHelper.h"
#include "../Models/Parameter.h"

namespace mas
{
  class Datetime : public Parameter
  {
  public:
    constexpr Datetime(
      const char* const name_P)
      : Parameter(name_P)
    {
    }

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      time_t* ptr = reinterpret_cast<time_t*>(pMember);

      time_t value;
      if (!DatetimeHelper::tryParseIso(input, value, outError))
      {
        strlcpy_P(outError, Localisation::ERROR_INVALID_DATE_FORMAT, DataSize::MESSAGE_PARAMETERS_SIZE);
        *ptr = 0;
        return false; 
      }

      *ptr = value;
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      time_t* ptr = reinterpret_cast<time_t*>(pMember);
      DatetimeHelper::toIso(*ptr, output);
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}