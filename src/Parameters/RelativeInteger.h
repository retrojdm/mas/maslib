#pragma once

#include "../Platform.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Parameter.h"
#include "../Models/Relative.h"

namespace mas
{
  template<typename T>
  class RelativeInteger : public Parameter
  {
  public:
    constexpr RelativeInteger(
      const char* const name_P,
      const T minValue,
      const T maxValue,
      const char* const format_P)
      :
      Parameter(name_P),
      minValue(minValue),
      maxValue(maxValue),
      format_P(format_P)
    {
    }

    const T minValue;
    const T maxValue;
    const char* const format_P;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Relative<T>* ptr = reinterpret_cast<Relative<T>*>(pMember);

      ptr->set(0, false);

      // We're not using `ValidationHelper::tryParseIntegerRelative` here, because we don't actually want to apply the
      // relative value. We're simply describing it.
      // To apply the wrapped value, use `Relative::valueRelativeTo(...)`.
      T value;
      if (!ValidationHelper::tryParseInteger(input, name_P, minValue, maxValue, format_P, value, outError))
      {
        return false;
      }

      // We assume all values prefixed with a '-' or '+' sign are relative.
      // Note: This means that all negative values are treated as relative.
      ptr->set(value, input[0] == '-' || input[0] == '+');
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Relative<T>* ptr = reinterpret_cast<Relative<T>*>(pMember);
      
      if (ptr->relative())
      {
        char relativeFormat[5]; // Can accomodate "+%ld" plus null terminator.
        char* pOut = relativeFormat;
        if (ptr->value() > 0)
        {
          *(pOut++) = '+';
        }

        *pOut = '\0';

        strcat(relativeFormat, format_P);
        sprintf(output, relativeFormat, ptr->value());
      }
      else
      {
        sprintf_P(output, format_P, ptr->value());
      }

      return true;
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}