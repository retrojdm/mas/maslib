#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Parameter.h"
#include "../Models/Optional.h"

namespace mas
{
  class OptionalChar : public Parameter
  {
  public:
    constexpr OptionalChar(const char* const name_P)
    : Parameter(name_P)
    {
    }

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Optional<char>* ptr = reinterpret_cast<Optional<char>*>(pMember);

      if (strlen(input) != 1)
      {
        strlcpy_P(outError, Localisation::ERROR_EXPECTED_SINGLE_CHARACTER, DataSize::MESSAGE_PARAMETERS_SIZE);
        ptr->reset();
        return false; 
      }

      ptr->set(input[0]);
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Optional<char>* ptr = reinterpret_cast<Optional<char>*>(pMember);
      
      if (!ptr->hasValue())
      {
        output[0] = '\0';
        return false;
      }

      output[0] = ptr->value();
      output[1] = '\0';
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::OPTIONAL, name_P);
    }
  };
}