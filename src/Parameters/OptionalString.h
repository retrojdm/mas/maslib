#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Parameter.h"
#include "../Models/Optional.h"

namespace mas
{
  class OptionalString : public Parameter
  {
  public:
    constexpr OptionalString(
      const char* const name_P,
      const uint8_t minLength,
      const uint8_t maxLength,
      const char* const emptyValuePlaceholder = nullptr)
      : Parameter(name_P),
      minLength(minLength),
      maxLength(maxLength),
      emptyValuePlaceholder(emptyValuePlaceholder)
    {
    }

    const uint8_t minLength;
    const uint8_t maxLength;
    const char* const emptyValuePlaceholder;

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Optional<char*>* ptr = reinterpret_cast<Optional<char*>*>(pMember);

      uint8_t len = strlen(input);
      if (len < minLength || len > maxLength)
      {
        sprintf_P(
          outError,
          minLength == maxLength ? Localisation::ERROR_STRING_LENGTH_EXACT : Localisation::ERROR_STRING_LENGTH_RANGE,
          minLength,
          maxLength);

        return false;
      }

      if (emptyValuePlaceholder && strcmp(input, emptyValuePlaceholder) == 0)
      {
        ptr->set("");
      }
      else
      {
        ptr->set(input);
      }

      
      ptr->set(input);
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Optional<char*>* ptr = reinterpret_cast<Optional<char*>*>(pMember);
      if (!ptr->hasValue() || ptr->value() == nullptr)
      {
        return false;
      }

      if (emptyValuePlaceholder && ptr->value()[0] == '\0')
      {
        strcpy(output, emptyValuePlaceholder);
      }
      else
      {
        strcpy(output, ptr->value());
      }

      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::OPTIONAL, name_P);
    }
  };
}