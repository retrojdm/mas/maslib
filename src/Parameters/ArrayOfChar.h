#pragma once

#include "../Platform.h"
#include "../Constants/Encoding.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Array.h"
#include "../Models/Parameter.h"

namespace mas
{
  class ArrayOfChar : public Parameter
  {
  public:
    constexpr ArrayOfChar(const char* const name_P)
    : Parameter(name_P)
    {
    }

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Array<char>* pArray = reinterpret_cast<Array<char>*>(pMember);
      pArray->setLength(0);

      char buffer[DataSize::MESSAGE_PARAMETERS_SIZE];
      strlcpy(buffer, input, DataSize::MESSAGE_PARAMETERS_SIZE);

      char delimiters[2] = { Encoding::ARRAY_SEPARATOR, '\0' };
      char* token = strtok(buffer, delimiters);
      while (token != NULL && pArray->length() < pArray->size())
      {
        pArray->add(token[0]);
        token = strtok(NULL, delimiters);
      }

      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Array<char>* pArray = reinterpret_cast<Array<char>*>(pMember);
      output[0] = '\0';

      if (pArray->length() == 0)
      {
        return false; 
      }

      char* pOut = output;
      for (uint8_t i = 0; i < pArray->length(); i++)
      {
        *(pOut++) = (*pArray)[i];
        if (i < pArray->length() - 1)
        {
          *(pOut++) = Encoding::ARRAY_SEPARATOR;
        }
      }

      *pOut = '\0';
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::ARRAY, name_P);
    }
  };
}