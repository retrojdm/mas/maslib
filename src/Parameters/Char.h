#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Models/Parameter.h"

namespace mas
{
  class Char : public Parameter
  {
  public:
    constexpr Char(const char* const name_P)
    : Parameter(name_P)
    {
    }

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      char* ptr = reinterpret_cast<char*>(pMember);

      if (strlen(input) != 1)
      {
        strlcpy_P(outError, Localisation::ERROR_EXPECTED_SINGLE_CHARACTER, DataSize::MESSAGE_PARAMETERS_SIZE);
        *ptr = '\0';
        return false; 
      }

      *ptr = input[0];
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      char* ptr = reinterpret_cast<char*>(pMember);
      
      output[0] = *ptr;
      output[1] = '\0';
      return true; 
    }
    
    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::REQUIRED, name_P);
    }
  };
}