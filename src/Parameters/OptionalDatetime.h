#pragma once

#include "../Constants/Localisation.h"
#include "../Constants/ParameterUsageFormats.h"
#include "../Helpers/DatetimeHelper.h"
#include "../Models/Optional.h"
#include "../Models/Parameter.h"

namespace mas
{
  class OptionalDatetime : public Parameter
  {
  public:
    constexpr OptionalDatetime(
      const char* const name_P)
      : Parameter(name_P)
    {
    }

    bool tryParse(void* pMember, const char* const input, char* const outError) const
    {
      Optional<time_t>* ptr = reinterpret_cast<Optional<time_t>*>(pMember);

      time_t t;
      if (!DatetimeHelper::tryParseIso(input, t, outError))
      {
        strlcpy_P(outError, Localisation::ERROR_INVALID_DATE_FORMAT, DataSize::MESSAGE_PARAMETERS_SIZE);
        ptr->reset();
        return false; 
      }

      ptr->set(t);
      return true;
    }

    bool toString(void* pMember, char* const output) const
    {
      Optional<time_t>* ptr = reinterpret_cast<Optional<time_t>*>(pMember);
      
      if (!ptr->hasValue())
      {
        output[0] = '\0';
        return false;
      }

      DatetimeHelper::toIso(ptr->value(), output);
      return true; 
    }

    void usage(char * const output) const
    {
      sprintf_P(output, ParameterUsageFormats::OPTIONAL, name_P);
    }
  };
}