#pragma once

#if defined(ARDUINO)
// Used when compiled via the Arduino IDE
#include <Arduino.h>
#else
// Used when compiled by clang++ for the unit tests.

// PROGMEM etc isn't available when compiling with clang for our unit tests.
#define PROGMEM
#define DMAMEM 
#define PSTR(str) str

// <Arduino.h> includes a lot of standard C libraries, which our code relies on.
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdalign.h>

// PROGMEM versions of string functions. Usually included on the Arduino.
#define strlcpy_P strlcpy
#define sprintf_P sprintf
#define strcmp_P strcmp
#define strcat_P strcat

#define pgm_read_byte_near(address) (*(address))

// strlcpy isn't a standard function.
// We'll roll our own for unit tests.
size_t strlcpy(char *dst, const char *src, size_t dstsize);
#endif