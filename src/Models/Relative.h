#pragma once

#include "../Platform.h"

namespace mas
{
  // The Relative<T> class can be used on integers to mark them as relative.
  template <class T>
  class Relative
  {
  public:
    Relative() : _value{}, _relative(false) {}
    Relative(const T &value) : _value(value), _relative(false) {}
    Relative(const T &value, bool relative) : _value(value), _relative(relative) {}

    const T value() const
    {
      return _value;
    }

    const T valueRelativeTo(const T value) const
    {
      return _relative ? value + _value : _value;
    }

    bool relative() const
    {
      return _relative;
    }

    void set(T value, bool relative)
    {
      _value = value;
      _relative = relative;
    }

    Relative &operator=(const T &value)
    {
      _value = value;
      _relative = false;
      return *this;
    }

  private:
    T _value;
    bool _relative;
  };
}