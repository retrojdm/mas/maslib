#include "SettingDictionary.h"

namespace mas
{
  SettingDictionary::SettingDictionary()
  {
    _count = 0;
  }

  // No bounds checking is performed.
  Setting& SettingDictionary::operator[](int index)
  {
    return _settings[index];
  }

  // No bounds checking is performed.
  char* SettingDictionary::operator[](const char* const key)
  {
    return get(key);
  }
  
  // Clear all settings.
  void SettingDictionary::clear()
  {
    _count = 0;
  }

  // Returns the count.
  uint8_t SettingDictionary::count() const
  {
    return _count;
  }

  // Returns true if the dictionary contains the key.
  bool SettingDictionary::hasKey(const char* const key) const
  {
    if (key == nullptr)
    {
      return false;
    }

    int8_t index = ArrayHelper::findIndex(
      _settings, _count, [key] (Setting x) { return strcmp(x.key, key) == 0; });

    return index != -1;
  }

  // Gets a setting.
  // Returns nullptr if not found.
  char* SettingDictionary::get(const char* const key) const
  {
    if (key == nullptr)
    {
      return nullptr;
    }

    int8_t index = ArrayHelper::findIndex(
      _settings, _count, [key] (Setting x) { return strcmp(x.key, key) == 0; });

    if (index == -1)
    {
      return nullptr;
    }

    return const_cast<char* const>(_settings[index].value);
  }

  // Gets a setting.
  // Returns an empty string if not found.
  bool SettingDictionary::get(const char* const key, char* const outValue) const
  {
    if (key == nullptr)
    {
      return false;
    }

    int8_t index = ArrayHelper::findIndex(
      _settings, _count, [key] (Setting x) { return strcmp(x.key, key) == 0; });

    if (index == -1)
    {
      outValue[0] = '\0';
      return false;
    }

    strlcpy(outValue, _settings[index].value, DataSize::SETTING_VALUE_SIZE);
    return true;
  }

  // Sets a setting.
  // Warning: there's no indication at all when we max out the settings array.
  bool SettingDictionary::set(const char* const key, const char* const value)
  {
    if (key == nullptr || value == nullptr)
    {
      return false;
    }

    int8_t index = ArrayHelper::findIndex(
      _settings, _count, [key] (Setting x) { return strcmp(x.key, key) == 0; });

    if (index == -1)
    {
      if (_count == DataSize::SETTINGS_MAX)
      {
        // Can't add any more.
        return false;
      }

      index = _count;
      // Set unused chars to zero, so that the EEPROM.update() function can avoid changing them if possible.
      memset(_settings[index].key, '\0', DataSize::SETTING_KEY_SIZE);
      strlcpy(_settings[index].key, key, DataSize::SETTING_KEY_SIZE);
      _count++;
    }

    // Set unused chars to zero, so that the EEPROM.update() function can avoid changing them if possible.
    memset(_settings[index].value, '\0', DataSize::SETTING_VALUE_SIZE);
    strlcpy(_settings[index].value, value, DataSize::SETTING_VALUE_SIZE);

    return true;
  }

  // Removes a given setting.
  void SettingDictionary::remove(const char* const key)
  {
    if (key == nullptr)
    {
      return;
    }

    int8_t index = ArrayHelper::findIndex(
      _settings, _count, [key] (Setting x) { return strcmp(x.key, key) == 0; });

    if (index == -1)
    {
      return;
    }

    for (uint8_t i = index; i < _count - 1; i++)
    {
      _settings[i] = _settings[i + 1];
    }

    _count--;
  }
}
