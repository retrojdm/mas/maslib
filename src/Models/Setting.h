#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"

namespace mas
{
  class Setting
  {
  public:
    Setting()
    {
      key[0] = '\0';
      value[0] = '\0';
    }

    Setting& operator=(const Setting& other)
    {
      memcpy(key, other.key, DataSize::SETTING_KEY_SIZE);
      memcpy(value, other.value, DataSize::SETTING_VALUE_SIZE);
      return *this;
    }

    char key[DataSize::SETTING_KEY_SIZE];
    char value[DataSize::SETTING_VALUE_SIZE]; // already includes the null terminator.
  };
}
