#pragma once

#include "../Platform.h"

namespace mas
{
  class Parameter
  {
  public:
    constexpr Parameter(const char* const name_P)
    : name_P(name_P)
    {
    }

    const char* const name_P;

    // Tries to parse and pack the unescaped string parameter.
    // Returns `false` with an error message if there was a problem.
    virtual bool tryParse(void* pMember, const char* const input, char* const outError) const = 0;
    
    // Tries to serialise the parameter to an unescaped string.
    // Returns `false` if an optional value was null.
    virtual bool toString(void* pMember, char* const output) const = 0;

    // Tries to serialise the parameter to an unescaped string.
    // Returns `false` if an optional value was null.
    virtual void usage(char* const output) const = 0;
  };
}