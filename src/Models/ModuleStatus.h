#pragma once

#include "../Enums/SystemStatus.h"

namespace mas
{
  struct ModuleStatus
  {  
    ModuleStatus() : key('\0'), status(SystemStatus::Stopped) {} 
    ModuleStatus(const char key, const SystemStatus status) : key(key), status(status) {}
    char key;
    SystemStatus status;
  };
}
