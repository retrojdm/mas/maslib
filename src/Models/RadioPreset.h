#pragma once

#include "../Platform.h"
#include "../Enums/RadioBand.h"

namespace mas
{
  class RadioPreset
  {
  public:
    RadioPreset()
    {
      band = RadioBand::FM;
      frequency = 0;
    }

    RadioPreset& operator=(const RadioPreset& other)
    {
      band = other.band;
      frequency = other.frequency;
      return *this;
    }

    RadioBand band;
    uint16_t frequency; // 94.1 is stored as 9410.
  };
}
