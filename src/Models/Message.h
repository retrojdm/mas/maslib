#pragma once
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <new>
#include <MemoryPool.h>
#include <StrategyLinear.h>
#include "MessageMemoryPool.h"
#include "Parameter.h"
#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Enums/MessageType.h"

namespace mas
{
  class Message
  {
  public:
    Message() : originPortIndex(-1) {}
    virtual ~Message() {}

    // Serial port index (of the array of serial ports passed into the SerialModule constructor).
    // Use -1 when originating from a local module.
    int8_t originPortIndex;

    // Derived classes need to describe themselves.
    virtual char topic() const = 0;
    virtual MessageType type() const = 0;
    virtual uint8_t minParameters() const { return 0; }
    virtual uint8_t maxParameters() const { return 0; }
    virtual const Parameter * const * parameters() const { return nullptr; }
    virtual void* memberPtr(const uint8_t i) const { return nullptr; }

    // The 'new' and 'delete' operators are overloaded to use a static memory pool instead of malloc.
    // Based on the Memory Pool in C++ tutorial by Mario Konrad.
    // See http://www.mario-konrad.ch/blog/programming/cpp-memory_pool.html
    //
    // Note:  This is done on the Tea5767, not the RadioCardBase to ensure the correct amount of memory is allocated for
    //        the class.
    //
    //        We'll be reserving more memory than we'll ever use, because we're using the same approach for the other
    //        radio card types. It shouldn't be a problem since the Teensy4.1 has relatively large amounts of SRAM.
    //
    void* operator new(size_t size) noexcept { return MessageMemoryPool::pool.alloc(size); }
    void* operator new(size_t size, const std::nothrow_t& tag) noexcept { return MessageMemoryPool::pool.alloc(size); }
    void operator delete(void * p) noexcept { MessageMemoryPool::pool.free(p); }
  };
}