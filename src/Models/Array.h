#pragma once

#include "../Platform.h"
#include "../Helpers/ArrayHelper.h"

namespace mas
{
  // We don't have access to the standard library, but an array class that has a length function is convenient,
  // especially when parsing command and event parameters.
  //
  // We pass a buffer in, rather than use size as a type argument to allow arrays of different sizes to be passed as
  // params to functions.
  template <typename T>
  class Array
  {
  private:
    uint8_t _size;
    uint8_t _length;
    T* _data;

  public:
    // WARNING FOR CHAR *
    //
    // We can't use something like char buffer[][]. It has to be a char * buffer[].
    // See Array::convertToPointerArray() to help with the conversion.
    Array(T* buffer, uint8_t size) : _size(size), _length(0), _data(buffer) {}

    // Copy constructor
    // Note: We still need to provide a buffer for the array. We don't use the other array's buffer. We copy its data.
    Array(T* buffer, const Array<T>& other) : _size(other._size), _length(other._length), _data(buffer)
    {
      for (uint8_t i = 0; i < _size; i++) {  
          // Note: We can't just copy the data when it's a string, so we use memcpy for all cases.
          // _data[i] = other._data[i];
          memcpy(static_cast<void*>(&(_data[i])), static_cast<const void*>(&(other._data[i])), sizeof(T));
      }
    }

    void clear()
    {
      _length = 0;
    }

    uint8_t size() const
    {
      return _size;
    }

    uint8_t length() const
    {
      return _length;
    }

    void setLength(uint8_t length)
    {
      _length = length;
    }

    bool add(T item)
    {
      if (_length >= _size)
      {
        return false;
      }

      _data[_length++] = item;
      return true;
    }

    // Specialisation of add for char *
    bool add(const char * item)
    {
      if (_length >= _size)
      {
        return false;
      }

      char * element = _data[_length++];
      if (element == nullptr)
      {
        return false;
      }

      strcpy(element, item);

      return true;
    }

    int16_t findIndex(T search) const
    {
      return ArrayHelper::findIndex(_data, _length, [search] (T x) { return x == search; });
    }

    // Specialisation of findIndex for char *
    int16_t findIndex(const char * search) const
    {
      return ArrayHelper::findIndex(_data, _length, [search] (const char* x) { return strcmp(x, search) == 0; });
    }

    T * data() const
    {
      return const_cast<T *>(_data);
    }

    // Overloaded subscript operator for accessing elements.
    T& operator[](uint8_t index)
    {
      return _data[index];
    }

    // Overloaded const subscript operator for accessing elements (for const objects).
    const T& operator[](uint8_t index) const
    {
      return _data[index];
    }

    // Copy assignment operator.
    Array<T>& operator=(const Array<T>& other)
    {
      if (this != &other) {
        _size = other._size;
        _length = other._length;
        for (uint8_t i = 0; i < _size; i++)
        {
          // Note: We can't just copy the data when it's a string, so we use memcpy for all cases.
          // _data[i] = other._data[i];
          memcpy(static_cast<void*>(&(_data[i])), static_cast<const void*>(&(other._data[i])), sizeof(T));
        }
      }
      return *this;
    }
  };
}