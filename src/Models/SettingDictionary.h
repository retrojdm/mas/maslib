#pragma once

#include "../Platform.h"
#include "../Constants/DataSize.h"
#include "../Helpers/ArrayHelper.h"
#include "Setting.h"

namespace mas
{
  class SettingDictionary
  {
  public:
    SettingDictionary();
    Setting& operator[](int index);
    char* operator[](const char* const key);
    void clear();
    uint8_t count() const;
    bool hasKey(const char* const key) const;
    char* get(const char* const key) const;
    bool get(const char* const key, char* const outValue) const;
    bool set(const char* const key, const char* const value);
    void remove(const char* const key);

  private:
    uint8_t _count;
    Setting _settings[DataSize::SETTINGS_MAX];
  };
}
