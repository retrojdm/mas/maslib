#pragma once

namespace mas
{
  class SpecialCharacter
  {
  public:
    constexpr SpecialCharacter(char character, char escapeCode) : character(character), escapeCode(escapeCode) {}

    char character;
    char escapeCode;
  };
}
