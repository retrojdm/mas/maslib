#pragma once

#include "../Platform.h"

namespace mas
{
  class RadioBandDefinition
  {
  public:
    constexpr RadioBandDefinition(const uint16_t min, const uint16_t max, const uint16_t step, const uint16_t divisor) :
      min(min), max(max), step(step), divisor(divisor)
    {
    }

    const uint16_t min;
    const uint16_t max;
    const uint16_t step;
    const uint16_t divisor;
  };
}
