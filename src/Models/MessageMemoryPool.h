#pragma once

#include <MemoryPool.h>
#include <StrategyLinear.h>
#include "../Constants/DataSize.h"

namespace mas
{
  using MessageMemoryPoolType =
    MemoryPool<DataSize::MESSAGE_SIZE, StrategyLinear<DataSize::MESSAGE_SIZE, DataSize::MESSAGES_MAX>>;

  class MessageMemoryPool
  {
  public:
    static MessageMemoryPoolType pool;
  };
}