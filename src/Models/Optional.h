#pragma once

#include "../Platform.h"

namespace mas
{
  // The Optional<T> class can be used like std::optional<T>.
  // This is neccessary on Arduino, since std::optional<T> is not available.
  template <class T>
  class Optional
  {
  public:
    Optional() : _hasValue(false) {}
    
    Optional(const T &value) : _hasValue(true), _value(value) {}
    
    bool hasValue() const
    {
      return _hasValue;
    }

    const T &value() const
    {
      if (!_hasValue)
      {
        // We should throw an exception, but Arduino code is compiled with the `-fno-exceptions` option.
        // Instead, we'll just return the default value.
        //
        // Devs will need to have discipline, and always check `.hasValue()` before accessing `.value()`.
        return _defaultValue;
      }
      return _value;
    }

    Optional &operator=(const T &value)
    {
      _value = value;
      _hasValue = true;
      return *this;
    }

    void set(const T &value)
    {
      _value = value;
      _hasValue = true;
    }

    virtual void reset()
    {
      _hasValue = false;
      _value = _defaultValue;
    }

  private:
    bool _hasValue;
    T _value;
    static const T _defaultValue;
  };

  // _defaultValue belongs to the Optional class template, and it's accessed using the scope resolution operator ::.
  // The empty initializer list initializes _defaultValue.
  // For fundamental types like int, float, double, etc., it initializes the variable with zero. For user-defined types,
  // it initializes the variable using the default constructor.
  template <class T>
  const T Optional<T>::_defaultValue{};

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Specialisation for pointer types. Eg: char*.
  template <class T>
  class Optional<T*>
  {
  public:   
    Optional(const T* pBuffer, const uint8_t size)
    : _hasValue(false), _pBuffer(pBuffer), _size(size)
    {
    }

    Optional(const T* pBuffer, const uint8_t size, const char * const value)
    : _hasValue(true), _pBuffer(pBuffer), _size(size)
    {
      memcpy((void*)_pBuffer, (void*)value, _size);
    }
    
    bool hasValue() const
    {
      return _hasValue;
    }

    T const * value() const
    {
      if (!_hasValue)
      {
        return nullptr;
      }
      return _pBuffer;
    }

    Optional &operator=(const T* value)
    {
      memcpy((void*)_pBuffer, (void*)value, _size);
      _hasValue = true;
      return *this;
    }

    void set(const T* value)
    {
      memcpy((void*)_pBuffer, (void*)value, _size);
      _hasValue = true;
    }

    virtual void reset()
    {
      _hasValue = false;
      memset((void*)_pBuffer, 0, _size);
    }

  private:
    bool _hasValue;
    T const * _pBuffer;
    uint8_t _size;
  };
}