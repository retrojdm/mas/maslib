#include <Audio.h>
#include <TEA5767.h>

// GUItool: begin automatically generated code
AudioInputI2S            i2sIn;           //xy=77,38
AudioOutputUSB           usbOut;           //xy=216,38
AudioConnection          patchCord1(i2sIn, 0, usbOut, 0);
AudioConnection          patchCord2(i2sIn, 1, usbOut, 1);
// GUItool: end automatically generated code

TEA5767 radio();

void setup()
{
  AudioMemory(8);
  
  radio.init();
  radio.setBandFrequency(RADIO_BAND_FM, 9410);
}

void loop()
{
}
