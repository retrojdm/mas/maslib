#include <Audio.h>

// GUItool: begin automatically generated code
AudioInputUSB            usbIn;           //xy=70,40
AudioOutputI2S           i2sOut;           //xy=230,40
AudioConnection          patchCord1(usbIn, 0, i2sOut, 0);
AudioConnection          patchCord2(usbIn, 1, i2sOut, 1);
// GUItool: end automatically generated code


#define PIN_MUTE 2

void setup()
{
  AudioMemory(8);

  pinMode(PIN_MUTE, OUTPUT);
  digitalWrite(PIN_MUTE, HIGH);
}

void loop()
{
}
