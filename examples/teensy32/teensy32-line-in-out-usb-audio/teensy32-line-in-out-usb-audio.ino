#include <Audio.h>

// GUItool: begin automatically generated code
AudioInputUSB            usb_In;           //xy=70,40
AudioInputI2S            i2s_In;           //xy=70,80
AudioOutputI2S           i2s_Out;           //xy=213,40
AudioOutputUSB           usb_Out;           //xy=215.5,80
AudioConnection          patchCord1(usb_In, 0, i2s_Out, 0);
AudioConnection          patchCord2(usb_In, 1, i2s_Out, 1);
AudioConnection          patchCord3(i2s_In, 0, usb_Out, 0);
AudioConnection          patchCord4(i2s_In, 1, usb_Out, 1);
// GUItool: end automatically generated code


#define PIN_MUTE 2

void setup()
{
  AudioMemory(8);

  pinMode(PIN_MUTE, OUTPUT);
  digitalWrite(PIN_MUTE, HIGH);
}

void loop()
{
}