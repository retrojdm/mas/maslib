#include <Audio.h>

// This is the bare-minimum configuration to get I2S audio from a Teensy 3.2 onto a PC.
// Note: You may need to fiddle with sound device settings in Windows to "listen to this device".

// GUItool: begin automatically generated code
AudioInputI2S            i2sIn;           //xy=77,38
AudioOutputUSB           usbOut;           //xy=216,38
AudioConnection          patchCord1(i2sIn, 0, usbOut, 0);
AudioConnection          patchCord2(i2sIn, 1, usbOut, 1);
// GUItool: end automatically generated code

#include <SI4735.h>

#define AM_FUNCTION 1
#define FM_FUNCTION 0

#define PIN_SI4735_RESET 0
#define PIN_SI4735_GPIO1 1
#define PIN_SI4735_GPIO2 2

SI4735 rx;

void setup()
{
  Serial.begin(115200);
  Serial.println("SI4735.");
  
  AudioMemory(8);

  // Si4735
  pinMode(PIN_SI4735_RESET, OUTPUT);
  pinMode(PIN_SI4735_GPIO1, OUTPUT);
  pinMode(PIN_SI4735_GPIO2, OUTPUT);  
  digitalWrite(PIN_SI4735_RESET, HIGH); // Reset is active low. This is just getting it ready. rx.setup() does the resetting.
  digitalWrite(PIN_SI4735_GPIO1, HIGH); //
  digitalWrite(PIN_SI4735_GPIO2, LOW);  // At reset, GPIO1 and GPIO2 need to be HIGH and LOW respectively to enable I2C mode.
  
  // Look for the Si47XX I2C bus address
  int16_t si4735Addr = rx.getDeviceI2CAddress(PIN_SI4735_RESET);
  if ( si4735Addr == 0 ) {
    Serial.println("Si473X not found! Did you pull SDA and SCL high with 4.7K resistors?");
    Serial.flush();
    while (1);
  } else {
    Serial.print("The Si473X I2C address is 0x");
    Serial.println(si4735Addr, HEX);
  }

  rx.setRefClock(32768);
  rx.setRefClockPrescaler(1);
  rx.setup(PIN_SI4735_RESET, -1, FM_CURRENT_MODE, SI473X_ANALOG_DIGITAL_AUDIO, XOSCEN_RCLK);

  // This seems to need to go before the I2S stuff.
  rx.setFM(8400, 10800, 9410, 10);

  rx.digitalOutputSampleRate(44100);

  // OSIZE Dgital Output Audio Sample Precision (0=16 bits, 1=20 bits, 2=24 bits, 3=8bits).
  // OMONO Digital Output Mono Mode (0=Use mono/stereo blend ).
  // OMODE Digital Output Mode (0=I2S, 6 = Left-justified, 8 = MSB at second DCLK after DFS pulse, 12 = MSB at first DCLK after DFS pulse).
  // OFALL Digital Output DCLK Edge (0 = use DCLK rising edge, 1 = use DCLK falling edge)
  rx.digitalOutputFormat(0 /* OSIZE */, 0 /* OMONO */, 0 /* OMODE */, 0 /* OFALL*/);

  rx.setVolume(63);

  Serial.print("currentFrequency: ");
  Serial.println(rx.getFrequency());
}

void loop()
{
}
