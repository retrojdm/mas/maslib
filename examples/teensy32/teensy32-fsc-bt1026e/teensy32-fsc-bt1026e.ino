// IMPORTANT
// This sketch is essentially just a UART echo.
// You'll need to manually send the commsnds described in comments below via the serial monitor.
// The I2S config is especially important.
 
#include <Audio.h>

// GUItool: begin automatically generated code
AudioInputI2S            i2sIn;           //xy=77,38
AudioOutputUSB           usbOut;           //xy=216,38
AudioConnection          patchCord1(i2sIn, 0, usbOut, 0);
AudioConnection          patchCord2(i2sIn, 1, usbOut, 1);
// GUItool: end automatically generated code

// Power BT module with 3V3

// UART
const int pinRx = 0; // BT Tx = Teensy RX1
const int pinTx = 1; // BT Rx = Teensy TX1

// System
const int pinSysCtrl = 2; // SYS_CTRL

// I2S
const int pinbBclk = 9;
const int pinbMclk = 11;
const int pinbLrclk = 23;
const int pinI2sIn = 13; // BT out = Teensy In

void setup() {
  // Init the SYS_CTRL line low.
  pinMode(pinSysCtrl, OUTPUT);
  digitalWrite(pinSysCtrl, LOW);

  AudioMemory(8);  
  
  Serial.begin(9600); // PC <-> Teensy
  Serial1.begin(115200); // Teensy <-> FSC-BT1026E

  
  // POWER UP
  //
  // Power the device by ulling the SYS_CTRL pin HIGH.
  //
  // Note: We won't have any GPIOs left on an MU50 I/O card, so we can't dedicate one to the RESET pin.
  // Luckily there's an AT command for that: "AT+REBOOT".
  delay(100);
  digitalWrite(pinSysCtrl, HIGH);

  // The real implementation will configure all of this before "powering up", i.e. to ensure the name shows correctly on your
  // phone etc.
  // In this sketch, we'll manually send AT commands via the serial monitor.

  // NAME
  //
  // Send an "AT+NAME=whatever" to enable rename the bluetooth module.
  //
  // Eg: AT+NAME=FSC-BT1026E

  
  // PROFILES
  //
  // Send an "AT+PROFILE=160" to enable only A2DP Sink, and AVRCP Controller.
  // Note: You might get "ERROR" in response, since this may already be set, and it's persisted between power cycles.
  //
  // 00010100000 = 160
  //
  // BIT[0]   SPP (Serial Port Profile
  // BIT[1]   GATT Server (Generic Attribute Profile)
  // BIT[2]   GATT Client (Generic Attribute Profile)
  // BIT[3]   HFP Sink (Hands-Free Profile)
  // BIT[4]   HFP Source (Hands-Free Profile)
  // BIT[5]   A2DP Sink (Advanced Audio Distribution Profile)
  // BIT[6]   A2DP Source (Advanced Audio Distribution Profile)
  // BIT[7]   AVRCP Controller (Audio/Video Remote Control Profile)
  // BIT[8]   AVRCP Target (Audio/Video Remote Control Profile)
  // BIT[9]   HID Keyboard (Human Interface Profile)
  // BIT[10]  PBAP (Phonebook Access Profile)

  
  // AVRCP CONFIG
  //
  // Send an "AT+AVRCPCFG=3"
  //
  // 3210
  // 0011 = 3
  //
  // BIT[0]   Auto get track ID3 information (title, artist, album) on track changed. Default: 1
  // BIT[1-3] Auto get track state (play progress) inf value > 0. default: 5 (seconds)

  
  // I2S CONFIG
  //
  // Send an "AT+I2SCFG=7" to enable 16-bit 44,100Hz, I2S out in slave mode.
  //
  // 76543210
  // 00000111 = 7
  //
  // BIT[0]    0: Disable I2S/PCM for audio input/output
  //           1: Enable I2S/PCM for audio input/output
  // BIT[1]    0: I2S/PCM master role
  //           1: I2S/PCM slave role
  // BIT[3-4] 00: I2S Philips standard format
  // BIT[5-6] 00: 16-bit resolution
  //          01: 24-bit resolution
  //          02: 32-bit resolution


  // VOLUME
  //
  // Send an "AT+SPKVOL=15" to set the speaker volume between 0 and 15.
}

void loop() {
  if (Serial.available()) {
    Serial1.write(Serial.read());
  }

  if (Serial1.available()) {
    Serial.write(Serial1.read());
  }
}
