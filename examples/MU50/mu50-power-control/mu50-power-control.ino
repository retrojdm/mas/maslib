#define PIN_OPTO       10
#define PIN_RELAY      22
#define PIN_LED        13

void setup() {
  pinMode(PIN_OPTO, INPUT);

  pinMode(PIN_LED, OUTPUT);

  pinMode(PIN_RELAY, OUTPUT);
  digitalWrite(PIN_RELAY, LOW);

  for (int i = 0; i < 3; i++)
  {
    digitalWrite(PIN_LED, LOW);
    delay(250);
    digitalWrite(PIN_LED, HIGH);
    delay(250);      
  }
  
  // Energise relay (switch from 12V ACC to 12V BATT)
  digitalWrite(PIN_RELAY, HIGH);
}

bool opto = false;
bool optoWas = false;

void loop() {
  delay(100);

  optoWas = opto;
  opto = digitalRead(PIN_OPTO) == LOW;

  if (opto != optoWas)
  {
    digitalWrite(PIN_LED, opto ? HIGH : LOW);
  }

  if (!opto)
  {
    for (int i = 0; i < 2; i++)
    {
      digitalWrite(PIN_LED, LOW);
      delay(500);
      digitalWrite(PIN_LED, HIGH);
      delay(500);      
    }

    digitalWrite(PIN_RELAY, LOW); // Kill power to the entire system.
    delay(2000);
  }
}
