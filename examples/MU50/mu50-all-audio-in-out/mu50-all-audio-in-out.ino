#include <Wire.h>
#include <Audio.h>
#include <SI4735.h>

// GUItool: begin automatically generated code
AudioInputI2SOct         slot0123In;     //xy=80,162
AudioInputUSB            pcIn;           //xy=286,48
AudioInputI2S2           slot4In;        //xy=289,265
AudioMixer4              slot0123MixerL; //xy=314,126
AudioMixer4              slot0123MixerR; //xy=316,191
AudioMixer4              finalMixerL;    //xy=543,123
AudioMixer4              finalMixerR;    //xy=543,190
AudioAmplifier           ampSlot3R;      //xy=800,168
AudioAmplifier           ampPcR;         //xy=802,88
AudioAmplifier           ampSlot4R;      //xy=802,256
AudioAmplifier           ampSlot3L;      //xy=803,135
AudioAmplifier           ampPcL;         //xy=805,55
AudioAmplifier           ampSlot4L;      //xy=805,223
AudioOutputUSB           pcOut;          //xy=969,72
AudioOutputI2S           slot3Out;       //xy=969,151
AudioOutputI2S2          slot4Out;       //xy=969,235
AudioConnection          patchCord1(slot0123In, 0, slot0123MixerL, 0);
AudioConnection          patchCord2(slot0123In, 1, slot0123MixerR, 0);
AudioConnection          patchCord3(slot0123In, 2, slot0123MixerL, 1);
AudioConnection          patchCord4(slot0123In, 3, slot0123MixerR, 1);
AudioConnection          patchCord5(slot0123In, 4, slot0123MixerL, 2);
AudioConnection          patchCord6(slot0123In, 5, slot0123MixerR, 2);
AudioConnection          patchCord7(slot0123In, 6, slot0123MixerL, 3);
AudioConnection          patchCord8(slot0123In, 7, slot0123MixerR, 3);
AudioConnection          patchCord9(pcIn, 0, finalMixerL, 0);
AudioConnection          patchCord10(pcIn, 1, finalMixerR, 0);
AudioConnection          patchCord11(slot4In, 0, finalMixerL, 3);
AudioConnection          patchCord12(slot4In, 1, finalMixerR, 3);
AudioConnection          patchCord13(slot0123MixerL, 0, finalMixerL, 2);
AudioConnection          patchCord14(slot0123MixerR, 0, finalMixerR, 2);
AudioConnection          patchCord15(finalMixerL, ampPcL);
AudioConnection          patchCord16(finalMixerL, ampSlot3L);
AudioConnection          patchCord17(finalMixerL, ampSlot4L);
AudioConnection          patchCord18(finalMixerR, ampPcR);
AudioConnection          patchCord19(finalMixerR, ampSlot3R);
AudioConnection          patchCord20(finalMixerR, ampSlot4R);
AudioConnection          patchCord21(ampSlot3R, 0, slot3Out, 1);
AudioConnection          patchCord22(ampPcR, 0, pcOut, 1);
AudioConnection          patchCord23(ampSlot4R, 0, slot4Out, 1);
AudioConnection          patchCord24(ampSlot3L, 0, slot3Out, 0);
AudioConnection          patchCord25(ampPcL, 0, pcOut, 0);
AudioConnection          patchCord26(ampSlot4L, 0, slot4Out, 0);
// GUItool: end automatically generated code

SI4735 rx;

#define PIN_RELAY        22 // Power board

#define PIN_BT_MFB       39 // MU50 Slot 0 GPIO 2

#define PIN_SI4735_RESET 15 // MU50 Slot 1 GPIO0
#define PIN_SI4735_GPIO1 14 // MU50 Slot 1 GPIO1
#define PIN_SI4735_GPIO2 41 // MU50 Slot 1 GPIO2

#define PIN_SLOT3_MUTE   30 // MU50 Slot 3 GPIO 2

#define PIN_SLOT4_MUTE   36 // MU50 Slot 4 GPIO 2

#define AM_FUNCTION 1
#define FM_FUNCTION 0

void setup()
{
  // Teensy USB to PC. Not actually 9600 baud.
  Serial.begin(9600);
  Serial.println("all-audio-in-out");

  // Slot 0 UART (MU50-PCB-BLU).
  Serial1.begin(38400);
  
  AudioMemory(32);

  slot0123MixerL.gain(0, 1.0);
  slot0123MixerR.gain(0, 1.0);

  slot0123MixerL.gain(1, 1.0);
  slot0123MixerR.gain(1, 1.0);

  slot0123MixerL.gain(2, 1.0);
  slot0123MixerR.gain(2, 1.0);

  slot0123MixerL.gain(3, 1.0);
  slot0123MixerR.gain(3, 1.0);

  finalMixerL.gain(0, 1.0);
  finalMixerR.gain(0, 1.0);
  
  finalMixerL.gain(1, 1.0);
  finalMixerR.gain(1, 1.0);
  
  finalMixerL.gain(2, 1.0);
  finalMixerR.gain(2, 1.0);
  
  finalMixerL.gain(3, 1.0);
  finalMixerR.gain(3, 1.0);

  ampPcL.gain(1.0);
  ampPcR.gain(1.0);

  ampSlot3L.gain(1.0);
  ampSlot3R.gain(1.0);
  pinMode(PIN_SLOT3_MUTE, OUTPUT);
  digitalWrite(PIN_SLOT3_MUTE, HIGH); // Unmute

  ampSlot4L.gain(1.0);
  ampSlot4R.gain(1.0);
  pinMode(PIN_SLOT4_MUTE, OUTPUT);  
  digitalWrite(PIN_SLOT4_MUTE, HIGH); // Unmute

  // Bluetooth
  pinMode(PIN_BT_MFB, OUTPUT);  
  digitalWrite(PIN_BT_MFB, HIGH); // BT Power up

  // Si4735
  pinMode(PIN_SI4735_RESET, OUTPUT);
  pinMode(PIN_SI4735_GPIO1, OUTPUT);
  pinMode(PIN_SI4735_GPIO2, OUTPUT);  
  digitalWrite(PIN_SI4735_RESET, HIGH); // Reset is active low. This is just getting it ready. rx.setup() does the resetting.
  digitalWrite(PIN_SI4735_GPIO1, HIGH); //
  digitalWrite(PIN_SI4735_GPIO2, LOW);  // At reset, GPIO1 and GPIO2 need to be HIGH and LOW respectively to enable I2C mode.

  delay(500);
  
  // Look for the Si47XX I2C bus address
  int16_t si4735Addr = rx.getDeviceI2CAddress(PIN_SI4735_RESET);
  if ( si4735Addr == 0 ) {
    Serial.println("Si473X not found! Did you pull SDA and SCL high with 4.7K resistors?");
    Serial.flush();
    while (1);
  } else {
    Serial.print("The Si473X I2C address is 0x");
    Serial.println(si4735Addr, HEX);
  }

  delay(500);
  
  rx.setRefClock(32768);
  rx.setRefClockPrescaler(1);   // will work with 32768  
  rx.setup(PIN_SI4735_RESET, -1, FM_CURRENT_MODE, SI473X_ANALOG_DIGITAL_AUDIO, XOSCEN_RCLK); // Analog and digital audio outputs (LOUT/ROUT and DCLK, DFS, DIO), external RCLK
  // rx.setup(PIN_SI4735_RESET, -1, FM_CURRENT_MODE, SI473X_DIGITAL_AUDIO1, XOSCEN_RCLK);    // Digital audio output (DCLK, LOUT/DFS, ROUT/DIO)
  // rx.setup(PIN_SI4735_RESET, -1, FM_CURRENT_MODE, SI473X_DIGITAL_AUDIO2, XOSCEN_RCLK);    // Digital audio outputs (DCLK, DFS, DIO)

  delay(500); // Wait the oscillator becomes stable. To be checked...

  rx.setFM(8400, 10800, 9410, 10);
  delay(500);

  // Setting SI473X Sample rate to 44.1K.
  rx.digitalOutputSampleRate(44100);

  // OSIZE Dgital Output Audio Sample Precision (0=16 bits, 1=20 bits, 2=24 bits, 3=8bits).
  // OMONO Digital Output Mono Mode (0=Use mono/stereo blend ).
  // OMODE Digital Output Mode (0=I2S, 6 = Left-justified, 8 = MSB at second DCLK after DFS pulse, 12 = MSB at first DCLK after DFS pulse).
  // OFALL Digital Output DCLK Edge (0 = use DCLK rising edge, 1 = use DCLK falling edge)
  rx.digitalOutputFormat(0 /* OSIZE */, 0 /* OMONO */, 0 /* OMODE */, 0 /* OFALL*/);

  rx.setVolume(45);

  Serial.print("currentFrequency: ");
  Serial.println(rx.getFrequency());
}

void loop()
{
  if (Serial.available()) {      // If anything comes in Serial (USB),
    Serial1.write(Serial.read());   // read it and send it out Serial1 (pins 0 & 1)
  }

  if (Serial1.available()) {     // If anything comes in Serial1 (pins 0 & 1)
    Serial.write(Serial1.read());   // read it and send it out Serial (USB)
  }
}
