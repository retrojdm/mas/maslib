// How many slots are there on the MU50 main unit?
const int slots =  5;

// I/O pins only. Not including the four power pins (GND, +3V3, +5V, +12V).
const int ioPinsPerSlot = 14;

// Maps slot pins to teensy pins.
// -1 = no connection.
int ioPins[slots][ioPinsPerSlot] =
{
  //    1A,     2A,     3A, 4A, 5A,  6A,        1B,   2B,    3B,  4B,  5B,   6B,   7B,   8B
  // GPIO0,  GPIO1,  GPIO2, CS, IN, OUT,      MCLK, BCLK, LRCLK, SDA, SCL, MOSI, MISO, SCLK
  //
  {      0,      1,     39, 38,  8,  -1,        23,   21,    20,  18,  19,   11,   12,   13  },   // Slot 0
  {     15,     14,     40, 41,  6,  -1,        23,   21,    20,  18,  19,   11,   12,   13  },   // Slot 1
  {     25,     24,     26, 27,  9,  -1,        23,   21,    20,  18,  19,   11,   12,   13  },   // Slot 2
  {     28,     29,     30, 31, 32,   7,        23,   21,    20,  18,  19,   11,   12,   13  },   // Slot 3
  {     34,     35,     36, 37,  5,   2,        33,    4,     3,  18,  19,   11,   12,   13  }    // Slot 4
};

void setup() {
  Serial.begin(9600);
  Serial.println("I/O Card test");
  
  for (int s = 0; s < slots; s++)
  {
    for (int p = 0; p < ioPinsPerSlot; p++)
    {
      int pin = ioPins[s][p];
      if (pin == -1)
      {
        continue;
      }
      
      pinMode(pin, OUTPUT);
    }
  }
}

bool led = false;

void loop() {  
  for (int s = 0; s < slots; s++)
  {
    for (int p = 0; p < ioPinsPerSlot; p++)
    {
      int pin = ioPins[s][p];
      if (pin == -1)
      {
        continue;
      }
      
      digitalWrite(pin, led ? HIGH : LOW);
    }
  }

  Serial.write(led ? "O" : "X");

  delay(500);
  led = !led;
}
