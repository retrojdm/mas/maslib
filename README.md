# Modular Audio System (MAS) Core Arduino Library

## ⚠️ Work In Progress

**This project is still in development and is untested.**

---

Part of the retrojdm.com _Modular Audio System_.

|     | Repo                                                               | Description                              |
| :-: | :----------------------------------------------------------------- | :--------------------------------------- |
|  🡆  | **[mas-core](https://gitlab.com/retrojdm/mas/mas-core)**           | **Core Arduino library for firmware**    |
|     | [mas-front-panel](https://gitlab.com/retrojdm/mas/mas-front-panel) | Front Panel Arduino library for firmware |
|     | [MU50](https://gitlab.com/retrojdm/mas/MU50)                       | Main Unit and I/O cards                  |
|     | [FP10](https://gitlab.com/retrojdm/mas/FP10)                       | 12-button Front Panel                    |
|     | [FP20](https://gitlab.com/retrojdm/mas/FP20)                       | 5-button Front Panel                     |

---

## Overview

This library contains code that is common to all units (main unit and all front panel variants).

## System architecture

The Modular Audio System can be made up of multiple units connected via serial ports:

| Unit        | Description                                                                                                           | Role   |
| :---------- | :-------------------------------------------------------------------------------------------------------------------- | :----- |
| Main Unit   | Contains system, serial, clock, power, I/O cards, audio source, volume, effects, radio, bluetooth, and media modules. | Server |
| Front Panel | Contains serial, button, encoder, and UI (including display) modules, etc.                                            | Client |
| PC          | Usually used as a serial terminal for debugging. A desktop app could potentially be created.                          | Client |

The Main Unit is the heart of the system. It's the only required unit; acting as a kind of "server".

## Publisher/subscriber

An internal message queue is managed using the publisher/subscriber pattern.

A broker runs in the main loop of each unit, with the following responsibilities:

- Allow modules to subscribe to specific "topics"
- Allow modules to publish messages (push to the queue)
- Distribute messages to the various subscribed modules (pop from the queue)

## Serial module

Both the Main Unit and Front Panel have a special Serial module with the following responsibilities:

- Receive messages from serial ports
- Parse/validate received messages - Sending any error messages directly back to the origin port
- Mark internal messages with their origin port number (-1 is used for messages originating from local modules)
- Prevent messages from looping back to their origin port
- Ensure message delivery via simple flow control (ETX/ACK character codes)

# Docs

- [TODO](./docs/todo.md)
- [Serial commands](./docs/messages.md)
- [Serial messages - Main Unit (MU50)](./docs/messages-main-unit.md)
- [Serial messages - Front Panel (FP10, FP20)](./docs/messages-front-panel.md)
- [Unit tests](./tests/README.md)